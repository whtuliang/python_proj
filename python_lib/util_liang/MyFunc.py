# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 11:55:09 2019

@author: ltu
"""

import numpy as np

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)
    
    
def get_range(data):
    x = np.ndarray.flatten(data)
#        x_box = plt.boxplot(x)
    x = x[~np.isnan(x)]
#    median = np.percentile(x, 50)
    Q1 = np.percentile(x, 25)
    Q3 = np.percentile(x, 75)
    IQR = Q3 - Q1
    if Q1==Q3:
        return np.percentile(x, 5),  np.percentile(x, 95)
    else:
        return max(Q1 - 0.5*IQR, min(x)), min(Q3 + 0.5*IQR, max(x))
    
    
    
    
def image_fft_ifft(img):
    from scipy import fftpack
    data_fft = np.fft.fft2( img-img.mean() ) #(img-img.mean())/img.std()
    im_fft2 = data_fft.copy()
    
    data_fft_shift = np.abs(np.fft.fftshift(data_fft))
    keep_fraction = 0.005
    high_freq_band = 0.2
    r, c = data_fft_shift.shape
#    im_fft2[int(r*keep_fraction):int(r*(1-keep_fraction))] = 0
#    im_fft2[:, int(c*keep_fraction):int(c*(1-keep_fraction))] = 0
    
#    im_fft2[:int(r*keep_fraction), :int(c*keep_fraction)] = 0
#    im_fft2[:int(r*keep_fraction), int(c*(1-keep_fraction)): ] = 0
#    im_fft2[int(r-r*keep_fraction):, :int(c*keep_fraction) ] = 0
#    im_fft2[int(r-r*keep_fraction):, int(c*(1-keep_fraction)): ] = 0
    
    im_fft2[:int(r*keep_fraction), :] = 0
    im_fft2[int(r-r*keep_fraction):, : ] = 0
#    im_fft2[:, :int(c*keep_fraction) ] = 0
#    im_fft2[:, int(c*(1-keep_fraction)): ] = 0
    
    im_fft2[int(r*(0.5-high_freq_band)):int(r*(0.5+high_freq_band)), : ] = 0
    im_fft2[:, int(c*(0.5-high_freq_band)):int(c*(0.5+high_freq_band))] = 0
    
    data_ifft = fftpack.ifft2(im_fft2).real + img.mean()
#    plt.imshow(data_ifft)
#    plt.imshow(data_fft_shift)
    return data_ifft

def image_rescale(img):
    from skimage.transform import rescale   #, resize, downscale_local_mean
    image_rescaled = rescale(img, 0.1, anti_aliasing=False)
    return image_rescaled