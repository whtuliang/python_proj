# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 12:28:39 2019

@author: ltu
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
#import statsmodels.api as sm # import statsmodels 
import os
#from os import listdir
#from os.path import isfile, isdir, join
import h5py
import shutil, tempfile

def get_data_h5(file, plot_property):
    def create_temporary_copy(src):
      tf = tempfile.TemporaryFile(mode='r+b', prefix='__', suffix='.rsts.h5')
      with open(src,'rb') as f:
        shutil.copyfileobj(f,tf)
      tf.seek(0) 
      return tf
    if os.path.isfile(file):
        with create_temporary_copy(file) as temp:
#            temp = create_temporary_copy(file) 
            try:
                data = h5py.File(temp, 'r')['ReducedZMWMetrics']  # <KeysViewHDF5 ['All', 'P1', 'Sequencing']> 
            except:
                print('Read Fail: ' + file)
                return None
                
            if plot_property=='P1_prod':
                data_final = data['Sequencing']['Count=1']['Productivity'][:] 
            elif plot_property=='P0_prod':
                data_final = data['Sequencing']['Count=0']['Productivity'][:] 
            elif plot_property=='P2_prod':
                data_final = data['Sequencing']['Count=2']['Productivity'][:] 
            elif plot_property=='P1_loading':
                data_final = data['Sequencing']['Count=1']['Loading'][:]
            elif plot_property=='NumBases':
                data_final = data['All']['Sum']['NumBases'][:] 
            elif plot_property=='HQP1_start':
                data_final = data['P1']['Median']['HQRegionStart'][:] 
            elif plot_property=='HQPKmid':
                data_final = data['P1']['Median']['HQPkmid'][:,:,1] 
            elif plot_property=='HQSNR':
                data_final = data['P1']['Median']['HQRegionSnrMean'][:,:,1] 
            elif plot_property=='P1_ReadLength':
                data_final = data['P1']['Mean']['ReadLength'][:,:] 
            elif plot_property=='BaseLine':
                data_final = np.sum(data['Sequencing']['Median']['BaselineLevel'][:,:,:], axis=2)
            elif plot_property=='BaseRate':
                data_final = data['P1']['Median']['LocalBaseRate'][:] 
            elif plot_property=='BaseIpd':
                data_final = data['P1']['Mean']['BaseIpd'][:,:] 
            else:
                data_final = None
#            data_final = np.array([np.int_(x) for x in data_final])
            return data_final
    else:
        print(file + ' not found')
        return None
    
def get_movie_rsts(movie_i,chip_i, prod_type, plot_property, isEOLQC=False):
    instr,date, seq = movie_i.split('_')
    lot,waf_n,chip_n = chip_i.split('-')
    if prod_type == 'Spider':
        grid_size = 13*13
    else:
        grid_size = 8*8
#    wafer = lot + '-' + waf_n
    if isEOLQC:
        if prod_type == 'Spider':
            file = r'\\pbi\dept\techsupport\internaldata\spExtra\{0}\nrt\data\pacbio\context\collection\{1}\{1}.rsts.h5'.format(instr[1:], movie_i)
        else:
            file = r'\\pbi\dept\techsupport\internaldata\sqExtra\{0}\icc\data\pacbio\context\collection\{1}\{1}.rsts.h5'.format(instr[1:], movie_i)
    else:
        if prod_type == 'Spider':
            file = r'\\pbi\dept\techsupport\spExtra\{0}\nrt\data\pacbio\context\collection\{1}\{1}.rsts.h5'.format(instr[1:], movie_i)
        else:
            file = r'\\pbi\dept\techsupport\sqExtra\{0}\icc\data\pacbio\context\collection\{1}\{1}.rsts.h5'.format(instr[1:], movie_i)   
    if os.name != 'nt':
        file = file.replace('\\','/')
    try:
        data_final = get_data_h5(file, plot_property) / grid_size * 100
    except:
        data_final = None
    return data_final


def get_EOLQC_chip_gif(wafer, prod_type):
    if prod_type == 'Sequel':
        path_eolqc = os.path.join( r'\\pbi\dept\systems\emcc\WaferId', wafer)
    else:
        path_eolqc = os.path.join( r'\\pbi\dept\systems\emcc\Spider\WaferId', wafer)
    onlyfiles = [[f[0:4],os.path.join(path_eolqc,f)] for f in os.listdir(path_eolqc) if (f[0:4].isnumeric())
                & (os.path.isfile(os.path.join(path_eolqc, f))) & ('EOLQC' in f)]
    return onlyfiles