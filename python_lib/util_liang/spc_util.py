# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 13:13:10 2019

@author: ltu
"""

#from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import os
import datetime
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pyodbc 

def get_prodtype(wafer):
    if (wafer[0]=='P' or wafer[0]=='p'):
        return 'Sequel'
    else:
        return 'Spider'

def load_data():
    #folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'
    folder_path = (r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data').replace('\\','/')
    data_name = 'all_chip_level_data.csv'
    data_raw = pd.read_csv( os.path.join(folder_path, data_name) )    
    return data_raw

def waf_PBI_SA():
    data_raw = load_data()
    data_summary = data_raw.groupby(['Wafer','PBI_Lot','SALot']).size().reset_index()
    data_summary = data_summary.drop(columns=[0])
    return data_summary

def chip_PBI_SA_status():
    data_raw = load_data()
    data_summary = data_raw.groupby(['Chip','PBI_Lot','SALot', 'CCY_Status']).size().reset_index()
    data_summary = data_summary.drop(columns=[0])
    return data_summary

def get_FE_maxWW(data_raw):
    date_str_list = data_raw['FE_QC_Cumulative_WW'].unique()
#    date_str_list = data_raw['FEWorkWeekOut'].unique()
    date_str_list = date_str_list [~ pd.isnull(date_str_list)]
    date_max = date_str_list.max()
    data_ww = data_raw[data_raw.FE_QC_Cumulative_WW == date_max] 
    return date_max, data_ww

def summary_FE_failure(data_ww, prod):
    col_names = ['Product','PBI_Lot','Wafer','Chip','FE_QC_Cumulative_WW','FE_QC_Cumulative_Grade',
              'VQC1','VQC2','EOQC1','OQC1','OQA','BCD_Grade','CCY_Status','CCY_WorkWeek', 'CCY_AC_Finished']
    data_ww = data_ww[col_names]
    #prod = 'Spider'
    data_sub = data_ww[data_ww.Product == prod]
    if prod == 'Sequel':
        grade_names = ['EOQC1','OQC1', 'VQC1']
    else:
        grade_names = ['EOQC1','OQA','BCD_Grade',  'OQC1', 'VQC1', 'VQC2'] # 'VQC1' only 5 waf per lot, 'VQC2' is included in PDP
    def failure_mode(x):
        index =  [True if x[g]=='F' else False  for g in grade_names ]
        if np.any(index):
            return str(np.array(grade_names)[index])
        else:
            if x['Product']=='Spider':
                index =  [True if pd.isnull(x[g]) else False  for g in ['EOQC1','OQC1', 'OQA'] ]  #grade_names
            else:
                index =  [True if pd.isnull(x[g]) else False  for g in ['EOQC1','OQC1'] ]
            if np.any(index):
                return 'Not Finished'
            else:
                return 'Pass'
    if len(data_sub)==0:
        data_sub['Failure_Cause'] = pd.np.nan
        data_sub_finished = data_sub
    else:
        data_sub['Failure_Cause'] = data_sub.apply(failure_mode, axis=1)
#        data_sub_finished = data_sub
        data_sub_finished = data_sub[data_sub['Failure_Cause']!='Not Finished']

    return data_sub_finished


def summary_FE_CDEF(data_ww, prod):
    col_names = ['Product','PBI_Lot','Wafer','Chip','FE_QC_Cumulative_WW','FE_QC_Cumulative_Grade',
              'VQC1','VQC2','EOQC1','OQC1','OQA','BCD_Grade','CCY_Status','CCY_WorkWeek', 'CCY_AC_Finished']
    data_ww = data_ww[col_names]
    #prod = 'Spider'
    data_sub = data_ww[data_ww.Product == prod]
    if prod == 'Sequel':
        grade_names = ['EOQC1','OQC1', 'VQC1']
    else:
        grade_names = ['EOQC1','OQA','BCD_Grade',  'OQC1', 'VQC1', 'VQC2'] # 'VQC1' only 5 waf per lot, 'VQC2' is included in PDP
    def failure_mode(x):
        index =  [True if x[g] in ('C','D','F') else False  for g in grade_names ]
        if np.any(index):
            return str(np.array(grade_names)[index])
        else:
            if x['Product']=='Spider':
                index =  [True if pd.isnull(x[g]) else False  for g in ['EOQC1','OQC1', 'OQA'] ]  #grade_names
            else:
                index =  [True if pd.isnull(x[g]) else False  for g in ['EOQC1','OQC1'] ]
            if np.any(index):
                return 'Not Finished'
            else:
                return 'Pass'
    if len(data_sub)==0:
        data_sub['Failure_Cause'] = pd.np.nan
        data_sub_finished = data_sub
    else:
        data_sub['Failure_Cause'] = data_sub.apply(failure_mode, axis=1)
#        data_sub_finished = data_sub
        data_sub_finished = data_sub[data_sub['Failure_Cause']!='Not Finished']

    return data_sub_finished


def pivot_FE_failure(*args):
    argCount = len(args)
    if argCount == 1:
        data_sub_finished = args[0]
        failure_sum = data_sub_finished.groupby(['PBI_Lot','Wafer','Failure_Cause'])['Chip'].count().reset_index().rename(columns={'Chip':'Failure_Count'})
        failure_pivot = failure_sum.pivot(index='Wafer', columns='Failure_Cause', values='Failure_Count')
        wafer_list_valid = failure_pivot[failure_pivot.sum(axis=1, skipna=True)>80].index.values
        data_sub_finished_valid = data_sub_finished[data_sub_finished['Wafer'].isin (wafer_list_valid)]
        failure_sum_valid = data_sub_finished_valid.groupby(['PBI_Lot','Failure_Cause'])['Chip'].count().reset_index().rename(columns={'Chip':'Failure_Count'})
        failure_pivot_valid = failure_sum_valid.pivot(index='PBI_Lot', columns='Failure_Cause', values='Failure_Count')
        return failure_pivot_valid
    elif argCount == 2:
        data_sub_finished = args[0]
        lot = args[1]
        data_lot = data_sub_finished[data_sub_finished['PBI_Lot']==lot]
        failure_sum = data_lot.groupby(['Wafer','Failure_Cause'])['Chip'].count().reset_index().rename(columns={'Chip':'Failure_Count'})
        failure_pivot = failure_sum.pivot(index='Wafer', columns='Failure_Cause', values='Failure_Count')
        return failure_pivot
    else:
        raise Exception('Too many args: {}'.format(args))


def generate_lot_summary_chart_alt():
    folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'

    data_name = 'all_chip_level_data.csv'
    data_raw = pd.read_csv( os.path.join(folder_path, data_name) )
    
    year = datetime.date.today().year
    ww = datetime.date.today().isocalendar()[1] - 1
    year_ww = str(year) + '-' + str(ww)
    
    data_raw['CCY_Status'].fillna(value = '0_NA', inplace = True )
    data_raw['CCY_Status_wafer'] = data_raw.groupby('Wafer')['CCY_Status'].transform(max)
    
    param_name_list = ['FE_Yield_A','FE_Yield_AC','PDP_Yield','BE_Yield','DS_Yield','EOLQC_Yield','FR_Yield', "CCY_A_Finished", "CCY_AC_Finished"]
    param_ww_list = ['FE_QC_Cumulative_WW','FE_QC_Cumulative_WW','PDPWorkWeekOut','BEWorkWeekOut','DSWorkWeekOut','EOLQCWorkWeekOut','FRWorkWeekIn', 'CCY_WorkWeek', 'CCY_WorkWeek']
    param_list = zip(param_name_list, param_ww_list)
    
    data_sum = pd.DataFrame()
    for prod in ['Sequel','Spider']:
        data = data_raw[data_raw['Product']==prod]
        for param in param_list:
           # data[param[1]].fillna(value = '0_NA', inplace = True )
            data_sub = data[~np.isnan(data[param[0]])]
            data_sub = data_sub[~pd.isna(data_sub[param[1]])]
            data_sub = data_sub.groupby('Wafer')
            data_sum[param[1]+'_wafer'] = data_sub[param[1]].max()
            data_sum[param[0]+'_count'] = data_sub[param[0]].count()
            data_sum[param[0]+'_sum'] = data_sub[param[0]].sum() 
            data_sum[param[0]+'_yield'] =  data_sum[param[0]+'_sum'] / data_sum[param[0]+'_count'] * 100
            data_sum['CCY_Status_wafer'] = data_sub['CCY_Status_wafer'].max()
            data_sum['PBI_Lot'] = data_sub['PBI_Lot'].max()
            
            fig = make_subplots(rows=1, cols=1)
            
            fig.add_trace(go.Scatter(x=data_sum['PBI_Lot'], y=data_sum[param[0]+'_yield'], mode="markers",  hovertext=data_sum.index + '<br>' + data_sum[param[1]+'_wafer'], hoverinfo="text",
                                          marker=dict(size=6,line=dict(width=2,color='DarkSlateGrey'),  opacity=0.7, cauto=True) ), row=1, col=1)  
            
            fig.update_layout(
                title=go.layout.Title(
                    text=prod + ' - ' + param[0],  xref="paper", x=0
                ),
                xaxis=go.layout.XAxis(
                    title=go.layout.xaxis.Title(
                        font=dict(family="Courier New, monospace", size=18,color="#7f7f7f")
                    )
                ),
                yaxis=go.layout.YAxis(
                    title=go.layout.yaxis.Title(
                        text=param[0],
                        font=dict(family="Courier New, monospace", size=18,color="#7f7f7f")
                    )
                )
            )
                        
            fig.show()
    
def get_EOLQC_chips(wafer):
    ####   get all functional test 
    #conn = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly') 
    if os.name == 'nt':
        conn = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  #camstarsql_mes
    else:
        server = 'usmp-vm-campd8d\sql2012ods'
        #database = 'CamODS_6X' 
        username = 'odbc' 
        password = 'readonly' 
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';UID='+username+';PWD='+ password)
       # conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    
    
    conn.autocommit = True
    get_data_sql =  ''' select pd.ContainerName as Chip, pd.DataValue,pd.DataName,pd.TxnDate
     from PBI_ParametricData pd
     --join (
     --   select containerName, Max(TxnDate) as maxdate
     --   from PBI_ParametricData
     --   where dataName ='moviename'  and DataCollectionDefName='EOL Sample Data' and pd.ContainerName like '{0}%'
     --   Group by containerName) as pd2 
     --on pd.containerName=pd2.containerName and pd.TxnDate=pd2.maxdate 
     where pd.DataCollectionDefName='EOL Sample Data'  and pd.ContainerName like '{0}%'
     '''.format(wafer)  #   and pd.TxnDate > '{0}' 
    data_raw = pd.read_sql(get_data_sql, conn)
    conn.close()
    # Replace SQL_getting_maxDate with Python scipts
    data_raw_1 = data_raw[data_raw['DataName']=='moviename'].groupby(['Chip'])['TxnDate'].max()
    # data_raw_2 = data_raw.apply(lambda x: 'y' if x['TxnDate'] == data_raw_1[x['Chip']] else 'n')
    data_raw_2 = data_raw.apply(lambda x: True if x['TxnDate'] == data_raw_1[x['Chip']] else False, axis=1)
    data_raw = data_raw[data_raw_2]
    
    
    
    data_pivot = data_raw.pivot_table(index=['Chip', 'TxnDate'], columns='DataName', values='DataValue', aggfunc=np.max).reset_index()

    ####   get EOLQC
    if os.name == 'nt':
        conn1 = pyodbc.connect('DSN=MESDWH;UID=DWView;PWD=dwview') 
    else:
        server = 'Usmp-vm-MESDWH1'
        username = 'DWView' 
        password = 'dwview' 
        conn1 = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';UID='+username+';PWD='+ password)
    conn1.autocommit = True
    get_data_sql =  ''' select * 
    from MESDWH.DWHView.GetMoveHistoryDetails
    where ToOperation='Functional test Complete'  and wafercontainername = '{0}'
     '''.format(wafer)  #and TransactionDateTime>{0}  wafercontainername = '{0}'
    data_EOLQC = pd.read_sql(get_data_sql, conn1)
    EOLQC_list = data_EOLQC['ContainerName'].tolist()

    def is_EOLQC(x):
        if x['Chip'] in (EOLQC_list):
            return 'EOLQC'
        else:
            return 'Eng Run'
    data_pivot['type'] = data_pivot.apply(is_EOLQC, axis=1)
    return data_pivot


def generate_quarter_yield(prod='Sequel', year = '2019', quarter = 3):
    ww_start = year + '-{:02}'.format(13 * (quarter-1) + 1)
    ww_end = year + '-{:02}'.format(13 * quarter + 1)
    folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'
    data_name = 'all_chip_level_data.csv'
    data_raw = pd.read_csv( os.path.join(folder_path, data_name) )
    data_valid = data_raw[(data_raw['Product']==prod) & (data_raw['FRWorkWeekOut']>=ww_start) & (data_raw['FRWorkWeekOut']<ww_end) ]
    data_yield = data_valid[data_valid['CCY_Status']=='7_Yielded']
    wafer_list = data_yield['Wafer'].unique().tolist()
    data_valid_wafer = data_raw[data_raw['Wafer'].isin(wafer_list)]
    data_valid_summary = data_valid_wafer.groupby(['Wafer', 'CCY_Status']).size().reset_index().rename(columns={0:'count'})
    data_valid_pivot = pd.pivot_table(data_valid_summary, values ='count', index='Wafer', columns='CCY_Status' , aggfunc={'count':sum}   )
#    data_valid_waferSum = pd.pivot_table(data_valid_summary, values ='count', index='Wafer', aggfunc={'count':sum}   )
    data_valid_pivot['total'] = 93
    data_valid_pivot['Yield %'] = data_valid_pivot['7_Yielded']  / data_valid_pivot['total']  * 100
    return data_valid_pivot
    
def generate_quarter_yield_thisYear(year = '2019'):
    folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'
    data_name = 'all_chip_level_data.csv'
    data_raw = pd.read_csv( os.path.join(folder_path, data_name) )
    data_summary = pd.DataFrame()
    for prod in ['Sequel','Spider']:
        for i in range(4):
            quarter = i + 1
            ww_start = year + '-{:02}'.format(13 * (quarter-1) + 1)
            ww_end = year + '-{:02}'.format(13 * quarter + 1)
            data_valid = data_raw[(data_raw['Product']==prod) & (data_raw['FRWorkWeekOut']>=ww_start) & (data_raw['FRWorkWeekOut']<ww_end) ]
            data_yield = data_valid[data_valid['CCY_Status']=='7_Yielded']
            wafer_list = data_yield['Wafer'].unique().tolist()
            data_valid_wafer = data_raw[data_raw['Wafer'].isin(wafer_list)]
            data_valid_summary = data_valid_wafer.groupby(['Wafer', 'CCY_Status']).size().reset_index().rename(columns={0:'count'})
            data_valid_pivot = pd.pivot_table(data_valid_summary, values ='count', index='Wafer', columns='CCY_Status' , aggfunc={'count':sum}   )
        #    data_valid_waferSum = pd.pivot_table(data_valid_summary, values ='count', index='Wafer', aggfunc={'count':sum}   )
            if prod =='Sequel':
                data_valid_pivot['total'] = 93
            else:
                data_valid_pivot['total'] = 87
            data_valid_pivot['Yield %'] = data_valid_pivot['7_Yielded']  / data_valid_pivot['total']  * 100
            data_valid_pivot['prod'] = prod
            data_valid_pivot['Quarter'] = quarter
            data_summary = data_summary.append(data_valid_pivot)
    return data_summary

#from util_liang import spc_util   
#q3_seq = util_liang.spc_util.generate_quarter_yield(prod='Sequel', year = '2019', quarter = 3)
#q3_spider = util_liang.spc_util.generate_quarter_yield(prod='Spider', year = '2019', quarter = 3)
#q4_seq = util_liang.spc_util.generate_quarter_yield(prod='Sequel', year = '2019', quarter = 4)
#q4_spider = util_liang.spc_util.generate_quarter_yield(prod='Spider', year = '2019', quarter = 4)
#    
    
    
    