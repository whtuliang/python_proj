# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 17:37:27 2019

@author: ltu
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import os
import pyodbc 



chip = 'AY16231-17-5050' #'AW33042-'
#chip = ('PW14001-02','PV45127-01','PV50027-06','PW24058-03','PW30037-01','PW15000-03','PV51024-23','PV50026-02','PW24058-21','PW30037-21','PW28028-04','PW12013-06','PW13000-16','PW28028-12','PW12013-19','PW14001-25','PW13000-01','PV50027-22','PW32035-03','PW32035-21','PV45057-03','PW15000-11','PW15000-25','PV50026-17','PV51024-08','PV48031-12','PV46001-03','PW17002-24','PW03018-06','PW02109-08','PW06145-16','PW15000-03','PW15000-15','PW09062-23','PW08074-06','PW19048-11','PW19048-17')
get_data_sql =  ''' SELECT  [MovedContainer]
      ,[Chip]
      ,[Substrate]
      ,[FromOperation]
      ,[ChipCurrentProduct]
      ,[MoveDate]
  FROM [CamODS_6X].[insitedb_schema].[View_MoveHistoryByOpAndDateRange]
  WHERE Chip like '{}%'
  ---WHERE MovedContainer in 
'''.format( chip)

conn1 = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly;Database=CamODS_6X')  #camstarsql_mes
conn1.autocommit = True
data_raw = pd.read_sql(get_data_sql, conn1)
data = data_raw.groupby('Chip').apply(lambda x:x.sort_values(by='MoveDate'))
conn1.close()

#data.to_csv('move_hist.csv')
# get movie info
#get_data_sql =  ''' select containername,DataName,DataValue,TxnDate from 
#[insitedb_schema].[PBI_ParametricData] where containername like'{}%' and DataName='moviename' '''.format(chip)
#data1 = pd.read_sql(get_data_sql, conn1)


#Get data from MES
get_data_sql =  ''' select distinct Chip, Substrate, FromOperation, MoveDate
      from [insitedb_schema].[View_MoveHistoryByOpAndDateRange]
      where FromOperation in ('Wet Bench')
      and MoveDate >= '2017-06-01' 
      order by MoveDate
'''
conn1 = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly;Database=CamODS_6X')  #camstarsql_mes
conn1.autocommit = True
data_raw = pd.read_sql(get_data_sql, conn1)
data = data_raw.groupby('Chip').apply(lambda x:x.sort_values(by='MoveDate'))
conn1.close()
data.to_csv('PDP_date.csv')