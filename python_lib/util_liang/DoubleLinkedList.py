# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 14:51:07 2020

@author: ltu
"""

class DoubleListNode:
    def __init__(self, value):
        self.value = value
        self.next = None
        self.pre = None
        
class DoubleLinkedList:
    def __init__(self):
        self.head = DoubleListNode(0)
        self.tail = DoubleListNode(0)
        self.head.next = self.tail
        self.tail.pre = self.head
        
        