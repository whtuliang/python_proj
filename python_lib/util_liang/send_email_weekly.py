# -*- coding: utf-8 -*-
"""
Created on Fri Oct  4 16:50:00 2019

@author: ltu
"""

# import the smtplib module. It should be included in Python by default
import smtplib
# import necessary packages
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# email_list = '''
# Jelena Pesic <jpesic@pacificbiosciences.com>; Mark Phillips <maphillips@pacificbiosciences.com>; Allan Bonilla <abonilla@pacificbiosciences.com>; Ann Gabrys <agabrys@pacificbiosciences.com>; Brian Porter <bporter@pacificbiosciences.com>;
# Hai Le <hle@pacificbiosciences.com>; Jinsong Gao <jgao@pacificbiosciences.com>; Kun Hou <khou@pacificbiosciences.com>; Loreto Cantillep <lcantillep@pacificbiosciences.com>; Minh Ngoc Tran <mntran@pacificbiosciences.com>; Nicolas Dyer <ndyer@pacificbiosciences.com>;
# Remy Santiago <rsantiago@pacificbiosciences.com>; Andrew Jae <ajae@pacificbiosciences.com>; Robert Beckman <rbeckman@pacificbiosciences.com>; Naining Yin <nyin@pacificbiosciences.com>; Shabbir Shahdawala <sshahdawala@pacificbiosciences.com>; Denis Zaccarin <DZaccarin@pacificbiosciences.com>;
# Mathieu Foquet <mfoquet@pacificbiosciences.com>; Ravi Saxena <rsaxena@pacificbiosciences.com>; Ezgi Evcik <eevcik@pacificbiosciences.com>; Mike Goloubef <mgoloubef@pacificbiosciences.com>
# '''

email_list = '''
Chip NPI <chipnpi@pacificbiosciences.com>; Chip EOL QC Group <chipeolqcgroup@pacificbiosciences.com>;
Loreto Cantillep <lcantillep@pacificbiosciences.com>;Andrew Jae <ajae@pacificbiosciences.com>; Robert Beckman <rbeckman@pacificbiosciences.com>;Shabbir Shahdawala <sshahdawala@pacificbiosciences.com>; Denis Zaccarin <DZaccarin@pacificbiosciences.com>; Mathieu Foquet <mfoquet@pacificbiosciences.com>; Ravi Saxena <rsaxena@pacificbiosciences.com>;  Mike Goloubef <mgoloubef@pacificbiosciences.com>
'''


def main(ww_num, names_emails=[(['Liang', 'Tu'], 'ltu@pacificbiosciences.com')]):  # read contacts  ,('Liang Tu','tuxxx038@umn.edu')
    # set up the SMTP server
    s = smtplib.SMTP(host='smtp-mail.outlook.com', port=587)
    s.starttls()
    s.login('ltu@pacificbiosciences.com', 'SDFjkl678')

    message_template = '''<pre>Dear Team, 
    
The yield report has been generated and can be found in the following location:
<a href="\\\\sharepoint\mfg\sequelcell\Documents\Manufacturing Engineering\Yield Reports\Weekly Yield Meetings\\2021\WW{0} ">SMRTCell Yield WW{0}</a>
    
Please add your slides as needed. Thanks!


Below is the brief Yield Summary:
    
{1}
    
Regards, 
Liang </pre> '''.format(ww_num, email_list)

    # For each contact, send the email:
    for name, email in names_emails:
        msg = MIMEMultipart()  # create a message

        # add in the actual person name to the message template
        message = message_template.replace('PERSON_NAME', name[0])

        # setup the parameters of the message
        msg['From'] = 'ltu@pacificbiosciences.com'
        msg['To'] = email
        msg['Subject'] = 'SMRTCell Yield WW' + ww_num

        # add in the message body
        msg.attach(MIMEText(message, 'html'))

        # send the message via the server set up earlier.
        s.send_message(msg)

        del msg


def get_group_emails():
    email_list_str = 'Dalia Ruiz <druiz@pacificbiosciences.com>; Jelena Pesic <jpesic@pacificbiosciences.com>; Mark Phillips <maphillips@pacificbiosciences.com>; Martin Desroches <mdesroches@pacificbiosciences.com>; Allan Bonilla <abonilla@pacificbiosciences.com>; Ann Gabrys <agabrys@pacificbiosciences.com>; Brian Porter <bporter@pacificbiosciences.com>; Hai Le <hle@pacificbiosciences.com>; Jinsong Gao <jgao@pacificbiosciences.com>; Kun Hou <khou@pacificbiosciences.com>; Loreto Cantillep <lcantillep@pacificbiosciences.com>; Ming Sun <msun@pacificbiosciences.com>; Minh Ngoc Tran <mntran@pacificbiosciences.com>; Nicolas Dyer <ndyer@pacificbiosciences.com>; Remy Santiago <rsantiago@pacificbiosciences.com>; Andrew Jae <ajae@pacificbiosciences.com>; Robert Beckman <rbeckman@pacificbiosciences.com>; Naining Yin <nyin@pacificbiosciences.com>; Shabbir Shahdawala <sshahdawala@pacificbiosciences.com>; Denis Zaccarin <DZaccarin@pacificbiosciences.com>; Mathieu Foquet <mfoquet@pacificbiosciences.com>'
    email_list = email_list_str.split(';')

    def split_email_info(x):
        x = x.strip()
        idx1 = x.find('<')
        idx2 = x.find('>')
        name = x[:idx1].strip().split(' ')
        email = x[idx1 + 1:idx2]
        return name, email

    return list(map(split_email_info, email_list))


def send_group_emails(ww_num):
    email_list_str = 'Dalia Ruiz <druiz@pacificbiosciences.com>; Jelena Pesic <jpesic@pacificbiosciences.com>; Mark Phillips <maphillips@pacificbiosciences.com>; Martin Desroches <mdesroches@pacificbiosciences.com>; Allan Bonilla <abonilla@pacificbiosciences.com>; Ann Gabrys <agabrys@pacificbiosciences.com>; Brian Porter <bporter@pacificbiosciences.com>; Hai Le <hle@pacificbiosciences.com>; Jinsong Gao <jgao@pacificbiosciences.com>; Kun Hou <khou@pacificbiosciences.com>; Loreto Cantillep <lcantillep@pacificbiosciences.com>; Ming Sun <msun@pacificbiosciences.com>; Minh Ngoc Tran <mntran@pacificbiosciences.com>; Nicolas Dyer <ndyer@pacificbiosciences.com>; Remy Santiago <rsantiago@pacificbiosciences.com>; Andrew Jae <ajae@pacificbiosciences.com>; Robert Beckman <rbeckman@pacificbiosciences.com>; Naining Yin <nyin@pacificbiosciences.com>; Shabbir Shahdawala <sshahdawala@pacificbiosciences.com>; Denis Zaccarin <DZaccarin@pacificbiosciences.com>; Mathieu Foquet <mfoquet@pacificbiosciences.com>'
    email_list = email_list_str.split(';')

    def split_email_info(x):
        x = x.strip()
        idx1 = x.find('<')
        idx2 = x.find('>')
        #        name = x[:idx1].strip().split(' ')
        email = x[idx1 + 1:idx2]
        return email

    email_list_str_new = ','.join((map(split_email_info, email_list)))

    main(ww_num, names_emails=[(['Team', 'NPI'], email_list_str_new)])


if __name__ == '__main__':
    main(ww_num='939')
