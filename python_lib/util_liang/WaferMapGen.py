# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 12:12:18 2019

@author: ltu
"""
import matplotlib
from matplotlib import pyplot as plt

plt.ioff()  # Turn interactive plotting off
import numpy as np
from util_liang.MyFunc import cart2pol
import pandas as pd
from util_liang.MyFunc import get_range
import os
from matplotlib.colors import LinearSegmentedColormap
import cv2


class WaferMap:
    wafer_mask = [[0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0], [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                  [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                  [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0], [0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0]]

    #    cmap = matplotlib.cm.brg   #viridis  gist_rainbow
    #    cmap.set_bad('white',alpha = 10.)

    #    cmap.set_bad('white',alpha = 10.)

    def __init__(self, n=11):
        self.n = n
        self.unit_plot = 1000 // n / 1000
        self.clim_min, self.clim_max = 0, 0

        #        from collections import defaultdict
        #        colors = [(0,0,0,0),(1/100,1,0,0),(5/100,255/255,165/255,0),(10/100,1,1,0),(23/100,0,1,0),(33/100,0,1,1),(43/100,100/255,149/255,237/255),(48/100,0,0,128/255),(53/100,0,0,1),(63/100,128/255,0,128/255),(73/100,1,1,1),(1,1,1,1)]
        #        cdict = defaultdict(set)
        #        for i in range(len(colors)-1):
        #            cdict['red'].add((colors[i][0], colors[i][1], colors[i+1][1]))
        #            cdict['green'].add((colors[i][0], colors[i][2], colors[i+1][2]))
        #            cdict['blue'].add((colors[i][0], colors[i][3], colors[i+1][3]))
        #        self.p1_color = LinearSegmentedColormap('p1_color', cdict)

        # colors = [(1, 0, 0), (0, 1, 0), (0, 0, 1)]
        colors = [(0, (0, 0, 0)), (2 / 100, (1, 0, 0)), (10 / 100, (255 / 255, 165 / 255, 0)), (15 / 100, (1, 1, 0)),
                  (30 / 100, (0, 1, 0)), (50 / 100, (0, 1, 1)),
                  (60 / 100, (100 / 255, 149 / 255, 237 / 255)), (70 / 100, (30 / 255, 144 / 255, 255 / 255)),
                  (80 / 100, (0, 0, 1)), (98 / 100, (128 / 255, 0, 128 / 255)), (1, (1, 1, 1))]
        self.cm = LinearSegmentedColormap.from_list('my_list', colors)

    #  plt.register_cmap(name='p1_color', data=cdict)
    #  self.cmap = plt.get_cmap('p1_color')

    def set_cm(self, prod):
        if os.name == 'nt':
            color_map = pd.read_csv(r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\data-temp\color_map_etienne.csv',
                                    engine='python')
        else:
            color_map = pd.read_csv(
                r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\data-temp\color_map_etienne.csv'.replace('\\', '/'),
                encoding="ISO-8859-1")
        if prod == 'Spider':
            cm = color_map.iloc[1:, :3].values
            cm = [[float(x[0]), float(x[1]), float(x[2])] for x in cm]
        else:
            cm = color_map.iloc[1:66, 4:].values
            cm = [[float(x[0]), float(x[1]), float(x[2])] for x in cm]
        colors = []
        n = len(cm) - 1
        for idx, val in enumerate(cm):
            colors.append((idx / n, tuple(val)))
        self.cm = LinearSegmentedColormap.from_list('my_list', colors)

    def create_wafer_map(self, title=None):
        self.fig, self.axes = plt.subplots(self.n, self.n, sharex='none', sharey='none', figsize=(10, 10), squeeze=True)
        plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0, hspace=0)
        for i, ax_row in enumerate(self.axes):
            for j, ax in enumerate(ax_row):
                ax.set_zorder(-1)
                ax.set_xticks([])
                ax.set_yticks([])
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                ax.spines['bottom'].set_visible(False)
                ax.spines['left'].set_visible(False)
                if self.wafer_mask[i][j] == 1:
                    jj = j + 45
                    ii = 55 - i
                    ax.text(0.5, 0.5, str(jj) + str(ii), fontsize=14, ha='center')
        if title:
            self.fig.suptitle(title, fontsize=20, x=0.5, y=0.95, ha='center', va='top')
        return self.fig

    def img_ind(self, data, jj, ii, title=None, clim=None, filter_img=None, vmin=0, vmax=1):
        ax = self.axes[55 - ii][jj - 45]
        if len(ax.texts) >= 1:
            ax.texts[0].set_visible(False)
        else:
            print('error: ' + str(jj) + str(ii))
        #        if not isinstance(data,PIL.GifImagePlugin.GifImageFile):
        #            data = self.normalize_int_255(data)
        if filter_img == 'Median':
            data = cv2.medianBlur(np.uint8(data), 5)
        data[0][0] = 100
        self.im = ax.imshow(data, cmap=self.cm, vmin=vmin, vmax=vmax)  # self.cmap
        if clim == None:
            self.im.set_clim(0, 100)
        #            self.im.set_clim(get_range(data))
        else:
            self.clim_min = min(self.clim_min, clim[0])
            self.clim_max = max(self.clim_max, clim[1])
            self.im.set_clim([self.clim_min, self.clim_max])

    def scatter_ind(self, data, jj, ii, title=None, clim=None):
        ax = self.axes[55 - ii][jj - 45]
        if len(ax.texts) >= 1:
            ax.texts[0].set_visible(False)
        else:
            print('error: ' + str(jj) + str(ii))
        self.im = ax.scatter(data[0], data[1])

    def plotxy_ind(self, x, y, jj, ii, title=None, clim=None):
        ax = self.axes[55 - ii][jj - 45]
        if len(ax.texts) >= 1:
            ax.texts[0].set_visible(False)
        else:
            print('error: ' + str(jj) + str(ii))
        self.im = ax.plot(x, y)

    def normalize_int_255(self, data):
        data_max = np.nanmax(data)
        data = data / data_max
        return data

    def show_boarder(self, jj, ii, color='black', line='--'):
        ax = self.axes[55 - ii][jj - 45]
        for side in ['left', 'right', 'top', 'bottom']:
            ax.spines[side].set_visible(True)
            ax.spines[side].set_linewidth(3)
            ax.spines[side].set_color(color)
            ax.spines[side].set_position(('outward', 2))
            ax.spines[side].set_linestyle(line)
            ax.spines[side].set_zorder(1)

    def show_cbar(self, pos=[0.85, 0.7, 0.02, 0.2], title=''):
        try:
            cb_ax = self.fig.add_axes(pos)
            cbar = self.fig.colorbar(self.im, cax=cb_ax)
            cbar.set_label(title, labelpad=-20, y=1.1, rotation=0)
        except AttributeError:
            cb_ax = self.fig.add_axes(pos)


#            cbar.set_label(title, labelpad=-20, y=1.1, rotation=0)
#        norm = matplotlib.colors.Normalize(vmin=0, vmax=200)
#        m = matplotlib.cm.ScalarMappable(norm=norm, cmap=self.cmap)
#        cbar = self.fig.colorbar(m.to_rgba([0,100,200]), cax=cb_ax)
#        cbar.set_ticks(np.arange(10, 81, 30))
#        cbar.set_ticklabels(['low', 'medium', 'high'])

class ChipMap:
    def __init__(self):
        self.clim_min, self.clim_max = 0, 0
        self.fig, self.ax = plt.subplots(1, 1, figsize=(6, 6), squeeze=True)


    def set_cm(self, prod):
        if os.name == 'nt':
            color_map = pd.read_csv(r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\data-temp\color_map_etienne.csv',
                                    engine='python')
        else:
            color_map = pd.read_csv(
                r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\data-temp\color_map_etienne.csv'.replace('\\', '/'),
                encoding="ISO-8859-1")
        if prod == 'Spider':
            cm = color_map.iloc[1:, :3].values
            cm = [[float(x[0]), float(x[1]), float(x[2])] for x in cm]
        else:
            cm = color_map.iloc[1:66, 4:].values
            cm = [[float(x[0]), float(x[1]), float(x[2])] for x in cm]
        colors = []
        n = len(cm) - 1
        for idx, val in enumerate(cm):
            colors.append((idx / n, tuple(val)))
        self.cm = LinearSegmentedColormap.from_list('my_list', colors)

    def img_ind(self, data, title=None, clim=None):
        self.im = self.ax.imshow(data, cmap=self.cm)  # self.cmap
        if clim == None:
            self.im.set_clim(0, 100)
        #            self.im.set_clim(get_range(data))
        else:
            self.clim_min = min(self.clim_min, clim[0])
            self.clim_max = max(self.clim_max, clim[1])
            self.im.set_clim([self.clim_min, self.clim_max])
        if title:
            self.fig.suptitle(title, fontsize=20, x=0.5, y=0.95, ha='center', va='top')

    def show_cbar(self, pos=[1.05, 0.6, 0.02, 0.2], title=''):
        try:
            cb_ax = self.fig.add_axes(pos)
            cbar = self.fig.colorbar(self.im, cax=cb_ax)
            cbar.set_label(title, labelpad=-20, y=1.1, rotation=0)
        except AttributeError:
            cb_ax = self.fig.add_axes(pos)


def radius_prof(data):
    i_len, j_len = data.shape
    i_mid = i_len // 2
    j_mid = j_len // 2
    # result = pd.DataFrame(columns=['rho','value'])
    rho_arr, value_arr = [], []
    for i in range(i_len):
        for j in range(j_len):
            rho, phi = cart2pol(i - i_mid, j - j_mid)
            rho_arr.append(rho)
            value_arr.append(data[i, j])
    return rho_arr, value_arr


def radius_max(xm, ym):
    data_raw = pd.DataFrame(np.array([xm, ym]).T, columns=['radius', 'value'])
    data_raw['radius_n'] = data_raw['radius'].apply(lambda x: round(x))
    return None


def get_wafer_image(wafer):
    from util_liang.spc_util import get_prodtype
    prod = get_prodtype(wafer)
    if prod == 'Sequel':
        file_path = os.path.join(r'\\pbi\dept\systems\emcc\WaferId', wafer, wafer + '_P1_FromField.gif')
    else:
        file_path = os.path.join(r'\\pbi\dept\systems\emcc\Spider\WaferId', wafer, wafer + '_P1_FromField.gif')
    return file_path


def get_wafer_HTMLimage(wafer):
    from util_liang.spc_util import get_prodtype
    prod = get_prodtype(wafer)
    if prod == 'Sequel':
        file_path = os.path.join(r'http://usmp-vm-sequelwebapp/SequelWaferMap', wafer, wafer + '_P1_FromField.gif')
    else:
        file_path = os.path.join(r'http://usmp-vm-sequelwebapp/SpiderWaferMap', wafer, wafer + '_P1_FromField.gif')
    return file_path
