# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 01:35:38 2020

@author: ltu
"""

# import pandas as pd
# import numpy as np
import pyodbc
# import matplotlib
# import matplotlib.pyplot as plt
# import seaborn as sb
# from sklearn import preprocessing
# import matplotlib.patheffects as path_effects
# import datetime
import os


def connect_db(db_source):
    conn = None
    if db_source == 'MES':
        if os.name == 'nt':
            conn = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly;Database=CamODS_6X')  # camstarsql_mes
        else:
            server = 'usmp-vm-campd8d\sql2012ods'
            #            database = 'CamODS_6X'
            username = 'odbc'
            password = 'readonly'
            conn = pyodbc.connect(
                'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)  # ';DATABASE='+database+
    elif db_source == 'SpiderMetrology':
        if os.name == 'nt':
            conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')
        else:
            server = 'camstarsql\sql2012'
            username = 'odbc'
            password = 'readonly'
            conn = pyodbc.connect(
                'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)
    elif db_source == 'MESDWH':
        if os.name == 'nt':
            conn = pyodbc.connect('DSN=MESDWH;UID=odbc;PWD=readonly')  # camstarsql_mes
        else:
            server = 'Usmp-vm-MESDWH1'
            database = 'MESDWH'
            username = 'DWView'
            password = 'dwview'
            conn = pyodbc.connect(
                'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)

    return conn


def get_netdrive_url(directory):
    if os.name != 'nt':
        return directory.replace('\\', '/')
    else:
        return directory
