# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 16:15:20 2020

@author: ltu
"""

from xvfbwrapper import Xvfb
import pdfkit


vdisplay = Xvfb()
vdisplay.start()

# launch stuff inside
# virtual display here.
pdfkit.from_file('/home/ltu/Python/EOLQC_Report/test.html','test.pdf')

vdisplay.stop()
