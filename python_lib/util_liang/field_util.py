# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 10:42:55 2019

@author: ltu
"""
import warnings

warnings.filterwarnings("ignore")
# warnings.filterwarnings("ignore", category=FutureWarning)
import zipfile
from matplotlib import pyplot as plt
import datetime, pyodbc
import pandas as pd
import numpy as np
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
package_path = os.path.dirname(dir_path)
sp_path = os.path.join(package_path, 'site_packages')
if package_path not in os.sys.path:
    os.sys.path.append(package_path)
if sp_path not in os.sys.path:
    os.sys.path.append(sp_path)
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import urllib, json
from pandas.io.json import json_normalize
from IPython.display import display, HTML, Markdown

today = datetime.datetime.today()
# date_start1 = datetime.date(today.year,today.month-3,1)
date_start = (today - datetime.timedelta(days=90)).date()
acquisition_list = ['Complete', 'Failed', 'Minilid', 'Hard_Elec', 'Soft_Elec']


def context_to_time(context):
    time = context.split('_')[1]
    year = int(time[0:2]) + 2000
    month = int(time[2:4])
    day = int(time[4:6])
    date = datetime.date(year, month, day)
    week = date.isocalendar()[1]
    return year, month, day, week, date


def modify_acquisition_status(s, Minilid_mode, failure_mode_hard, failure_mode_soft, user_abort_mode):
    if s.iloc[1] in Minilid_mode:
        return 'Minilid'
    elif s.iloc[1] in failure_mode_hard:
        return 'Hard_Elec'
    elif s.iloc[1] in failure_mode_soft:
        return 'Soft_Elec'
    elif s.iloc[1] in user_abort_mode:
        return 'Aborted_User'
    else:
        return s.iloc[0]


def generate_failure_mode(data):
    failure_mode_list = pd.read_csv(os.path.join(package_path, 'util_liang', 'Failure_Mode_List.csv'), dtype=str)
    failure_mode_hard_search = list(
        failure_mode_list[failure_mode_list['Hard/Soft'] == 'Hard']['Search_String'].unique())
    failure_mode_soft_search = list(
        failure_mode_list[failure_mode_list['Hard/Soft'] == 'Soft']['Search_String'].unique())
    # Minilid Failure: 
    # hard_failure_group = data['hard_failure_cause_group'].unique()
    # Minilid_mode_group = hard_failure_group[pd.Series(hard_failure_group).str.contains('.+Minilid.+').fillna(False)]
    data['hard_failure_cause'] = data['hard_failure_cause'].fillna('NaN')
    hard_failure = list(data['hard_failure_cause'].unique())
    # Minilid_mode = hard_failure[pd.Series(hard_failure).str.contains('.+mini-lid did not successfully engage.+').fillna(False)]
    Minilid_mode = list(filter(lambda x: 'mini-lid did not successfully engage' in x, hard_failure))
    failure_mode_hard, failure_mode_soft = [], []
    for item in failure_mode_hard_search:
        failure_mode_hard.extend(list(filter(lambda x: item in x, hard_failure)))
    for item in failure_mode_soft_search:
        failure_mode_soft.extend(list(filter(lambda x: item in x, hard_failure)))
    user_abort_mode = list(
        filter(lambda x: 'Collection aborted due to: The user has requested to abort the run' in x, hard_failure))
    data['acquisition_status'] = data[['acquisition_status', 'hard_failure_cause']].\
        apply(modify_acquisition_status, args=(Minilid_mode, failure_mode_hard, failure_mode_soft, user_abort_mode), axis=1)
    return data


def fieldData_mapTo_wafer(data):
    #    Substrate --> Wafer, Chip, Tray, Pos
    # ------------------------------------------------------------------
    #    The query below is outdated.
    #    if os.name == 'nt':
    #        conn1 = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  #camstarsql_mes spidermetrology
    #        os.chdir(r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib')
    #    else:
    #        server = 'usmp-vm-campd8d\sql2012ods'
    #       # database = 'CamODS_6X'
    #        username = 'odbc'
    #        password = 'readonly'
    #        conn1 = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';UID='+username+';PWD='+ password) #';DATABASE='+database+
    #        os.chdir('//pbi/dept/chip/ChipEng/Liang/Python-Lib')
    #    conn1.autocommit = True
    #
    #    text = open(os.path.join('util_liang',"ChipsInTrayQuery_New.txt"), "rb").read().decode()
    #    tr_data_init = pd.read_sql(text, conn1)
    #    tr_data_pos = pd.melt(tr_data_init[['Wafer','Tray','Pos1Index','Pos2Index','Pos3Index','Pos4Index']], id_vars=['Wafer','Tray'], var_name='Parameter_pos',  value_name='Chip')
    #    tr_data_pos['Pos'] =  tr_data_pos['Parameter_pos'].apply(lambda x:x[3])
    #    tr_data_sub = pd.melt(tr_data_init[['Wafer','Tray','Pos1Substrate','Pos2Substrate','Pos3Substrate','Pos4Substrate']], id_vars=['Wafer','Tray'], var_name='Parameter_sub',  value_name='Substrate')
    #    tr_data_sub['Pos'] =  tr_data_sub['Parameter_sub'].apply(lambda x:x[3])
    #    tr_data_1 = tr_data_pos.merge(tr_data_sub, left_on=['Wafer','Tray','Pos'],right_on=['Wafer','Tray','Pos'])
    #    tr_data_2 = tr_data_1[~tr_data_1.Wafer.str.contains('.+_FTSet_.+')]
    #    tr_data_2.dropna(inplace=True)
    #    tr_data_2.drop(columns=['Parameter_pos', 'Parameter_sub'], inplace=True)
    #    tr_data_2.rename({'Pos':'Pos_ChipInTray'}, inplace=True)
    #    data = data.merge(tr_data_2, left_on=['Cell_SerialNumber'],right_on=['Substrate'], how='left')
    #    data.drop(columns=['Cell_SerialNumber'])
    #    del tr_data_pos, tr_data_sub, tr_data_1, tr_data_2
    # ------------------------------------------------------------------
    #   If necessary, use the new query below
    #   ServerName: Usmp-vm-MESDWH1
    #   DataBaseName: MESDWH
    #   UserName = DWView
    #   PWD: Use the password you already have use for DWView
    #   ViewName: SMRTFleet.TrayDetailsBySubstrate
    #   select * from SMRTFleet.TrayDetailsBySubstrate  where SubstrateName= ‘CA042387’

    data = data.rename(columns={'Cell_SerialNumber': 'Substrate', 'ChipId': 'Chip', 'WaferId': 'Wafer'})
    from util_liang import spc_util
    data_PBI_SA = spc_util.chip_PBI_SA_status()
    data = data.merge(data_PBI_SA, left_on=['Chip'], right_on=['Chip'], how='left')
    #    data = data[~data['CCY_Status'].isin(['2_Fail','8_MechTray'])]
    return data


def load_field_data(year=2018, month=1, date=1):
    path_folder = r'\\pbi\dept\chip\ChipEng\Liang\Field Data'.replace('\\', '/')
    data_seq = pd.read_pickle(os.path.join(path_folder, 'Sequel.pkl'))
    data_spider = pd.read_pickle(os.path.join(path_folder, 'Spider.pkl'))
    data = data_seq.append(data_spider)
    data1 = data[data['acquisition_status'].isin(acquisition_list)]
    data1 = data1[data1['date'] >= datetime.date(year, month, date)]
    return data1


def save_field_data(data, save_path):
    data.to_csv(save_path + r'.csv')
    data.to_pickle(save_path + r'.pkl')


class FieldPlotly:
    def __init__(self, data, region):
        self.region = region
        if region == 'World':
            self.data1 = data
        else:
            self.data1 = data[data['Account Region'] == region]
        # self.data1.loc[:,'full_name'] = self.data1['full_name'].apply(lambda x: x[0:20])
        self.prod_list = ['Sequel', 'Spider']
        self.prod_color = {'Sequel': 'blue', 'Spider': 'red'}

    def display_total_volume(self):
        chip_count = self.data1.groupby(['Instr_Type']).size().reset_index(name='counts')
        display(Markdown('<span style=\'color:red\'>* ' + ': Chip Total Volume </span>'))
        display(HTML(chip_count.to_html(index=False)))
        display(HTML('<br>'))

    def plotly_failure_month(self):
        chip_count = self.data1.groupby(['Instr_Type', 'Yr-Mth'])['acquisition_status'].count().reset_index()

        chip_status_count = self.data1.groupby(['Instr_Type', 'Yr-Mth', 'acquisition_status']).size().reset_index(
            name='counts')
        df = pd.pivot_table(chip_status_count, index=['Instr_Type', 'Yr-Mth'], columns='acquisition_status',
                            values='counts', aggfunc=np.sum).reset_index()
        for chip_status in acquisition_list:
            if chip_status not in df.columns:
                df[chip_status] = 0
        df = df.fillna(0)
        df = df.merge(chip_count, left_on=['Instr_Type', 'Yr-Mth'], right_on=['Instr_Type', 'Yr-Mth'])
        df['%Fails'] = (df['Failed'] + df['Minilid'] + df['Hard_Elec'] + df['Soft_Elec']) / (
            df['acquisition_status']) * 100
        df['%Fails_Minilid'] = (df['Minilid']) / (df['acquisition_status']) * 100
        df['%Fails_Elec'] = (df['Hard_Elec'] + df['Soft_Elec']) / (df['acquisition_status']) * 100
        df['%Fails_Elec_Hard'] = (df['Hard_Elec']) / (df['acquisition_status']) * 100
        df['%Escape'] = (df['Hard_Elec'] + df['Minilid']) / (df['acquisition_status']) * 100

        def gen_failure_list(x, para):
            df_sub = self.data1[
                (self.data1['Yr-Mth'] == x['Yr-Mth']) & (self.data1['Instr_Type'] == x['Instr_Type']) & (
                        self.data1['acquisition_status'] == para)]
            return df_sub['Chip'].tolist()

        df['Minilid_List'] = df.apply(lambda x: gen_failure_list(x, para='Minilid'), axis=1)
        df['HardElec_List'] = df.apply(lambda x: gen_failure_list(x, para='Hard_Elec'), axis=1)

        def gen_href(x):
            if isinstance(x, list):
                output = 'Chip:'
                for item in x:
                    #             output += "<a href= https://github.com>" + item + "</a>"
                    output += "<br> " + str(item)
                return output
            else:
                return x

        property_iter = [("acquisition_status", 'acquisition_status', 'Field Volume', 'Chip #'),
                         ('%Fails_Minilid', 'Minilid_List', 'Minilid_Failure_Rate', 'Failure %'),
                         ('%Fails_Elec_Hard', 'HardElec_List', 'HardElec_Failure_Rate',
                          'Failure %')]  # (field, hovertext, title, y_axis)

        for property in property_iter:
            fig = make_subplots(rows=1, cols=1)
            for prod in self.prod_list:
                df1 = df[df['Instr_Type'] == prod]

                fig.add_trace(go.Scatter(x=df1["Yr-Mth"], y=df1[property[0]], mode='lines',
                                         line=dict(color=self.prod_color[prod]),
                                         name=prod), row=1, col=1)
                fig.add_trace(go.Scatter(x=df1["Yr-Mth"], y=df1[property[0]], mode="markers", hovertext=list(
                    map(lambda x, y: '{:.3f}'.format(x) + '%<br>' + y if isinstance(y, str) else str(x),
                        df1[property[0]].tolist(), df1[property[1]].apply(gen_href).tolist())), hoverinfo="text",
                                         marker=dict(size=6, line=dict(width=2, color='DarkSlateGrey'),
                                                     color=self.prod_color[prod]),
                                         opacity=0.7, showlegend=False), row=1, col=1)

                #     fig = px.line(chip_count, x="Yr-Mth", y="acquisition_status",   color='Instr_Type')

            fig.update_layout(
                title=go.layout.Title(
                    text=property[2], xref="paper", x=0
                ),
                xaxis=go.layout.XAxis(
                    title=go.layout.xaxis.Title(
                        #                 text="x Axis",
                        font=dict(family="Courier New, monospace", size=18, color="#7f7f7f")
                    )
                ),
                yaxis=go.layout.YAxis(
                    title=go.layout.yaxis.Title(
                        text=property[3],
                        font=dict(family="Courier New, monospace", size=18, color="#7f7f7f")
                    )
                )
            )

            fig.show()

        data2 = self.data1[
            (self.data1['date'] >= date_start) & (self.data1['acquisition_status'].isin(['Minilid', 'Hard_Elec']))]
        data2 = data2[['Instr_Type', 'date', 'acquisition_status', 'Chip', 'MovieContext',
                       'hard_failure_cause']]  # 'week',,'Cell_ExpirationDate','full_name',

        headerColor = 'grey'
        rowEvenColor = 'lightgrey'
        rowOddColor = 'white'
        fig = go.Figure(data=[go.Table(
            columnwidth=[5, 8, 5, 12, 16, 40],
            header=dict(
                values=list(data2.columns),
                line_color='darkslategray',
                fill_color=headerColor,
                align=['left', 'center'],
                font=dict(color='white', size=12)
            ),
            cells=dict(
                values=data2.values.transpose().tolist(),
                line_color='darkslategray',
                # 2-D list of colors for alternating rows
                fill_color=[[rowOddColor, rowEvenColor, rowOddColor, rowEvenColor, rowOddColor, rowEvenColor] * 5],
                align=['left', 'center'],
                font=dict(color='darkslategray', size=11)
            ))
        ])

        fig.show()

    def plotly_lowPerf_month(self):

        data = self.data1
        df = data[(data['acquisition_status'] == 'Complete') & (data['MovieMinutes'].astype('float') > 600)]
        chip_count = df.groupby(['Instr_Type', 'Yr-Mth'])['acquisition_status'].count().reset_index().rename(
            columns={'acquisition_status': 'Count'})
        df_low = df[df['total_bases'].astype('float') < 2e9]
        chip_low_count = df_low.groupby(['Instr_Type', 'Yr-Mth'])['acquisition_status'].count().reset_index().rename(
            columns={'acquisition_status': 'Count_low'})
        df = chip_count.merge(chip_low_count, left_on=['Instr_Type', 'Yr-Mth'], right_on=['Instr_Type', 'Yr-Mth'])
        df['%Low_Performance'] = (df['Count_low']) / (df['Count']) * 100

        property_iter = [("%Low_Performance", '%Low_Performance', 'Low_Performance % (2Gb)',
                          '%Low_Performance')]  # (field, hovertext, title, y_axis)

        for property in property_iter:
            fig = make_subplots(rows=1, cols=1)
            for prod in self.prod_list:
                df1 = df[df['Instr_Type'] == prod]

                fig.add_trace(go.Scatter(x=df1["Yr-Mth"], y=df1[property[0]], mode='lines',
                                         line=dict(color=self.prod_color[prod]),
                                         name=prod), row=1, col=1)
                fig.add_trace(go.Scatter(x=df1["Yr-Mth"], y=df1[property[0]], mode="markers",
                                         marker=dict(size=6, line=dict(width=2, color='DarkSlateGrey'),
                                                     color=self.prod_color[prod]),
                                         opacity=0.7, showlegend=False), row=1, col=1)

                #     fig = px.line(chip_count, x="Yr-Mth", y="acquisition_status",   color='Instr_Type')

            fig.update_layout(
                title=go.layout.Title(
                    text=property[2], xref="paper", x=0
                ),
                xaxis=go.layout.XAxis(
                    title=go.layout.xaxis.Title(
                        #                 text="x Axis",
                        font=dict(family="Courier New, monospace", size=18, color="#7f7f7f")
                    )
                ),
                yaxis=go.layout.YAxis(
                    title=go.layout.yaxis.Title(
                        text=property[3],
                        font=dict(family="Courier New, monospace", size=18, color="#7f7f7f")
                    )
                )
            )

            fig.show()

        data2 = self.data1[
            (self.data1['MovieMinutes'].astype('float') > 600) & (self.data1['total_bases'].astype('float') < 2e9) & (
                    self.data1['date'] > date_start)]

        # find wafers with many low performance chips
        wafer_list = data2['Wafer'].unique().tolist()
        data2_allwafer = self.data1[self.data1['Wafer'].isin(wafer_list)]
        data2_lowPerf_hist = data2_allwafer[data2_allwafer['total_bases'].astype('float') < 2e9].groupby(
            ['Instr_Type', 'Wafer', 'Cell_Lot', 'PBI_Lot', 'SALot', 'Cell_PartNumber']).size().reset_index().rename(
            columns={0: 'Count'})
        # data2_lowPerf_hist.sort_values(by='Count', ascending=False)

        for prod in self.prod_list:

            display(Markdown('<span style=\'color:red\'>* ' + prod + ': Top 10 wafers with low performance </span>'))
            data2_lowPerf_hist_prod = data2_lowPerf_hist[data2_lowPerf_hist['Instr_Type'] == prod].sort_values(
                by='Count', ascending=False)[0:10]
            display(HTML(data2_lowPerf_hist_prod.to_html(index=False)))
            wafer_worst = data2_lowPerf_hist_prod.iloc[0, :]['Wafer']

            data3 = data2[data2['Instr_Type'] == prod]

            def low_perf_cat(x):
                if x < 1e6:
                    return '<1Mb'
                elif x < 1e9:
                    return '1Mb to 1Gb'
                elif x < 2e9:
                    return '1Gb to 2Gb'
                else:
                    return '2Gb to 10Gb'

            data3.loc[:, 'total_bases_bin'] = data3['total_bases'].astype('float').apply(
                low_perf_cat)  # lambda x: '<1Mb'  if x<1e6 else('1Mb to 1Gb'  if x<1e9 else ('2 to 10Gb' if x<2e9 '1 to 2Gb') )

            def lon_offset(x):
                return {
                    '<1Mb': 0,
                    '1Mb to 1Gb': 0.1,
                    '1Gb to 2Gb': 0.2,
                    '2Gb to 10Gb': 0.3,
                }.get(x, 0)

            data3.loc[:, 'location.lon.new'] = data3.apply(
                lambda x: x['location.lon'] + lon_offset(x['total_bases_bin']), axis=1)

            data4 = data3.groupby(['Yr-Mth', 'total_bases_bin', 'full_name', 'location.lon.new',
                                   'location.lat']).size().reset_index().rename(columns={0: 'Count'})

            data41 = data4.groupby('total_bases_bin')['Count'].mean().reset_index(name='Count')
            data41['Yr-Mth'] = '0'
            data5 = data4.append(data41)
            data5.sort_values(by='Yr-Mth', inplace=True)

            fig = px.scatter_geo(data5, lon="location.lon.new", lat='location.lat',
                                 color="total_bases_bin",  # which column to use to set the color of markers
                                 hover_name="full_name",  # column added to hover information
                                 size="Count",  # size of markers
                                 animation_frame="Yr-Mth",
                                 color_discrete_map={'<1Mb': 'red', '1Mb to 1Gb': 'purple', '1Gb to 2Gb': 'yellow',
                                                     '2Gb to 10Gb': 'lightskyblue'},
                                 projection="natural earth",
                                 title=prod + ': Low Performance in Field')
            fig.show()

            #            display(HTML(data2_lowPerf_hist_prod['Wafer']))
            df_lowPerf = self.data1[
                (self.data1['Wafer'] == wafer_worst) & (self.data1['total_bases'].astype('float') < 2e9)]
            df_lowPerf = df_lowPerf[
                ['Instr_Type', 'full_name', 'Wafer', 'Chip', 'MovieMinutes', 'prodP1_pct', 'reads_length',
                 'controlReads_count', 'Yr-Mth', 'total_bases']]

            # df_lowPerf = data3.loc[data3['date']>date_start, ['Instr_Type','full_name','Wafer','Chip','MovieMinutes','prodP1_pct', 'reads_length','controlReads_count','Yr-Mth', 'total_bases'] ]
            df_lowPerf = df_lowPerf.fillna(-1)
            df_lowPerf = df_lowPerf.astype({'prodP1_pct': 'float', 'reads_length': 'float', 'total_bases': 'float'})
            df_lowPerf['total_bases'] = df_lowPerf['total_bases'] / 1e6
            df_lowPerf = df_lowPerf.astype({'prodP1_pct': 'int', 'reads_length': 'int', 'total_bases': 'int'})
            df_lowPerf = df_lowPerf.sort_values(by=['Chip'])
            df_lowPerf = df_lowPerf.rename(
                columns={"Instr_Type": "Instr", "prodP1_pct": "P1 %", "reads_length": "reads length",
                         "controlReads_count": "ctrl count", "total_bases": "total bases (Mb)"})
            # df_lowPerf_top10 = df_lowPerf[df_lowPerf['Wafer'].isin(data2_lowPerf_hist_prod['Wafer'])]
            # df_lowPerf_top10 = df_lowPerf_top10.drop(columns=['Wafer'])
            display(HTML(df_lowPerf.to_html(index=False)))
            display(HTML('<br><br><br><br><br><br>'))

        return data2_lowPerf_hist_prod

    def plotly_status_month(self):
        def lon_offset(x):
            return {
                'Complete': 0,
                'Failed': 0.1,
                'Soft_Elec': 0.2,
                'Hard_Elec': 0.3,
                'Minilid': 0.4
            }.get(x, 0)

        for prod in self.prod_list:
            data1 = self.data1[self.data1['Instr_Type'] == prod]
            data1['location.lon.new'] = data1.apply(lambda x: x['location.lon'] + lon_offset(x['acquisition_status']),
                                                    axis=1)
            data1 = data1.groupby(['Yr-Mth', 'acquisition_status', 'full_name', 'location.lon.new',
                                   'location.lat']).size().reset_index().rename(columns={0: 'Count'})

            data41 = data1.groupby('acquisition_status')['Count'].mean().reset_index(name='Count')
            data41['Yr-Mth'] = '0'
            data5 = data1.append(data41)
            data5.sort_values(by='Yr-Mth', inplace=True)

            fig = px.scatter_geo(data5, lon="location.lon.new", lat='location.lat',
                                 color="acquisition_status",  # which column to use to set the color of markers
                                 hover_name="full_name",  # column added to hover information
                                 size="Count",  # size of markers
                                 animation_frame="Yr-Mth",
                                 color_discrete_map={'Complete': 'lightskyblue', 'Failed': 'coral',
                                                     'Soft_Elec': 'yellow', 'Hard_Elec': 'purple', 'Minilid': 'red'},
                                 projection="natural earth",
                                 title=prod)
            fig.show()

    def plotly_chipfailure_month(self):
        data2 = self.data1[self.data1['acquisition_status'].isin(['Minilid', 'Hard_Elec'])]
        data2.loc[:, 'full_name'] = data2['full_name'].apply(lambda x: x[0:20])
        data3 = data2.groupby(
            ['Instr_Type', 'Yr-Mth', 'acquisition_status', 'full_name', 'location.lon', 'location.lat'])
        data4 = data3.size().to_frame()  # .reset_index().rename(columns={0:'Count'})
        data4['chip_list'] = data3['Chip'].apply(list)
        data4 = data4.reset_index().rename(columns={0: 'Count'})
        data4['group'] = data4.apply(lambda x: x['Instr_Type'] + ' ' + x['acquisition_status'], axis=1)
        fig = px.scatter_geo(data4, lon="location.lon", lat='location.lat',
                             # color="group", # which column to use to set the color of markers
                             hover_name="full_name",  # column added to hover information
                             hover_data=['Instr_Type', 'acquisition_status', 'chip_list'],
                             size="Count",  # size of markers
                             animation_frame="Yr-Mth",
                             # color_discrete_map = {'Complete':'lightskyblue', 'Failed':'coral', 'Soft_Elec':'yellow', 'Hard_Elec':'purple', 'Minilid':'red'},
                             projection="natural earth",
                             # width = 1200,
                             # height = 1200,
                             title='Chip Failure (Sequel + Spider, Hard_Elec + Minilid)')
        fig.show()


def fetch_field_instr(staging='False'):
    if staging:
        instr_url_list = {'Sequel': "http://smrtfleet-api-staging:8080/v1/sq/instru",
                          'Spider': "http://smrtfleet-api-staging:8080/v1/sq2/instru"}
    else:
        instr_url_list = {'Sequel': "http://smrtfleet-api:8080/v1/sq/instru",
                          'Spider': "http://smrtfleet-api:8080/v1/sq2/instru"}
    data_instr = pd.DataFrame()
    for prod in ['Sequel', 'Spider']:
        instr_url = instr_url_list[prod]
        with urllib.request.urlopen(instr_url) as url:
            s = url.read().decode()
        ss = json.loads(s)['response']
        for key in ss.keys():
            sub_data = json_normalize(ss[key])
            sub_data['Instr_Type'] = prod
            data_instr = data_instr.append(sub_data)
    data_instr['x_Serial'] = data_instr['serial'].apply(lambda x: x[0:5] if ((len(x) > 5) and (x[5] == 'R')) else x)
    data_instr['Account Region'] = data_instr['account_region']
    return data_instr


#    folder_path = r'C:\Users\ltu\Documents\R\Customer_Failure'
#    instr_sequel_name = 'instruments_overview-20191017223816-Sequel.csv'
#    instr_sequel = pd.read_csv( os.path.join(folder_path, instr_sequel_name) , dtype=str)
#    instr_sequel['Instr_Type'] = 'Sequel'
#    instr_spider_name = 'instruments_overview-20191017223913-Spider.csv'
#    instr_spider = pd.read_csv( os.path.join(folder_path, instr_spider_name) , dtype=str)
#    instr_spider['Instr_Type'] = 'Spider'
#    instr = instr_sequel.append(instr_spider)
#    instr['x_Serial'] = instr['Serial'].apply(lambda x: x[0:5] if((len(x)>5) and (x[5]=='R')) else x)

def fetch_field_data(prod_type='Sequel', days=300):
    #    if prod_type == 'Sequel':
    #        internal = False
    #        if internal==True:
    #            activity_url = "http://vm-ts-instru_api-001:8080/v1/sequencing_performance/zip"
    #        else:
    #            activity_url = "http://smrtfleet-api.nanofluidics.com:8080/v1/sequencing_performance/zip"
    #        with urllib.request.urlopen(activity_url) as url:
    #            s = url.read().decode()
    #            download_link = s[15:-4]
    #        #    urllib.request.urlretrieve(download_link, '/Users/scott/Downloads/cat.jpg')
    #        #temp_f = tempfile.TemporaryFile(mode='w+b')
    #        r = urllib.request.urlretrieve(download_link)
    #        with zipfile.ZipFile(r[0],"r") as zip_ref:
    #            folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\data-temp'
    #            data_path = os.path.join(folder_path,  'sequencing_performance_data', zip_ref.namelist()[0])
    #            zip_ref.extractall(os.path.join(folder_path,  'sequencing_performance_data'))
    #        #data_name = 'sequencing_performance_details-20190626185214.csv'
    #        data = pd.read_csv( data_path, dtype=str)

    #        today = datetime.datetime.today()
    #        current_date = today.strftime("%Y%m%d")
    #        start_date_utc = (today - datetime.timedelta(days=300)).strftime("%Y%m%d")
    #        activity_url = "http://smrtfleet-api:8080/v1/sq/seq_perf_details?start_date_utc={0}&end_date_utc={1}".format(start_date_utc, current_date)
    #        with urllib.request.urlopen(activity_url) as url:
    #            s = url.read().decode()
    #        ss = json.loads(s)
    #        data = json_normalize(ss['response'])
    #    else:
    #        today = datetime.datetime.today()
    #        current_date = today.strftime("%Y%m%d")
    #        start_date_utc = (today - datetime.timedelta(days=300)).strftime("%Y%m%d")
    #        activity_url = "http://smrtfleet-api:8080/v1/sq2/seq_perf_details?start_date_utc={0}&end_date_utc={1}".format(start_date_utc, current_date)
    #        with urllib.request.urlopen(activity_url) as url:
    #            s = url.read().decode()
    #        ss = json.loads(s)
    #        data = json_normalize(ss['response'])

    today = datetime.datetime.today()
    batch_size = 200
    n_batch = days // batch_size
    data = pd.DataFrame()
    for i in range(n_batch + 1):
        if i == n_batch:
            start_date_utc = (today - datetime.timedelta(days=days)).strftime("%Y%m%d")
        else:
            start_date_utc = (today - datetime.timedelta(days=i * batch_size + batch_size - 1)).strftime("%Y%m%d")
        end_date_utc = (today - datetime.timedelta(days=i * batch_size)).strftime("%Y%m%d")
        if prod_type == 'Sequel':
            activity_url = "http://smrtfleet-api:8080/v1/sq/seq_perf_details?start_date_utc={0}&end_date_utc={1}".format(
                start_date_utc, end_date_utc)
        else:
            activity_url = "http://smrtfleet-api:8080/v1/sq2/seq_perf_details?start_date_utc={0}&end_date_utc={1}".format(
                start_date_utc, end_date_utc)

        with urllib.request.urlopen(activity_url) as url:
            s = url.read().decode()
        ss = json.loads(s)
        data_100days = json_normalize(ss['response'])
        data = data.append(data_100days)

    data['x_Serial'] = data['x_serial'].apply(lambda x: x[0:5] if (len(x) > 5 and x[5] == 'R') else x)
    return data


def fetch_staging_data(prod_type='Spider', days=300):
    today = datetime.datetime.today()
    #    current_date = today.strftime("%Y%m%d")
    batch_size = 200
    n_batch = days // batch_size
    data = pd.DataFrame()
    for i in range(n_batch + 1):
        if i == range(n_batch):
            start_date_utc = (today - datetime.timedelta(days=days)).strftime("%Y%m%d")
        else:
            start_date_utc = (today - datetime.timedelta(days=i * batch_size + batch_size - 1)).strftime("%Y%m%d")
        end_date_utc = (today - datetime.timedelta(days=i * batch_size)).strftime("%Y%m%d")
        if prod_type == 'Sequel':
            activity_url = "http://smrtfleet-api-staging:8080/v1/sq/seq_perf_details?start_date_utc={0}&end_date_utc={1}".format(
                start_date_utc, end_date_utc)
        else:
            activity_url = "http://smrtfleet-api-staging:8080/v1/sq2/seq_perf_details?start_date_utc={0}&end_date_utc={1}".format(
                start_date_utc, end_date_utc)

        with urllib.request.urlopen(activity_url) as url:
            s = url.read().decode()
        ss = json.loads(s)
        data_100days = json_normalize(ss['response'])
        data = data.append(data_100days)

    data['x_Serial'] = data['x_serial'].apply(lambda x: x[0:5] if (len(x) > 5 and x[5] == 'R') else x)
    return data


class FieldPlot:
    def __init__(self, data, prod_type, date_start):
        self.data = data
        self.data['p1_cat'] = self.data[['prodP1_pct', 'acquisition_status']].apply(self.p1_to_cat, axis=1)
        self.date_start = date_start
        self.data1 = self.data[self.data['date'] >= date_start]
        self.prod_type = prod_type

    def plot_failure_month(self, region, acquisition_list, save_folder):
        if region == 'World':
            data1 = self.data
        else:
            data1 = self.data[self.data['Account Region'] == region]
        chip_count = data1.groupby(['Yr-Mth'])['acquisition_status'].count().reset_index()
        fig, ax = plt.subplots(figsize=(10, 5))
        ax.plot(chip_count['Yr-Mth'], chip_count['acquisition_status'], color='r', zorder=1, lw=1)
        ax.scatter(chip_count['Yr-Mth'], chip_count['acquisition_status'], marker='o', color='r', zorder=2)
        _ = plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
        ax.set_title('Chip Escape by Month in ' + region)
        ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.set_ylabel('Chip_Count', color='r')
        ax.spines['left'].set_color('red')
        ax.yaxis.label.set_color('red')
        ax.tick_params(axis='y', colors='red')
        # ax.legend(loc='upper left', bbox_to_anchor=(1, 0.9))
        # plt.tight_layout()
        ax2 = ax.twinx()
        chip_status_count = data1.groupby(['Yr-Mth', 'acquisition_status']).size().reset_index(name='counts')
        df = chip_status_count.pivot(index='Yr-Mth', columns='acquisition_status', values='counts').reset_index()
        for chip_status in acquisition_list:
            if chip_status not in df.columns:
                df[chip_status] = 0
        #    if 'Minilid' not in df.columns:
        #        df['Minilid'] = 0
        #    if 'Hard_Elec' not in df.columns:
        #        df['Hard_Elec'] = 0
        #    if 'Soft_Elec' not in df.columns:
        #        df['Soft_Elec'] = 0
        df = df.fillna(0)
        df = df.merge(chip_count, left_on=['Yr-Mth'], right_on=['Yr-Mth'])
        df['%Fails'] = (df['Failed'] + df['Minilid'] + df['Hard_Elec'] + df['Soft_Elec']) / (
            df['acquisition_status']) * 100
        ax2.scatter(df['Yr-Mth'], df['%Fails'], marker='d', color='g', facecolors='none', zorder=2)
        df['%Fails_Minilid'] = (df['Minilid']) / (df['acquisition_status']) * 100
        ax2.scatter(df['Yr-Mth'], df['%Fails_Minilid'], marker='d', color='c', facecolors='none', zorder=2)
        plt.ylabel('%Fails', color='k')
        df['%Fails_Elec'] = (df['Hard_Elec'] + df['Soft_Elec']) / (df['acquisition_status']) * 100
        ax2.scatter(df['Yr-Mth'], df['%Fails_Elec'], marker='d', color='b', facecolors='none', zorder=2)
        df['%Fails_Elec_Hard'] = (df['Hard_Elec']) / (df['acquisition_status']) * 100
        ax2.scatter(df['Yr-Mth'], df['%Fails_Elec_Hard'], marker='d', color='coral', facecolors='none', zorder=2)
        plt.ylabel('% Fails', color='k')
        if self.prod_type == 'Sequel':
            ax2.legend(loc="upper right")
        else:
            ax2.legend(loc="upper left")
        ax2.plot(df['Yr-Mth'], df['%Fails'], color='g', zorder=1, lw=1)
        ax2.plot(df['Yr-Mth'], df['%Fails_Minilid'], color='c', zorder=1, lw=1)
        ax2.plot(df['Yr-Mth'], df['%Fails_Elec'], color='b', zorder=1, lw=1)
        ax2.plot(df['Yr-Mth'], df['%Fails_Elec_Hard'], color='coral', zorder=1, lw=1)
        fig.savefig(os.path.join(save_folder, 'Plots', self.prod_type + ' ' + region + ' Escape.png'))
        df['%Escape'] = (df['Hard_Elec'] + df['Minilid']) / (df['acquisition_status']) * 100
        return df

    def plot_p1_dist_byMonth(self):
        data1_sum = self.data.groupby(['Yr-Mth', 'p1_cat']).size().reset_index().rename(columns={0: 'Count'})
        p1_pivot = data1_sum.pivot(index='Yr-Mth', columns='p1_cat', values='Count')
        p1_pct = p1_pivot.fillna(0).apply(lambda x: x / sum(x) * 100, axis=1)

        fig, ax = plt.subplots(figsize=(10, 5))
        p1_pct.plot.bar(stacked=True, rot=0, ax=ax)
        ax.set_title(self.prod_type + ': P1 by Month')
        ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.set_ylabel('% Movies')
        ax.legend(loc='upper left', bbox_to_anchor=(1, 0.9), title='P1 Percentage')
        plt.xticks(rotation=90)
        plt.tight_layout()
        return self.data

    def plot_failure_instr(self, *args):
        # data_failure = data1[((data1['p1_cat'] == 'Hard_Elec') | (data1['p1_cat'] == 'Unknown') ) & (data1['date']>= date_start_1mo) ]
        if len(args) == 0:
            data_failure = self.data1[(self.data1['p1_cat'].isin(['Hard_Elec', 'Unknown']))]
        elif len(args) == 1:
            date_start = args[0]
            data_failure = self.data[
                (self.data['p1_cat'].isin(['Hard_Elec', 'Unknown'])) & (self.data['date'] >= date_start)]
        else:
            raise NameError('Wrong input args!')
        data_failure.loc[:, 'full_name'] = data_failure['full_name'].apply(lambda x: x[0:20])
        data_failure = data_failure.sort_values(by=['Account Region'])
        data1_failure_sum = data_failure.groupby(['Account Region', 'Tray', 'full_name']).size().reset_index().rename(
            columns={0: 'Count'})
        name_order = data1_failure_sum[['Account Region', 'full_name']].drop_duplicates()['full_name']
        # data1_failure_sum = data1_failure_sum.sort_values(by=['Account Region'])['full_name']
        data1_failure_count = data1_failure_sum.groupby('full_name')['Count'].sum().reset_index().rename(
            columns={'Count': 'Total_Count'})
        data1_failure_sum = data1_failure_sum.merge(data1_failure_count, left_on=['full_name'], right_on=['full_name'],
                                                    how='left')  # .sort_values('Total_Count',ascending=False)
        data1_failure_pivot = data1_failure_sum.pivot(index='full_name', columns='Tray', values='Count').reindex(
            name_order)
        fig, ax = plt.subplots(figsize=(10, 5))
        data1_failure_pivot.plot.bar(stacked=True, rot=0, ax=ax)
        ax.set_title(self.prod_type + ': Failure by Instrument (from ' + str(self.date_start) + ') (color by Tray)')
        ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.set_ylabel('% Movies')
        ax.get_legend().remove()
        # ax.legend(loc='upper left', bbox_to_anchor=(1, 0.9), title='Yr-Mth')
        plt.xticks(rotation=90)
        plt.tight_layout()

    def save_failure_instr(self, save_folder, date_start_1mo):
        failure_elec_list = ['Minilid', 'Hard_Elec', 'Soft_Elec', 'Failed']
        data2 = self.data[self.data['acquisition_status'].isin(failure_elec_list)]
        data2 = data2[data2['date'] >= date_start_1mo]
        data2 = data2.sort_values(by=['date'])
        failure_summary = data2[
            ['date', 'Account Region', 'full_name', 'Cell_ExpirationDate', 'acquisition_status', 'hard_failure_cause',
             'hard_failure_cause_group', 'Chip', 'Tray', 'ICS']]
        failure_summary.to_csv(os.path.join(save_folder, self.prod_type + '_failure_cases.csv'))

    def plot_hardElec_failure_instr(self, *args):
        if len(args) == 0:
            data_failure = self.data1[(self.data1['p1_cat'] == 'Hard_Elec')]
        elif len(args) == 1:
            date_start = args[0]
            data_failure = self.data[(self.data['p1_cat'] == 'Hard_Elec') & (self.data['date'] >= date_start)]
        else:
            raise NameError('Wrong input args!')

        data_failure.loc[:, 'full_name'] = data_failure['full_name'].apply(lambda x: x[0:20])
        if len(data_failure) > 0:
            data1_failure_sum = data_failure.groupby(['Yr-Mth', 'full_name']).size().reset_index().rename(
                columns={0: 'Count'})
            data1_failure_count = data1_failure_sum.groupby('full_name')['Count'].sum().reset_index().rename(
                columns={'Count': 'Total_Count'})
            data1_failure_sum = data1_failure_sum.merge(data1_failure_count, left_on=['full_name'],
                                                        right_on=['full_name'], how='left').sort_values('Total_Count',
                                                                                                        ascending=False)
            data1_failure_pivot = data1_failure_sum.pivot(index='full_name', columns='Yr-Mth', values='Count')
            fig, ax = plt.subplots(figsize=(10, 5))
            data1_failure_pivot.plot.bar(stacked=True, rot=0, ax=ax)
            ax.set_title(self.prod_type + ': Hard Elec Failure by Instrument (from ' + str(self.date_start) + ')')
            ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
            ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
            ax.set_ylabel('% Movies')
            ax.legend(loc='upper left', bbox_to_anchor=(1, 0.9), title='Yr-Mth')
            plt.xticks(rotation=90)
            plt.tight_layout()

    def plot_ctrlRL_by_bases(self, *args):
        if len(args) == 0:
            data2 = self.data1
        elif len(args) == 1:
            date_start = args[0]
            data2 = self.data[self.data['date'] >= date_start]
        else:
            raise NameError('Wrong input args!')

        data2 = data2[data2['MovieMinutes'].astype('float') > 600]
        if self.prod_type == 'Sequel':
            data2['total_bases (bin: Gb)'] = round(data2['total_bases'].astype('float') / 1e9, -0)
        else:
            data2['total_bases (bin: Gb)'] = round(data2['total_bases'].astype('float') / 1e9, -1)
        data2['reads_length_bin'] = data2['reads_length'].astype('float').apply(
            lambda x: 'control RL<20kb' if x < 20000 else ('control RL 20-45kb' if x < 45000 else 'control RL>45kb'))
        data2_sum = data2.groupby(['total_bases (bin: Gb)', 'reads_length_bin']).size().reset_index().rename(
            columns={0: 'Count'})
        data2_sum_pivot = data2_sum.pivot(index='total_bases (bin: Gb)', columns='reads_length_bin', values='Count')
        data2_sum_pivot = data2_sum_pivot[['control RL<20kb', 'control RL 20-45kb', 'control RL>45kb']]
        fig, ax = plt.subplots(figsize=(10, 5))
        data2_sum_pivot.plot.bar(stacked=True, rot=0, ax=ax)
        ax.set_title(self.prod_type + ': Yield Histogram (10+ hr movies) (from ' + str(self.date_start) + ')')
        ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.set_ylabel('# of Movies')
        plt.xticks(rotation=90)
        handles, labels = ax.get_legend_handles_labels()
        # handles = [handles[1], handles[0], handles[2]]
        # labels = [labels[1], labels[0], labels[2]]
        ax.legend(handles, labels, loc='upper left', bbox_to_anchor=(0.75, 0.9), title='Yr-Mth')
        plt.tight_layout()

    def plot_lowPerf_instr(self, *args):
        try:
            if len(args) == 0:
                data3 = self.data1
            elif len(args) == 1:
                date_start = args[0]
                data3 = self.data[self.data['date'] >= date_start]
            else:
                raise NameError('Wrong input args!')

            data3.loc[:, 'full_name'] = data3['full_name'].apply(lambda x: x[0:20])
            data3 = data3[(data3['MovieMinutes'].astype('float') > 600) & (data3['total_bases'].astype('float') < 2e9)]
            data3['total_bases_bin'] = data3['total_bases'].astype('float').apply(
                lambda x: '<1Mb' if x < 1e6 else ('1Mb to 1Gb' if x < 1e9 else '1 to 2Gb'))
            data3_sum = data3.groupby(['Account Region', 'full_name', 'total_bases_bin']).size().reset_index().rename(
                columns={0: 'Count'})
            name_order = data3_sum[['Account Region', 'full_name']].drop_duplicates()['full_name']
            data3_pivot = data3_sum.pivot(index='full_name', columns='total_bases_bin', values='Count').reindex(
                name_order)
            fig, ax = plt.subplots(figsize=(15, 5))
            data3_pivot.plot.bar(stacked=True, rot=0, ax=ax)
            ax.set_title(self.prod_type + ': Low Performers by Instrument (from ' + str(self.date_start) + ')')
            ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
            ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
            ax.set_ylabel('# Movies')
            ax.legend(loc='upper left', bbox_to_anchor=(1, 0.9), title='total base')
            plt.xticks(rotation=90)
            plt.tight_layout()
        except ValueError:
            print('Index contains duplicate entries')

    def p1_to_cat(self, line):
        p1 = float(line.iloc[0])
        acquisition_status = line.iloc[1]
        if acquisition_status == 'Hard_Elec':
            return 'Hard_Elec'
        elif pd.isna(p1):
            return 'Unknown'
        if p1 < 20:
            return "0 to 20%"
        elif p1 < 30:
            return "20% to 30%"
        elif p1 < 40:
            return "30% to 40%"
        elif p1 < 50:
            return "40% to 50%"
        else:
            return "50% to 100%"
