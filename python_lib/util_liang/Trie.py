# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 11:51:25 2020

@author: ltu
"""
from collections import defaultdict
class TrieNode:
    def __init__(self):
        self.children = defaultdict(TrieNode)
        self.val = []
        
class TrieEOLQC:
    def __init__(self):
        self.root = TrieNode()
        
    def insert(self, chip, ts):
        node = self.root
#        for c in chip:
#            node.val.append({"value":chip, "ts":ts})
#            node = node.children[c]
        node.val.append({"value":chip, "ts":ts})
            
    def get(self, chip):
        node = self.root
#        for c in chip:
#            if c in node.children:
#                node = node.children[c]
#            else:
#                return []
        return node.val
            
    def sort(self):
        def dfs(node):
            if not node:
                return
            node.val.sort(key = lambda x: x["ts"], reverse = True)
            for k,v in node.children.items():
                dfs(v)
        dfs(self.root)