# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 12:19:38 2020

@author: ltu
"""

import pandas as pd
import numpy as np
import pyodbc
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn import preprocessing
import matplotlib.patheffects as path_effects
import os

if os.name == 'nt':
    conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
else:
    server = 'usmp-vm-campd8d\sql2012ods'
    # database = 'CamODS_6X'
    username = 'odbc'
    password = 'readonly'
    conn_MES = pyodbc.connect(
        'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)
conn_MES.autocommit = True


def get_PBI_Lot(prod='Spider'):
    if prod == 'Spider':
        Database = "SpiderMetrology"
    else:
        Database = "SequelMetrology"

    if os.name == 'nt':
        conn_metrology = pyodbc.connect('DSN={0};UID=odbc;PWD=readonly'.format('SpiderMetrology'))
    else:
        server = 'camstarsql\sql2012'
        database = 'SpiderMetrology'
        username = 'odbc'
        password = 'readonly'
        conn_metrology = pyodbc.connect(
            'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';Database=' + database + 'UID=' + username + ';PWD=' + password)

    conn_metrology.autocommit = True
    get_data_sql = '''SELECT *
    FROM [{0}].[dbo].[Batch]
    '''.format(Database)
    data = pd.read_sql(get_data_sql, conn_metrology)

    if prod == 'Spider':
        data = data.dropna()
        data['PBI_Lot'] = data['AliasBatchName'].apply(lambda x: x.split(';')[-1])
        del data['AliasBatchName']
    else:
        #        data = data.dropna()
        #        data['PBI_Lot'] = data['AliasBatchName']
        data = pd.read_csv(r'\\pbi\dept\chip\ChipEng\Liang\RStudio\SPC_Project_Server\Silterra Sequel Wafers.csv')
    #        data['Wafer'] = data['Sequel Wafer ID']
    return data


def get_SA_Lot(wafer_list=['PW11000-20', 'PV45127-01']):
    if os.name == 'nt':
        conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
    else:
        server = 'usmp-vm-campd8d\sql2012ods'
        # database = 'CamODS_6X'
        username = 'odbc'
        password = 'readonly'
        conn_MES = pyodbc.connect(
            'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)

    conn_MES.autocommit = True
    get_data_sql = '''SELECT *
    FROM [CamODS_6X].[insitedb_schema].[PBI_ContainerEnquiry]
    WHERE ContainerName in ('{}') -- ContainerName
    '''.format('\',\''.join(wafer_list))
    data = pd.read_sql(get_data_sql, conn_MES)
    data = data[['ContainerName', 'SALot', 'SAActiveDate', 'SAExpDate']].drop_duplicates(
        subset=['ContainerName']).dropna()  # 'ChildContainerName',
    return data


def get_moves_data(date='2018-01-01'):
    if os.name == 'nt':
        conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
    else:
        server = 'usmp-vm-campd8d\sql2012ods'
        # database = 'CamODS_6X'
        username = 'odbc'
        password = 'readonly'
        conn_MES = pyodbc.connect(
            'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)

    conn_MES.autocommit = True
    get_data_sql = '''SELECT [Chip],[MoveDate]
    FROM [CamODS_6X].[insitedb_schema].[View_MoveHistoryByOpAndDateRange]
    where FromOperation in ('Wet Bench')
    and (Comments IS NULL OR Comments NOT LIKE '%%MES 9.8 Release%%')
    and MoveDate >= '{}'
    and Chip like 'P%'
    '''.format(date)
    data = pd.read_sql(get_data_sql, conn_MES)
    data.to_csv(r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data\wetbench_date.csv')
    return data


def get_EOLQC_wafers(upload_date='2020-09-01'):
    #    upload_date='2020-09-01'
    #    end_date='2020-09-07'
    if isinstance(upload_date, str):
        get_data_sql = '''SELECT [ContainerName] Chip,[DataName],[DataValue] EOLUploadDate,[TxnDate]
          FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
          where DataName = 'EOLUploadDate' and DataValue > '{}'
          '''.format(upload_date)
    elif len(upload_date) == 1:
        get_data_sql = '''SELECT [ContainerName] Chip,[DataName],[DataValue] EOLUploadDate,[TxnDate]
          FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
          where DataName = 'EOLUploadDate' and DataValue > '{}'
          '''.format(upload_date[0])
    elif len(upload_date) == 2:
        get_data_sql = '''SELECT [ContainerName] Chip,[DataName],[DataValue] EOLUploadDate,[TxnDate]
          FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
          where DataName = 'EOLUploadDate' and DataValue > '{0}' and DataValue < '{1}'
          '''.format(upload_date[0], upload_date[1])

    data = pd.read_sql(get_data_sql, conn_MES)
    data['wafer'] = data['Chip'].apply(lambda x: x[:10])
    return list(data['wafer'].unique())


def get_EOLQC_chips(upload_date='2020-09-01'):
    get_data_sql = '''SELECT [ContainerName] Chip,[DataName],[DataValue] EOLUploadDate,[TxnDate]
          FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
          where DataName = 'EOLUploadDate' and DataValue > '{}'
    '''.format(upload_date)
    data = pd.read_sql(get_data_sql, conn_MES)
    return list(data['Chip'].unique())


def get_SCC_chip(chip_list):
    import json, os
    import requests
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'text/csv'  # application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    }
    df_chip = pd.DataFrame()
    for chip in chip_list:
        query = {
            "start": 0,
            "length": 3,
            "columns": [
                {
                    "name": "ContainerName",
                    "search": chip
                },
                {"name": "Substrate"},
                {"name": "BatchAlias"},
                {"name": "CustomerLot"},
                {"name": "BCD_estimate"},
                {"name": "ODS_IllumImage"}
            ],
            "order": {
                "column": 1,
                "dir": "desc"
            }
        }
        data = json.dumps(query)
        response = requests.post('http://itg/metrics-dev/scc/tables/Chip/export', headers=headers, data=data)
        abc = response.content
        aaa = abc.replace(b'"', b'').decode().split('\n')

        data_chip_substrate = [aaa[i].split(',')[:17] for i in range(1, len(aaa))]
        df_chip = df_chip.append(pd.DataFrame(data_chip_substrate, columns=aaa[0].split(',')), ignore_index=True)
    return df_chip


def SCC_EOLQC(container):  # if str, container is a single wafer; if list, container is a list of chips
    if isinstance(container, str):
        get_data_sql = '''SELECT [ContainerName] Chip,[DataName],[DataValue] EOLUploadDate,[TxnDate]
              FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
              where DataName = 'EOLUploadDate' and ContainerName like '{}%'
        '''.format(container)
    elif isinstance(container, list):
        get_data_sql = '''SELECT [ContainerName] Chip,[DataName],[DataValue] EOLUploadDate,[TxnDate]
              FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
              where DataName = 'EOLUploadDate' and ContainerName in ('{}')
        '''.format('\',\''.join(container))

    data = pd.read_sql(get_data_sql, conn_MES)
    chip_list = list(data['Chip'].unique())

    param_list = '''movie_index_in_cell, ALPStop_L1Power_mw, ALPStop_L2Power_mw, concordance, InsertReadLengthMean, movie_length, nscraps, nsubreads, nsubreads_mapped,
        nzmw_scraps, nzmws_subreads, pd_Empty, pd_Other,pd_Productive,pd_Undefined,ReadLengthMean,RedAngle,GreenAngle,SpectralAngle, 
        BaselineLevelMean_C, HQPkMidMedian_C, HQSNRMedian_C, LaserTitration,EOLUploadDate, moviename, Instrument, sts_xml, substrate_id
        '''.split(',')
    param_list = map(str.strip, param_list)
    get_data_sql = '''SELECT [ContainerName] Chip,[DataName],[DataValue],[TxnDate]
          FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
          where ContainerName in  ('{0}')
           and DataName in ('{1}')
    '''.format('\',\''.join(chip_list), '\',\''.join(param_list))
    data = pd.read_sql(get_data_sql, conn_MES)
    data['wafer'] = data['Chip'].apply(lambda x: x[:10])
    data1 = data.sort_values('TxnDate', ascending=False).drop_duplicates(['Chip', 'DataName'])
    data1 = pd.pivot_table(data1, values='DataValue', index=['Chip', 'wafer'], columns=['DataName'],
                           aggfunc=np.max).reset_index()
    data1 = data1.rename(columns={'substrate_id': 'Substrate'})
    #    data1['pdProdMap'] = "http://usmp-vm-sequelwebapp/heatmaps/EOLQC/"  + data1['Instrument'] + "/" + data1['moviename'] + "." + data1['Substrate'] + ".P1.gif"
    #
    #    img_list = ['ODS_Image', 'pdEmptyMap','pdProdMap','pdOtherMap']

    df_substrate = get_SCC_chip(data1['Chip'])
    df_eolqc = data1.merge(df_substrate[['Chip', 'Alias', 'CustomerLot', 'BCD_estimate', 'ODS_Image']], left_on='Chip',
                           right_on='Chip', how='left')

    df_eolqc['pd_Productive'] = df_eolqc['pd_Productive'].astype(int)
    df_eolqc['pd_Empty'] = df_eolqc['pd_Empty'].astype(int)
    df_eolqc['pd_Other'] = df_eolqc['pd_Other'].astype(int)
    df_eolqc['pdProd%'] = df_eolqc['pd_Productive'] / (
            df_eolqc['pd_Productive'] + df_eolqc['pd_Empty'] + df_eolqc['pd_Other'])
    df_eolqc['pdOther%'] = df_eolqc['pd_Other'] / (
            df_eolqc['pd_Productive'] + df_eolqc['pd_Empty'] + df_eolqc['pd_Other'])
    df_eolqc['pdEmpty%'] = df_eolqc['pd_Empty'] / (
            df_eolqc['pd_Productive'] + df_eolqc['pd_Empty'] + df_eolqc['pd_Other'])
    df_eolqc['Runcode'] = df_eolqc['sts_xml'].apply(lambda x: x.split('/')[4])
    return df_eolqc


def gen_wafer_dev():
    import re
    get_data_sql = '''SELECT *
                  FROM [CamODS_6X].[insitedb_schema].[View_MoveHistoryByOpAndDateRange]
      WHERE FromOperation = 'Final Release' AND MoveDate > '2020-01-01'
      --AND Chip = 'AX36000-05-4954'
            '''  # .format('\',\''.join(container))

    data = pd.read_sql(get_data_sql, conn_MES)
    df_wafer = data[['MovedContainer', 'MoveDate']].drop_duplicates()
    df_wafer['Date'] = df_wafer['MoveDate']
    df_wafer['Year-Month'] = df_wafer['Date'].apply(lambda x: '{}-{:02}'.format(x.year, x.month))
    df_wafer['Year-WW'] = df_wafer['Date'].apply(lambda x: '{}-{:02d}'.format(x.isocalendar()[0], x.isocalendar()[1]))
    wafer_list = df_wafer['MovedContainer'].unique().tolist()
    get_data_sql = '''SELECT [ContainerName], [PBI_DeviationLot]
                  FROM [CamODS_6X].[insitedb_schema].[Container]
      WHERE ContainerName in ('{}')
            '''.format('\',\''.join(wafer_list))
    data_deviation = pd.read_sql(get_data_sql, conn_MES)
    df_dev = df_wafer.merge(data_deviation, left_on='MovedContainer', right_on='ContainerName', how='inner')
    df_dev = df_dev.rename(columns={'MovedContainer': '"Wafer"', 'PBI_DeviationLot': '"Deviation"'})
    df_dev['"System"'] = df_dev['"Wafer"'].apply(lambda x: 'Sequel' if x[0] == 'P' else 'Spider')
    df_dev = df_dev[df_dev['"Wafer"'].str.len() == 10]

    df_dev_date = pd.DataFrame(columns=['System', 'wafer', 'Year-Month', 'Year-WW', 'deviation'])
    # df_dev_ww = pd.DataFrame(columns= ['System','wafer','Year-WW','deviation'])
    for idx, row in df_dev.iterrows():
        #    print(row)
        #    print(row['"deviation"'])
        try:
            deviation_list = row['"Deviation"'].split(' ')
        except:
            print(row)
        for dev in deviation_list:
            if len(dev) > 9:  # to handle the case like "D2045E400D2082E406"
                for dev_str in re.findall('DE?V?-?[0-9]{4}', dev, flags=re.IGNORECASE):
                    if dev_str[0] == 'D':
                        df_dev_date.loc[len(df_dev_date)] = [row['"System"'], row['"Wafer"'], row['Year-Month'],
                                                             row['Year-WW'], dev_str]
            else:
                df_dev_date.loc[len(df_dev_date)] = [row['"System"'], row['"Wafer"'], row['Year-Month'], row['Year-WW'],
                                                     dev]
    df_dev_date.to_pickle('//pbi/dept/chip/ChipEng/Liang/Yield/Deviation/deviation_data.pkl')

if __name__ == '__main__':
    prod = 'Sequel'  # 'Sequel'  'Spider'
    data = get_PBI_Lot(prod)
    if prod == 'Sequel':
        wafer_list = list(data['Sequel Wafer ID'].unique())
        data_SA = get_SA_Lot(wafer_list)
        data = data.merge(data_SA, how='left', left_on='Sequel Wafer ID', right_on='ParentContainer')
    data.to_csv(os.path.join(r'\\pbi\dept\chip\ChipEng\Liang\Field Data', 'PBI_SA_Lot_' + prod + '.csv'), index=False)

#    if os.name == 'nt':
#        conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  #camstarsql_mes
#    else:
#        server = 'usmp-vm-campd8d\sql2012ods'
#        #database = 'CamODS_6X' 
#        username = 'odbc' 
#        password = 'readonly' 
#        conn_MES = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';UID='+username+';PWD='+ password)
#     
#    conn_MES.autocommit = True
#    get_data_sql =  '''SELECT ChipID, DataValue
#    FROM [CamODS_6X].[insitedb_schema].[PBI_GetBCDModel1Data]
#    WHERE DataName like 'PBI_bcd_fit%' and ChipID in ('{}')
#    ''' .format('\',\''.join(chip_list))
#    data_fit = pd.read_sql(get_data_sql, conn_MES)
#    data_fit['BCD (nm)'] = data_fit['DataValue'].astype('float')*1e9
#    data_fit = data_fit.drop(columns=['DataValue'])
#    return data_fit
