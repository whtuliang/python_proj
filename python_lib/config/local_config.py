import os

if os.name == 'nt':
    dir_pbi_liang = r'\\pbi\dept\chip\ChipEng\Liang'
    folder_output = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
else:
    dir_pbi_liang = '//pbi/dept/chip/ChipEng/Liang'
    folder_output = '//pbi/dept/chip/ChipEng/EOLQC-Report'