def main():
    from SPC_Summary import FE_EOQC_FirstFailure
    # 'OQA', 'EOQC_Grade', 'EOQC_mode', 'EOQC_Failure_Bin', 'BCD', 'OQA_C/F', 'EOQC0_Bin'
    FE_EOQC_FirstFailure.FE_Defects_Plot(wafer='AY27186-07', idx=3)
    for wafer_id in range(25):
        # for wafer_id in range(14, 22):
        FE_EOQC_FirstFailure.FE_Defects_Plot(wafer='AY15211-' + '{:02d}'.format(wafer_id), idx=3)

    import pandas as pd
    data = pd.DataFrame()
    for wafer_id in range(14, 22):
        data = data.append(FE_EOQC_FirstFailure.get_FE_parameter(wafer='AY15211-' + '{:02d}'.format(wafer_id), idx=3))
    data.to_csv('EOQC1.csv')

    # Plot Wafer Map ['OQA', 'EOQC_Grade', 'EOQC_mode', 'EOQC_Failure_Bin', 'BCD', 'OQA_C/F', 'EOQC0']


if __name__ == "__main__":
    main()
