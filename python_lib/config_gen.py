# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 11:37:36 2020

@author: ltu
"""

import json

config = {

'folder_ChipEng' : r'\\pbi\dept\chip\ChipEng', 

'DB_MES' : {'DSN':'camstarsql_mes', 'UID':'odbc', 'PWD':'readonly'},

'Sequel': {'DB_Metrology' : {'DSN':'sequelmetrology', 'UID':'odbc', 'PWD':'readonly'},
'EOLQC_rsts': r'\\pbi\dept\techsupport\internaldata\sqExtra\INSTRUMENT\icc\data\pacbio\context\collection',
'Field_rsts': r'\\pbi\dept\techsupport\sqExtra\INSTRUMENT\icc\data\pacbio\context\collection',
'EOLQC_img_Etiene': r'\\pbi\dept\systems\emcc\WaferId'},

'Spider': {'DB_Metrology' : {'DSN':'spidermetrology', 'UID':'odbc', 'PWD':'readonly'},
'EOLQC_rsts': r'\\pbi\dept\techsupport\internaldata\spExtra\INSTRUMENT\nrt\data\pacbio\context\collection',
'Field_rsts': r'\\pbi\dept\techsupport\spExtra\INSTRUMENT\nrt\data\pacbio\context\collection',
'EOLQC_img_Etiene': r'\\pbi\dept\systems\emcc\Spider\WaferId'}

 }

json.dump(config, open('config.json', 'w'))
#json.load(open('/tmp/config.json'))  