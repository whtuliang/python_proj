# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 16:45:17 2020

@author: ltu
"""

import pandas as pd
import numpy as np
import pyodbc 
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn import preprocessing
import matplotlib.patheffects as path_effects


conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')  #camstarsql_mes
cursor = conn.cursor()
conn.autocommit = True



#FE_test = ['OQA','EOQC','EOQC_mode','BCD','OQA_C/F']  #only add new features at the end
#FE_test_name = ['SLT_OQA_DefectId','FTT_0000_DB_NOP_000','FTT_FIRST_FAILURE', 'PBI_bcd_fit', 'SLT_OQA_DefectId']
#grade = [{'C':'D21','F':['D29','D41']}, {'F':'9'}, {'A':'None'}, {'A':[85,125]}, {'A':'None'}]  #OQA {'A':['D{0:02}'.format(i) for i in list(range(1,21)) + list(range(22,28)) + list(range(30,41))]}
#    
#def FE_Defects_Plot(wafer = 'AX18075-20', idx = 4):

product = 'Spider'
    
test_name = '%tc2%' #'PBI_bcd_fit'

get_data_sql =  '''
Select Wafer.WaferName, DataName.DataName, DataUploadSession.DataCollectionTime, Site.SiteName, Site.ChipIndex, latestcd.StringValue   
From (Select WaferData.WaferId, WaferData.DataNameId, WaferData.SiteId, WaferData.StringValue, MAX(WaferData.DataUploadSessionId) as DataUploadSessionId
      From [SpiderMetrology].[dbo].[WaferData]
      Group by WaferData.StringValue, WaferData.SiteId, WaferData.DataNameId, DataUploadSessionId, WaferData.WaferId) as latestcd

Join [SpiderMetrology].[dbo].[Wafer] Wafer on latestcd.WaferId=Wafer.WaferId
Join [SpiderMetrology].[dbo].[DataName] DataName on DataName.DataNameId=latestcd.DataNameId
Join [SpiderMetrology].[dbo].[Site] Site on Site.SiteId=latestcd.SiteId
Join [SpiderMetrology].[dbo].[DataUploadSession] DataUploadSession on DataUploadSession.DataUploadSessionId=latestcd.DataUploadSessionId
Where Wafer.WaferName like 'A%' and DataName.DataName like 'SLT_spider_wg_Aft_oxtc2_cmp_thk%'
'''
  #Raw: SLT_spider_wg_Aft_oxtc2_cmp_thk
  #Fit: PBI_spider_wg_Aft_oxtc2_cmp_thk_fit     

#     GET TC2 fit from SpiderMetrology
product = 'Spider'
test_name = 'PBI_spider_wg_Aft_oxtc2_cmp_thk_fit' #'PBI_bcd_fit'
get_data_sql =  '''
SET NOCOUNT ON
DECLARE @LotName AS NVARCHAR(128)
--SET @LotName = '{0}%'

DECLARE @RawTable TABLE (
       WaferName NVARCHAR(255),
       BaeWaferName NVARCHAR(255),
       ChipIndex INT,
       SubstrateId CHAR(8),
       DataName NVARCHAR(255),
       DataValue real, --NVARCHAR(255),
       Unit NVARCHAR(255),
       DataUploadTime DATETIME,
       DataCollectionTime DATETIME,
       [FileName] NVARCHAR(255),
       [Description] NVARCHAR(255)
       )



INSERT INTO @RawTable 
       (WaferName, BaeWaferName, ChipIndex,SubstrateId,DataName,DataValue,Unit,DataUploadTime,
              DataCollectionTime,
              [Description])
       SELECT 
                      WaferName,
                      BaeWaferName,
                      ChipIndex,
                      {1}Metrology.dbo.ConvertSubstrateIdToString(SubstrateId),
                      DataName,
                      DataValue,
                      Unit,
                      DataUploadTime,
                      DataCollectionTime,
                      [Description]
              FROM (SELECT ChipId, DataUploadSessionId, DataNameId, CAST(CAST(ChipData.DataValue as NVARCHAR(255)) as real) as DataValue
                    FROM [{1}Metrology].[dbo].[ChipData] ) ChipData
                      JOIN [{1}Metrology].[dbo].[Chip] Chip ON ChipData.ChipId = Chip.ChipId
                      JOIN [{1}Metrology].[dbo].[Wafer] Wafer ON Chip.WaferId = Wafer.WaferId
                      JOIN [{1}Metrology].[dbo].[DataName] DataName ON ChipData.DataNameId = DataName.DataNameId
                      JOIN [{1}Metrology].[dbo].[DataUploadSession] DataUploadSession ON ChipData.DataUploadSessionId = DataUploadSession.DataUploadSessionId
              --WHERE Wafer.WaferName LIKE @LotName
              AND DataName LIKE '%{2}%';


SELECT 
       rt.WaferName,
       BaeWaferName,
       rt.ChipIndex,
       SubstrateId,
       rt.DataName,
       DataValue,
       Unit,
       rt.DataUploadTime,
       DataCollectionTime,
       [Description]
FROM @RawTable rt
INNER JOIN (
              select
                      WaferName,
                      ChipIndex,
                      DataName,
                      max(DataUploadTime) as DataUploadTime
              from @RawTable group by WaferName,ChipIndex,DataName
         ) sd
ON
       rt.WaferName = sd.WaferName AND
       rt.ChipIndex = sd.ChipIndex AND
       rt.DataName = sd.DataName AND
       rt.DataUploadTime = sd.DataUploadTime
       order by SubstrateId'''.format('', product, test_name)
       

#abc = cursor.execute(get_data_sql).fetchall()
data_raw = pd.read_sql(get_data_sql, conn)
data_raw.to_csv(r'\\pbi\dept\chip\ChipEng\Liang\Yield\BCD_Re_Estimation\Data\tc2_fit.csv')

#site_map = ['5353', '5252', '5151', '5050', '4949', '4848', '4747', '5347', '4753']
#data_raw['chip_ID'] = data_raw['SiteName'].apply(lambda x: site_map[int(x[4])-1])

