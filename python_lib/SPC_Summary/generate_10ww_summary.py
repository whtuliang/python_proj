# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 14:12:01 2019

@author: ltu
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import statsmodels.api as sm  # import statsmodels
import os
from os import listdir
from os.path import isfile, isdir, join
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from openpyxl import load_workbook

# folder_path = os.path.dirname(os.path.abspath('__file__'))
# ww = '31'  #NEED TO UPDATE
# folder_path = r'\\sharepoint\mfg\sequelcell\Documents\Manufacturing Engineering\Yield Reports\Weekly Yield Meetings\WW9'+ww+r'\Data'
folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'


def gen_summary_BCD():
    FE_name = 'chip_data_FE.csv'
    data_FE_raw = pd.read_csv(os.path.join(folder_path, FE_name))
    data_FE_spider = data_FE_raw[data_FE_raw.Product == 'Spider']

    def FE_grade(x):
        #        xx = np.array(x)
        if np.any(x == 'F'):
            return 'F'
        elif np.any(x == 'D'):
            return 'D'
        elif np.any(x == 'C'):
            return 'C'
        elif np.any(x == 'B'):
            return 'B'
        elif np.any(x == 'A'):
            return 'A'

    data_FE_spider['FE_noBCD'] = data_FE_spider[['EOQC1', 'OQC1', 'OQA', 'VQC1']].apply(FE_grade, axis=1)
    df = data_FE_spider[['WorkWeekOut', 'FE_noBCD']].groupby(['WorkWeekOut', 'FE_noBCD']).size().reset_index(
        name='counts')
    data_FE_WW = pd.pivot_table(df, values='counts', index=['WorkWeekOut'], columns=['FE_noBCD'], aggfunc=np.sum,
                                fill_value=0)
    data_FE_WW['BCD_Yield (%)'] = data_FE_WW['A'] / (
            data_FE_WW['A'] + data_FE_WW['B'] + data_FE_WW['C'] + data_FE_WW['D'] + data_FE_WW['F']) * 100

    def BCD_grade(x):
        if x == 'F':
            return 'F'
        elif x == 'C':
            return 'C'
        elif x == 'A':
            return 'A'

    data_FE_spider['BCD_Grade'] = data_FE_spider['BCDINLINE'].apply(BCD_grade)
    #    data_FE_spider.to_csv('BCD_grade.csv')
    df = data_FE_spider[['WorkWeekOut', 'BCD_Grade']].groupby(['WorkWeekOut', 'BCD_Grade']).size().reset_index(
        name='counts')
    data_BCD_WW = pd.pivot_table(df, values='counts', index=['WorkWeekOut'], columns=['BCD_Grade'], aggfunc=np.sum,
                                 fill_value=0)
    data_BCD_WW['BCD_Yield (%)'] = data_BCD_WW['A'] / (data_BCD_WW['A'] + data_BCD_WW['C'] + data_BCD_WW['F']) * 100


def generate_summary(path_to):
    if __name__ != '__main__':
        import shutil
        shutil.copyfile('Summary_10days.xlsx', path_to + r'\Summary_10days.xlsx')

    seq_name = 'ww_run_data_Sequel.csv'
    spider_name = 'ww_run_data_Spider.csv'
    data_seq_raw = pd.read_csv(os.path.join(folder_path, seq_name))
    data_spider_raw = pd.read_csv(os.path.join(folder_path, spider_name))

    col_list = ['Product', 'WorkWeek', 'FE_Yield_AC_n', 'PDP_Yield_n', 'BE_Yield_n', 'DS_Yield_n',
                'EOLQC_Yield_n', 'FR_Yield_n', 'FE_Yield_A_Yield', 'FE_Yield_AC_Yield', 'BCD_Yield_Yield',
                'FE_Yield_BCD_Yield',
                'PDP_Yield_Yield', 'BE_Yield_Yield', 'DS_Yield_Yield', 'EOLQC_Yield_Yield',
                'FR_Yield_Yield', 'CCYA_1_locf', 'CCYAC_1_locf']
    data_seq = data_seq_raw[col_list]
    col_list = ['Product', 'WorkWeek', 'FE_Yield_AC_n', 'PDP_Yield_n', 'BE_Yield_n', 'DS_Yield_n',
                'EOLQC_Yield_n', 'FR_Yield_n', 'FE_Yield_A_Yield', 'FE_Yield_BCD_Yield', 'BCD_Yield_Yield',
                'PDP_Yield_Yield', 'BE_Yield_Yield', 'DS_Yield_Yield', 'EOLQC_Yield_Yield',
                'FR_Yield_Yield', 'CCYA_1_locf', 'CCYAC_1_locf']
    data_spider = data_spider_raw[col_list]

    col_list = ['Product', 'WorkWeek', 'FE_Yield_A_Yield_13', 'FE_Yield_AC_Yield_13', 'BCD_Yield_Yield_13',
                'FE_Yield_BCD_13',
                'PDP_Yield_Yield_13', 'BE_Yield_Yield_13', 'DS_Yield_Yield_13',
                'EOLQC_Yield_Yield_13', 'FR_Yield_Yield_13', 'CCYA_13', 'CCYAC_13']
    data_seq_13 = data_seq_raw[col_list]
    col_list = ['Product', 'WorkWeek', 'FE_Yield_A_Yield_13', 'FE_Yield_BCD_13', 'BCD_Yield_Yield_13',
                'PDP_Yield_Yield_13', 'BE_Yield_Yield_13', 'DS_Yield_Yield_13',
                'EOLQC_Yield_Yield_13', 'FR_Yield_Yield_13', 'CCYA_13', 'CCYAC_13']
    data_spider_13 = data_spider_raw[col_list]

    seq_name = 'ww_run_data_Sequel_ww4_.csv'
    spider_name = 'ww_run_data_Spider_ww4_.csv'
    data_seq_4 = pd.read_csv(os.path.join(folder_path, seq_name))
    data_spider_4 = pd.read_csv(os.path.join(folder_path, spider_name))

    col_list = ['Product', 'WorkWeek', 'FE_Yield_A_Yield_4', 'FE_Yield_AC_Yield_4', 'BCD_Yield_Yield_4',
                'FE_Yield_BCD_Yield_4',
                'PDP_Yield_Yield_4', 'BE_Yield_Yield_4', 'DS_Yield_Yield_4',
                'EOLQC_Yield_Yield_4', 'FR_Yield_Yield_4', 'CCYA_4', 'CCYAC_4']
    data_seq_4 = data_seq_4[col_list]
    col_list = ['Product', 'WorkWeek', 'FE_Yield_A_Yield_4', 'FE_Yield_BCD_Yield_4', 'BCD_Yield_Yield_4',
                'PDP_Yield_Yield_4', 'BE_Yield_Yield_4', 'DS_Yield_Yield_4',
                'EOLQC_Yield_Yield_4', 'FR_Yield_Yield_4', 'CCYA_4', 'CCYAC_4']
    data_spider_4 = data_spider_4[col_list]

    data_seq = pd.merge(data_seq, data_seq_4, how='left', left_on=['Product', 'WorkWeek'],
                        right_on=['Product', 'WorkWeek'])
    data_seq = pd.merge(data_seq, data_seq_13, how='left', left_on=['Product', 'WorkWeek'],
                        right_on=['Product', 'WorkWeek'])
    data_spider = pd.merge(data_spider, data_spider_4, how='left', left_on=['Product', 'WorkWeek'],
                           right_on=['Product', 'WorkWeek'])
    data_spider = pd.merge(data_spider, data_spider_13, how='left', left_on=['Product', 'WorkWeek'],
                           right_on=['Product', 'WorkWeek'])

    # date_max =  datetime.strptime(data_seq['WorkWeek'].max()+'-0', '%Y-%U-%w')
    # datetime_object = datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p')
    date_max_raw = max(data_seq['WorkWeek'].max(), data_spider['WorkWeek'].max())
    date_max = date_max_raw.split('-')
    date_max_val = int(date_max[0]) * 52 + int(date_max[1])
    date_list = []
    for i in range(13):
        date_val_year = (date_max_val - i) // 52
        date_val_week = (date_max_val - i) % 52
        date_val = str(date_val_year) + '-' + '{0:02d}'.format(date_val_week)
        date_list.append(date_val)

    book = load_workbook(path_to + '\\Summary_10days.xlsx')
    writer = pd.ExcelWriter(path_to + '\\Summary_10days.xlsx', engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

    data_10days = data_seq[data_seq.WorkWeek.isin(date_list)].T
    data_10days.iloc[1:8, :].to_excel(writer, "Sheet1", startrow=2, startcol=2, index=False, header=False,
                                      na_rep='NA')  # sheet_name = 'x1'
    data_10days.iloc[[8, 9, 12, 13, 14, 15, 16, 17, 18], :].to_excel(writer, "Sheet1", startrow=10, startcol=2,
                                                                     index=False, header=False, na_rep='NA')
    data_10days.iloc[[19, 20, 23, 24, 25, 26, 27, 28, 29], :].to_excel(writer, "Sheet1", startrow=21, startcol=2,
                                                                       index=False, header=False, na_rep='NA')
    data_10days.iloc[[30, 31, 34, 35, 36, 37, 38, 39, 40], :].to_excel(writer, "Sheet1", startrow=32, startcol=2,
                                                                       index=False, header=False, na_rep='NA')

    data_10days = data_spider[data_spider.WorkWeek.isin(date_list)].T
    data_10days.iloc[1:8, :].to_excel(writer, "Sheet1", startrow=2, startcol=17, index=False, header=False,
                                      na_rep='NA')  # sheet_name = 'x1'
    data_10days.iloc[8:18, :].to_excel(writer, "Sheet1", startrow=10, startcol=17, index=False, header=False,
                                       na_rep='NA')
    data_10days.iloc[18:28, :].to_excel(writer, "Sheet1", startrow=21, startcol=17, index=False, header=False,
                                        na_rep='NA')
    data_10days.iloc[28:, :].to_excel(writer, "Sheet1", startrow=32, startcol=17, index=False, header=False,
                                      na_rep='NA')

    # %%

    data_all_file = 'all_chip_level_data.csv'
    data_all = pd.read_csv(os.path.join(folder_path, data_all_file))
    # data_FE = data_all[data_all['FE_QC_Cumulative_WW']== date_max_raw]
    data_FE = data_all[data_all['FEWorkWeekOut'] == date_max_raw]
    data_PDP = data_all[data_all['PDPWorkWeekOut'] == date_max_raw]
    data_BE = data_all[data_all['BEWorkWeekOut'] == date_max_raw]
    data_DS = data_all[data_all['DSWorkWeekOut'] == date_max_raw]
    data_FR = data_all[data_all['FRWorkWeekIn'] == date_max_raw]
    data_EOLQC = data_all[data_all['EOLQCWorkWeekOut'] == date_max_raw]
    del data_all

    data_FE['FE_QC_Cumulative_Grade'] = data_FE['FE_QC_Cumulative_Grade'].fillna('NA')
    data_FE_summary = data_FE.groupby(
        ['Product', 'PBI_Lot', 'Wafer', 'FE_QC_Cumulative_Grade', 'CCY_Status']).size().reset_index().rename(
        columns={0: 'Count'})
    data_FE_summary.to_excel(writer, "Sheet2", startrow=2, startcol=1, index=False, header=False, na_rep='NA')

    data_DS['DS_Grade'] = data_DS['DS_Grade'].fillna('NA')
    data_DS['DSLossReasonName'] = data_DS['DSLossReasonName'].fillna('NA')
    data_DS_summary = data_DS.groupby(
        ['Product', 'PBI_Lot', 'SALot', 'Wafer', 'DS_Grade', 'DSLossReasonName']).size().reset_index().rename(
        columns={0: 'Count'})
    data_DS_summary.to_excel(writer, "Sheet2", startrow=2, startcol=8, index=False, header=False, na_rep='NA')

    data_EOLQC['EOLQC_Grade'] = data_EOLQC['EOLQC_Grade'].fillna('NA')
    data_EOLQC_summary = data_EOLQC.groupby(
        ['Product', 'PBI_Lot', 'SALot', 'Wafer', 'EOLQC_Grade', 'CCY_AC', 'CCY_Status']).size().reset_index().rename(
        columns={0: 'Count'})
    data_EOLQC_summary.to_excel(writer, "Sheet2", startrow=2, startcol=17, index=False, header=False, na_rep='NA')

    data_BE['BELossReasonName'] = data_BE['BELossReasonName'].fillna('NA')
    data_BE_summary = data_BE.groupby(
        ['Product', 'PBI_Lot', 'Wafer', 'BE_Yield', 'BELossReasonName']).size().reset_index().rename(
        columns={0: 'Count'})
    data_BE_summary.to_excel(writer, "Sheet2", startrow=2, startcol=26, index=False, header=False, na_rep='NA')

    data_FR['FRLossReasonName'] = data_FR['FRLossReasonName'].fillna('NA')
    data_FR_summary = data_FR.groupby(
        ['Product', 'PBI_Lot', 'Wafer', 'FR_Yield', 'FRLossReasonName']).size().reset_index().rename(
        columns={0: 'Count'})
    data_FR_summary.to_excel(writer, "Sheet2", startrow=2, startcol=33, index=False, header=False, na_rep='NA')

    data_PDP['PDPLossReasonName'] = data_FR['PDPLossReasonName'].fillna('NA')
    data_PDP_summary = data_PDP.groupby(
        ['Product', 'PBI_Lot', 'Wafer', 'PDP_Yield', 'PDPLossReasonName']).size().reset_index().rename(
        columns={0: 'Count'})
    data_PDP_summary.to_excel(writer, "Sheet2", startrow=2, startcol=40, index=False, header=False, na_rep='NA')

    writer.save()


# %% Use FE_AC and FR_A to calculate CCY_A
def generate_summary_1(path_from, path_to):
    folder_path = path_from
    path_to_summary = os.path.join(path_to, 'Summary_10days_rev1.xlsx')

    if __name__ != '__main__':
        import shutil
        shutil.copyfile('Summary_10days_rev1.xlsx', path_to_summary)

    seq_name = 'ww_run_data_Sequel.csv'
    spider_name = 'ww_run_data_Spider.csv'
    data_seq_raw = pd.read_csv(os.path.join(folder_path, seq_name))
    data_spider_raw = pd.read_csv(os.path.join(folder_path, spider_name))

    col_list = ['Product', 'WorkWeek', 'FE_Yield_AC_n', 'PDP_Yield_n', 'BE_Yield_n', 'DS_Yield_n', 'EOLQC_Yield_n',
                'FR_Yield_n',
                'FE_Yield_A_Yield', 'FE_Yield_AC_Yield', 'BCD_Yield_Yield', 'FE_Yield_BCD_Yield', 'PDP_Yield_Yield',
                'BE_Yield_Yield', 'DS_Yield_Yield', 'EOLQC_Yield_Yield', 'FR_Yield_Yield', 'CCYA_1_locf',
                'CCYAC_1_locf']  # 'FR_Yield_FE_A_Yield'
    data_seq = data_seq_raw[col_list]
    col_list = ['Product', 'WorkWeek', 'FE_Yield_AC_n', 'PDP_Yield_n', 'BE_Yield_n', 'DS_Yield_n', 'EOLQC_Yield_n',
                'FR_Yield_n',
                'FE_Yield_A_Yield', 'FE_Yield_AC_Yield', 'BCD_Yield_Yield', 'FE_Yield_BCD_Yield', 'PDP_Yield_Yield',
                'BE_Yield_Yield', 'DS_Yield_Yield', 'EOLQC_Yield_Yield', 'FR_Yield_Yield', 'CCYA_1_locf',
                'CCYAC_1_locf']
    data_spider = data_spider_raw[col_list]

    col_list = ['Product', 'WorkWeek', 'FE_Yield_A_Yield_13', 'FE_Yield_AC_Yield_13', 'BCD_Yield_Yield_13',
                'FE_Yield_BCD_13',
                'PDP_Yield_Yield_13', 'BE_Yield_Yield_13', 'DS_Yield_Yield_13', 'EOLQC_Yield_Yield_13',
                'FR_Yield_FE_A_13', 'FR_Yield_Yield_13', 'CCYA_13', 'CCYAC_13']  #
    data_seq_13 = data_seq_raw[col_list]
    col_list = ['Product', 'WorkWeek', 'FE_Yield_A_Yield_13', 'FE_Yield_AC_Yield_13', 'BCD_Yield_Yield_13',
                'FE_Yield_BCD_13',
                'PDP_Yield_Yield_13', 'BE_Yield_Yield_13', 'DS_Yield_Yield_13', 'EOLQC_Yield_Yield_13',
                'FR_Yield_FE_A_13', 'FR_Yield_Yield_13', 'CCYA_13', 'CCYAC_13']
    data_spider_13 = data_spider_raw[col_list]

    seq_name = 'ww_run_data_Sequel_ww4_.csv'
    spider_name = 'ww_run_data_Spider_ww4_.csv'
    data_seq_4 = pd.read_csv(os.path.join(folder_path, seq_name))
    data_spider_4 = pd.read_csv(os.path.join(folder_path, spider_name))

    col_list = ['Product', 'WorkWeek', 'FE_Yield_A_Yield_4', 'FE_Yield_AC_Yield_4', 'BCD_Yield_Yield_4',
                'FE_Yield_BCD_Yield_4',
                'PDP_Yield_Yield_4', 'BE_Yield_Yield_4', 'DS_Yield_Yield_4',
                'EOLQC_Yield_Yield_4', 'FR_Yield_Yield_4', 'CCYA_4', 'CCYAC_4']
    data_seq_4 = data_seq_4[col_list]
    col_list = ['Product', 'WorkWeek', 'FE_Yield_A_Yield_4', 'FE_Yield_BCD_Yield_4', 'BCD_Yield_Yield_4',
                'PDP_Yield_Yield_4', 'BE_Yield_Yield_4', 'DS_Yield_Yield_4',
                'EOLQC_Yield_Yield_4', 'FR_Yield_Yield_4', 'CCYA_4', 'CCYAC_4']
    data_spider_4 = data_spider_4[col_list]

    data_seq = pd.merge(data_seq, data_seq_4, how='left', left_on=['Product', 'WorkWeek'],
                        right_on=['Product', 'WorkWeek'])
    data_seq = pd.merge(data_seq, data_seq_13, how='left', left_on=['Product', 'WorkWeek'],
                        right_on=['Product', 'WorkWeek'])
    data_spider = pd.merge(data_spider, data_spider_4, how='left', left_on=['Product', 'WorkWeek'],
                           right_on=['Product', 'WorkWeek'])
    data_spider = pd.merge(data_spider, data_spider_13, how='left', left_on=['Product', 'WorkWeek'],
                           right_on=['Product', 'WorkWeek'])

    data_seq['CCY_FR_A_13'] = data_seq['FE_Yield_AC_Yield_13'] * data_seq['PDP_Yield_Yield_13'] * data_seq[
        'BE_Yield_Yield_13'] * data_seq['DS_Yield_Yield_13'] * data_seq['EOLQC_Yield_Yield_13'] * data_seq[
                                  'FR_Yield_FE_A_13']
    data_spider['CCY_FR_A_13'] = data_spider['FE_Yield_AC_Yield_13'] * data_spider['PDP_Yield_Yield_13'] * data_spider[
        'BE_Yield_Yield_13'] * data_spider['DS_Yield_Yield_13'] * data_spider['EOLQC_Yield_Yield_13'] * data_spider[
                                     'FR_Yield_FE_A_13']

    # date_max =  datetime.strptime(data_seq['WorkWeek'].max()+'-0', '%Y-%U-%w')
    # datetime_object = datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p')
    date_max_raw = max(data_seq['WorkWeek'].max(), data_spider['WorkWeek'].max())
    date_max = date_max_raw.split('-')
    total_ww = 53  # TL some year has 52 ww, some has 53. Need to change every year
    date_max_val = int(date_max[0]) * total_ww + int(date_max[1])
    date_list = []
    for i in range(13):
        date_val_year = (date_max_val - i - 1) // total_ww  # TL solve "ww52"
        date_val_week = (date_max_val - i - 1) % total_ww + 1
        date_val = str(date_val_year) + '-' + '{0:02d}'.format(date_val_week)
        date_list.append(date_val)

    book = load_workbook(path_to_summary)
    writer = pd.ExcelWriter(path_to_summary, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

    data_10days = data_seq[data_seq.WorkWeek.isin(date_list)].T

    col_list = ['WorkWeek', 'FE_Yield_AC_n', 'PDP_Yield_n', 'BE_Yield_n', 'DS_Yield_n', 'EOLQC_Yield_n', 'FR_Yield_n']
    data_10days.loc[col_list, :].to_excel(writer, "Sheet1", startrow=2, startcol=2, index=False, header=False,
                                          na_rep='NA')  # sheet_name = 'x1'

    col_list = ['FE_Yield_A_Yield', 'FE_Yield_AC_Yield', 'PDP_Yield_Yield', 'BE_Yield_Yield', 'DS_Yield_Yield',
                'EOLQC_Yield_Yield', 'FR_Yield_Yield', 'CCYA_1_locf', 'CCYAC_1_locf']
    data_10days.loc[col_list, :].to_excel(writer, "Sheet1", startrow=10, startcol=2, index=False, header=False,
                                          na_rep='NA')
    #    data_10days.iloc[[8,9, 12,13,14,15,16,17,18],:].to_excel(writer, "Sheet1", startrow=10, startcol=2, index=False,header=False, na_rep='NA')

    col_list = ['FE_Yield_A_Yield_4', 'FE_Yield_AC_Yield_4', 'PDP_Yield_Yield_4', 'BE_Yield_Yield_4',
                'DS_Yield_Yield_4', 'EOLQC_Yield_Yield_4', 'FR_Yield_Yield_4', 'CCYA_4', 'CCYAC_4']
    data_10days.loc[col_list, :].to_excel(writer, "Sheet1", startrow=21, startcol=2, index=False, header=False,
                                          na_rep='NA')
    #    data_10days.iloc[[19,20, 23,24,25,26,27,28,29],:].to_excel(writer, "Sheet1", startrow=21, startcol=2, index=False,header=False, na_rep='NA')

    col_list = ['FE_Yield_A_Yield_13', 'FE_Yield_AC_Yield_13', 'PDP_Yield_Yield_13', 'BE_Yield_Yield_13',
                'DS_Yield_Yield_13', 'EOLQC_Yield_Yield_13', 'FR_Yield_FE_A_13', 'FR_Yield_Yield_13', 'CCYA_13',
                'CCY_FR_A_13', 'CCYAC_13']
    data_10days.loc[col_list, :].to_excel(writer, "Sheet1", startrow=32, startcol=2, index=False, header=False,
                                          na_rep='NA')
    #    data_10days.iloc[[30,31, 34,35,36,37,38,39,40],:].to_excel(writer, "Sheet1", startrow=32, startcol=2, index=False,header=False, na_rep='NA')

    data_10days = data_spider[data_spider.WorkWeek.isin(date_list)].T
    col_list = ['WorkWeek', 'FE_Yield_AC_n', 'PDP_Yield_n', 'BE_Yield_n', 'DS_Yield_n', 'EOLQC_Yield_n', 'FR_Yield_n']
    data_10days.loc[col_list, :].to_excel(writer, "Sheet1", startrow=2, startcol=17, index=False, header=False,
                                          na_rep='NA')  # sheet_name = 'x1'
    col_list = ['FE_Yield_BCD_Yield', 'FE_Yield_AC_Yield', 'BCD_Yield_Yield', 'PDP_Yield_Yield', 'BE_Yield_Yield',
                'DS_Yield_Yield', 'EOLQC_Yield_Yield', 'FR_Yield_Yield', 'CCYA_1_locf', 'CCYAC_1_locf']
    data_10days.loc[col_list, :].to_excel(writer, "Sheet1", startrow=10, startcol=17, index=False, header=False,
                                          na_rep='NA')
    #    data_10days.iloc[8:18,:].to_excel(writer, "Sheet1", startrow=10, startcol=17, index=False,header=False, na_rep='NA')
    col_list = ['FE_Yield_BCD_Yield_4', 'FE_Yield_AC_Yield_4', 'BCD_Yield_Yield_4', 'PDP_Yield_Yield_4',
                'BE_Yield_Yield_4', 'DS_Yield_Yield_4', 'EOLQC_Yield_Yield_4', 'FR_Yield_Yield_4', 'CCYA_4', 'CCYAC_4']
    data_10days.loc[col_list, :].to_excel(writer, "Sheet1", startrow=21, startcol=17, index=False, header=False,
                                          na_rep='NA')
    col_list = ['FE_Yield_BCD_13', 'FE_Yield_AC_Yield_13', 'BCD_Yield_Yield_13', 'PDP_Yield_Yield_13',
                'BE_Yield_Yield_13', 'DS_Yield_Yield_13', 'EOLQC_Yield_Yield_13', 'FR_Yield_FE_A_13',
                'FR_Yield_Yield_13', 'CCYA_13', 'CCY_FR_A_13', 'CCYAC_13']
    data_10days.loc[col_list, :].to_excel(writer, "Sheet1", startrow=32, startcol=17, index=False, header=False,
                                          na_rep='NA')

    # %%

    data_all_file = 'all_chip_level_data.csv'
    data_all = pd.read_csv(os.path.join(folder_path, data_all_file))
    # data_FE = data_all[data_all['FE_QC_Cumulative_WW']== date_max_raw]
    data_FE = data_all[data_all['FEWorkWeekOut'] == date_max_raw]
    data_PDP = data_all[data_all['PDPWorkWeekOut'] == date_max_raw]
    data_BE = data_all[data_all['BEWorkWeekOut'] == date_max_raw]
    data_DS = data_all[data_all['DSWorkWeekOut'] == date_max_raw]
    data_FR = data_all[data_all['FRWorkWeekIn'] == date_max_raw]
    data_EOLQC = data_all[data_all['EOLQCWorkWeekOut'] == date_max_raw]
    del data_all

    data_FE['FE_QC_Cumulative_Grade'] = data_FE['FE_QC_Cumulative_Grade'].fillna('NA')
    data_FE_summary = data_FE.groupby(
        ['Product', 'PBI_Lot', 'Wafer', 'FE_QC_Cumulative_Grade', 'CCY_Status']).size().reset_index().rename(
        columns={0: 'Count'})
    data_FE_summary.to_excel(writer, "Sheet2", startrow=2, startcol=1, index=False, header=False, na_rep='NA')

    data_DS['DS_Grade'] = data_DS['DS_Grade'].fillna('NA')
    data_DS['DSLossReasonName'] = data_DS['DSLossReasonName'].fillna('NA')
    data_DS_summary = data_DS.groupby(
        ['Product', 'PBI_Lot', 'SALot', 'Wafer', 'DS_Grade', 'DSLossReasonName']).size().reset_index().rename(
        columns={0: 'Count'})
    data_DS_summary.to_excel(writer, "Sheet2", startrow=2, startcol=8, index=False, header=False, na_rep='NA')

    data_EOLQC['EOLQC_Grade'] = data_EOLQC['EOLQC_Grade'].fillna('NA')
    data_EOLQC_summary = data_EOLQC.groupby(
        ['Product', 'PBI_Lot', 'SALot', 'Wafer', 'EOLQC_Grade', 'CCY_AC', 'CCY_Status']).size().reset_index().rename(
        columns={0: 'Count'})
    data_EOLQC_summary.to_excel(writer, "Sheet2", startrow=2, startcol=17, index=False, header=False, na_rep='NA')

    data_BE['BELossReasonName'] = data_BE['BELossReasonName'].fillna('NA')
    data_BE_summary = data_BE.groupby(
        ['Product', 'PBI_Lot', 'Wafer', 'BE_Yield', 'BELossReasonName']).size().reset_index().rename(
        columns={0: 'Count'})
    data_BE_summary.to_excel(writer, "Sheet2", startrow=2, startcol=26, index=False, header=False, na_rep='NA')

    data_FR['FRLossReasonName'] = data_FR['FRLossReasonName'].fillna('NA')
    data_FR_summary = data_FR.groupby(
        ['Product', 'PBI_Lot', 'Wafer', 'FR_Yield', 'FRLossReasonName']).size().reset_index().rename(
        columns={0: 'Count'})
    data_FR_summary.to_excel(writer, "Sheet2", startrow=2, startcol=33, index=False, header=False, na_rep='NA')

    data_PDP['PDPLossReasonName'] = data_PDP['PDPLossReasonName'].fillna('NA')
    data_PDP_summary = data_PDP.groupby(
        ['Product', 'PBI_Lot', 'Wafer', 'PDP_Yield', 'PDPLossReasonName']).size().reset_index().rename(
        columns={0: 'Count'})
    data_PDP_summary.to_excel(writer, "Sheet2", startrow=2, startcol=40, index=False, header=False, na_rep='NA')

    writer.save()


if __name__ == '__main__':
    current_path = os.path.dirname(os.path.abspath(__file__))
    generate_summary_1(current_path)
