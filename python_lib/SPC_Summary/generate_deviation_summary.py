# -*- coding: utf-8 -*-
"""
Created on Fri Aug 14 11:04:54 2020

@author: ltu
"""

import json, os
dir_path = os.path.dirname(os.path.realpath(__file__))
package_path = os.path.dirname(dir_path)
sp_path = os.path.join(package_path, 'site_packages')
if package_path not in os.sys.path:
    os.sys.path.append( package_path)
if sp_path not in os.sys.path:
    os.sys.path.append( sp_path)
import requests
import re
import pandas as pd
import plotly.graph_objects as go
from matplotlib import pyplot as plt
from PIL import Image
from io import BytesIO
import matplotlib.image as mpimg
from pptx import Presentation
from pptx.util import Inches, Cm
import csv
import warnings
warnings.filterwarnings("ignore")
from IPython.core.display import display, HTML
import datetime

################ get Wafer summary from SmrtCentral
headers = {
    'Content-Type': 'application/json',
    'Accept': 'text/csv'   #application/vnd.openxmlformats-officedocument.spreadsheetml.sheet   
}
df_wafer = pd.DataFrame()
start_date = '2020-01-01'
query = {
  "start": 0,
  "length": 3,
  "columns": [
    {
      "name": "Operation",
      "search": 'Finished Goods Inventory'
    },
    {"name": "ContainerName"},
    {"name": "CustomerLot"},
    {"name": "BatchAlias"},
    {"name": "Owner"},
    {"name": "System"},
    {"name": "QtyA"},
    {"name": "QtyB"},
    {"name": "QtyC"},
    {"name": "QtyD"},
    {"name": "QtyFTC"},
    {"name": "QtyFTD"},
    {"name": "MetrologyStatus"},
    {"name": "DeviationLot"},
    {"name": "NCR"},
    {"name": "LastMoveDate", 
     "search": start_date}
  ],
  "order": {
    "column": 1,
    "dir": "desc"
  }
}
data = json.dumps(query)
response = requests.post('http://itg/metrics-dev/scc/tables/Wafer/export', headers=headers, data=data)
abc = response.content
aaa = abc.decode().split('\n')


def get_section(s, idx):
    if idx >= len(s):
        return None
    elif s[idx]=='"':
        end = s[idx+1: ].find('"')
        return (s[idx+1: idx+1+end].replace(',',' '), idx+1+end+2)
    else:
        end = s[idx: ].find(',')
        return (s[idx: idx+end], idx+end+1)

data_list = []
for i in range(1, len(aaa) ):
    line = aaa[i]
    idx = 0
    line_list = []
    while get_section(line, idx):
        item = get_section(line, idx)
        line_list += [item[0]]
        idx = item[1]
    data_list += [line_list]
    
df_wafer = df_wafer.append(pd.DataFrame(data_list, columns = aaa[0].split(',')), ignore_index=True)

df_wafer['Date'] = df_wafer['"LastMoveDate"'].apply(lambda x:datetime.datetime.strptime(x[:10], '%Y-%m-%d'))
#df_wafer = df_wafer[df_wafer['Date']>datetime.datetime(2020, 1, 1)]
df_wafer['Year-Month'] = df_wafer['Date'].apply(lambda x: '{0}-{1:02d}'.format(x.year, x.month) )


df_dev_date = pd.DataFrame(columns= ['System','wafer','Year-Month','deviation'])
for idx,row in df_wafer.iterrows():
#    print(row)
#    print(row['"deviation"'])
    deviation_list = row['"Deviation"'].split(' ')
    for dev in deviation_list:
        df_dev_date.loc[len(df_dev_date)] =[row['"System"'], row['"Wafer"'], row['Year-Month'], dev]
        
month_list = df_dev_date['Year-Month'].unique()
month_list.sort()
for month in month_list:
    df_month = df_dev_date[df_dev_date['Year-Month']==month]
    wafer_num_month = df_month['wafer'].unique().size
    dev_sum = df_month['deviation'].value_counts().to_frame()
    dev_sum['deviation %'] = dev_sum['deviation'] / wafer_num_month * 100
    
    
    