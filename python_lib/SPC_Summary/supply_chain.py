# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 13:28:42 2019

@author: ltu
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
# import statsmodels.api as sm # import statsmodels
import os
from os import listdir
from os.path import isfile, isdir, join
import datetime
# from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from openpyxl import load_workbook
from tqdm import tqdm
import pyodbc
import pickle

# folder_path = os.path.dirname(os.path.abspath('__file__'))
# ww = '31'  #NEED TO UPDATE
# folder_path = r'\\sharepoint\mfg\sequelcell\Documents\Manufacturing Engineering\Yield Reports\Weekly Yield Meetings\WW9'+ww+r'\Data'
folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'
file_name = 'all_chip_level_data.csv'
save_folder = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Inventory_pipeline'

data = pd.read_csv(os.path.join(folder_path, file_name))
col_names = ['Product', 'Chip', 'Wafer', 'FEWorkWeekIn', 'FEWorkWeekOut', 'FELossReasonName', 'FE_QC_Cumulative_Grade',
             'PDPWorkWeekIn', 'PDPWorkWeekOut', 'PDPLossReasonName', 'BEWorkWeekIn', 'BEWorkWeekOut',
             'BELossReasonName',
             'DSWorkWeekIn', 'DSWorkWeekOut', 'DSLossReasonName', 'DS_Grade', 'EOLQCWorkWeekIn', 'EOLQCWorkWeekOut',
             'EOLQCLossReasonName', 'EOLQC_Grade',
             'FRWorkWeekIn', 'FRWorkWeekOut', 'FRLossReasonName', 'FE_Yield_AC', 'PDP_Yield', 'BE_Yield', 'DS_Yield',
             'EOLQC_Yield', 'FR_Yield', 'OwnerName', 'CCY_Status']
data = data[col_names]
data['FE_Yield'] = data['FE_Yield_AC']
op_list = ['FE', 'PDP', 'BE', 'DS', 'EOLQC', 'FR']
ww_col = ['FEWorkWeekIn', 'PDPWorkWeekIn', 'BEWorkWeekIn', 'DSWorkWeekIn', 'EOLQCWorkWeekIn', 'FRWorkWeekIn',
          'FRWorkWeekOut']
ww_list = []
for col in ww_col:
    ww_list += data[col].unique().tolist()
ww_set = set(ww_list)
ww_set.remove(np.nan)
ww_list = list(ww_set)
ww_list.sort()


class op_inventory:
    def __init__(self):
        self.FE_in, self.FE, self.FE_loss = 0, 0, 0
        self.PDP_in, self.PDP, self.PDP_loss = 0, 0, 0
        self.BE_in, self.BE, self.BE_loss = 0, 0, 0
        self.DS_in, self.DS, self.DS_loss = 0, 0, 0
        self.EOLQC_in, self.EOLQC, self.EOLQC_loss = 0, 0, 0
        self.FR_in, self.FR, self.FR_loss, self.FR_out = 0, 0, 0, 0

    @property
    def FE_out(self):
        return self.PDP_in

    @property
    def PDP_out(self):
        return self.BE_in

    @property
    def BE_out(self):
        return self.DS_in

    @property
    def DS_out(self):
        return self.EOLQC_in

    @property
    def EOLQC_out(self):
        return self.FR_in


data = data[data.CCY_Status != '5_Inactive']
# data = data[data.OwnerName=='PROD']

result = {}

for prod in ['Spider', 'Sequel']:
    data1 = data[data.Product == prod]
    for i in range(30):
        ww = ww_list[-1 - i]
        if ww not in result.keys():
            result[ww] = op_inventory()
        for op in op_list:
            #        exec('''result[ww].{0} = len(data1[(data1.{0}WorkWeekIn<=ww) & ((data1.{0}WorkWeekOut>ww) | pd.isnull(data1.{0}WorkWeekOut))
            #        & ((data1.CCY_Status==\'6_IP\') | (data1.CCY_Status==\'7_Yielded\')) & pd.isnull(data1.{0}LossReasonName)])'''.format(op))
            #        exec('result[ww].{0}_in = len(data1[data1.{0}WorkWeekIn==ww])'.format(op))
            #        exec('''result[ww].{0}_loss = len(data1[(data1.{0}WorkWeekIn==ww) & ( pd.isnull(data1.{0}WorkWeekOut))
            #        & ((data1.CCY_Status!=\'6_IP\') & (data1.CCY_Status!=\'7_Yielded\') ) ])'''.format(op))
            exec('''result[ww].{0} = len(data1[(data1.{0}WorkWeekIn<=ww) & ( ((data1.{0}WorkWeekOut>ww) & (data1.{0}_Yield==1)) | ((pd.isnull(data1.{0}WorkWeekOut)) & (data1.{0}_Yield!=0)) )
             ])'''.format(op))
            exec('result[ww].{0}_in = len(data1[data1.{0}WorkWeekIn==ww])'.format(op))
            exec('''result[ww].{0}_loss = len(
                    data1[  
                            (data1.{0}WorkWeekIn==ww) & 
                            ( (~pd.isnull(data1.{0}LossReasonName)) | (data1.{0}_Yield==0) | ((op=='EOLQC') & (data1.CCY_Status=='3_FTC')) )
                    ]
                )'''.format(op))
        result[ww].FR_out = len(data1[(data1.FRWorkWeekOut == ww) & (data1.CCY_Status == '7_Yielded')])

    result_table = pd.DataFrame()
    for op in op_list:
        x, y1, y2, y3, y4 = [], [], [], [], []
        for k in result.keys():
            x.append(k)
            exec('y1.append(result[k].{}_in)'.format(op))
            exec('y2.append(result[k].{})'.format(op))
            exec('y3.append(result[k].{}_loss)'.format(op))
            exec('y4.append(result[k].{}_out)'.format(op))
        idx_sort = sorted(range(len(x)), key=lambda i: x[i])
        x = [x[i] for i in idx_sort]
        y1 = [y1[i] for i in idx_sort]
        y2 = [y2[i] for i in idx_sort]
        y3 = [y3[i] for i in idx_sort]
        y4 = [y4[i] for i in idx_sort]
        fig, ax = plt.subplots(figsize=(15, 5))
        plt.plot(x, y1, x, y2, 'yo-', x, y3, x, y4)
        plt.legend(['in', 'inventory', 'loss', 'out'])
        plt.title(prod + ' - ' + op)
        plt.xticks(rotation=90)
        ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)

        fig.savefig(os.path.join(save_folder, 'ww-' + prod + '-' + op + '.png'))
        plt.close(fig)

        temp = [[prod for i in range(len(x))], [op for i in range(len(x))], x, y1, y2, y3, y4]
        result_table = result_table.append(np.array(temp).T.tolist(), ignore_index=True)

result_table.columns = ['prod', 'operation', 'ww', 'in', 'available', 'loss', 'out']
result_table.to_csv(os.path.join(save_folder, 'chip_op_inventory.csv'))

########### Calculate Duration Days  ##############################
conn = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes spidermetrology
conn.autocommit = True
today = datetime.date.today()
date_start = datetime.date(today.year - 3, today.month, 1)
get_data_sql = '''select distinct Chip, Substrate, FromOperation, MoveDate
      from [insitedb_schema].[View_MoveHistoryByOpAndDateRange]
      where FromOperation in ('FEOSM Scribe', 'Wafer QC Receive','QC Staging Area','SA Lot Formation','VQC2','Wafer Ship','Final Packaging Receive','Dry Sort','Functional Test Gate','Boxing', 'Final Release')
      and MoveDate >= '{0}' --and MoveDate <= '{1}'
      order by MoveDate
'''.format(date_start, today)
data_raw = pd.read_sql(get_data_sql, conn)
data_pivot = data_raw.pivot_table(index=['Chip'], columns='FromOperation', values='MoveDate',
                                  aggfunc=np.max).reset_index()  # delete 'Substrate' in summary
data_pivot['FE_days'] = (data_pivot['Wafer QC Receive'] - data_pivot['FEOSM Scribe']).apply(lambda x: x.days)
data_pivot['Metrology_days'] = (data_pivot['QC Staging Area'] - data_pivot['Wafer QC Receive']).apply(lambda x: x.days)
data_pivot['PrePDP_days'] = (data_pivot['SA Lot Formation'] - data_pivot['QC Staging Area']).apply(lambda x: x.days)
data_pivot['PDP_days'] = (data_pivot['Wafer Ship'] - data_pivot['SA Lot Formation']).apply(
    lambda x: x.days)  # VQC2 --> Wafer Ship
data_pivot['BE_days'] = (data_pivot['Final Packaging Receive'] - data_pivot['Wafer Ship']).apply(lambda x: x.days)
data_pivot['DS_days'] = (data_pivot['Dry Sort'] - data_pivot['Final Packaging Receive']).apply(lambda x: x.days)
data_pivot['EOLQC_days'] = (data_pivot['Functional Test Gate'] - data_pivot['Dry Sort']).apply(lambda x: x.days)
data_pivot['FR_days'] = (data_pivot['Final Release'] - data_pivot['Functional Test Gate']).apply(
    lambda x: x.days)  # Boxing --> Functional Test Gate
data_pivot['Total_days'] = (data_pivot['Final Release'] - data_pivot['Final Packaging Receive']).apply(lambda x: x.days)

data_all = pd.merge(data_pivot, data, how='left', left_on=['Chip'], right_on=['Chip'])
# data_all.to_csv('data.csv')
data_all.to_csv(os.path.join(save_folder, 'data.csv'))
########### Calculate Duration Dist  ##############################
ww_col = ['FEWorkWeekIn', 'PDPWorkWeekIn', 'BEWorkWeekIn', 'DSWorkWeekIn', 'EOLQCWorkWeekIn', 'FRWorkWeekIn']
ww_list = []
for col in ww_col:
    ww_list += data_all[col].unique().tolist()
ww_set = set(ww_list)
ww_set.remove(np.nan)
ww_list = list(ww_set)
ww_list.sort()
op_list_new = ['FE', 'Metrology', 'PrePDP', 'PDP', 'BE', 'DS', 'EOLQC', 'FR', 'Total']
result = {'Spider': {}, 'Sequel': {}}
for op in op_list_new:
    result['Spider'][op] = {ww: [] for ww in ww_list}
    result['Sequel'][op] = {ww: [] for ww in ww_list}

with open(os.path.join(save_folder, 'result.pickle'), 'wb') as handle:
    pickle.dump(result, handle, protocol=pickle.HIGHEST_PROTOCOL)


# with open(os.path.join(save_folder, 'result.pickle'), 'rb') as handle:
#    b = pickle.load(handle)


def date_diff(a, b):
    return int(b[0:4]) * 52 + int(b[5:]) - int(a[0:4]) * 52 - int(a[5:])


class BreakIt(Exception): pass


try:
    for prod in ['Sequel', 'Spider']:
        data1 = data_all[data_all.Product == prod]
        for idx, row in tqdm(data1.iterrows()):
            for op in op_list_new:
                if (not pd.isna(row[op + '_days'])):
                    try:
                        if (op == 'Metrology') | (op == 'PrePDP'):
                            result[prod][op][row['FEWorkWeekOut']].append(
                                row[op + '_days'])  # Summary by WW_In or WW_Out
                        elif op == 'Total':
                            result[prod][op][row['FRWorkWeekOut']].append(row[op + '_days'])
                        else:
                            result[prod][op][row[op + 'WorkWeekOut']].append(
                                row[op + '_days'])  # Summary by WW_In or WW_Out
                    except:
                        #                        raise BreakIt
                        pass
except BreakIt:
    print(op)
    print(row)

#            ww_in = row[op + 'WorkWeekIn']
#            ww_out = row[op + 'WorkWeekOut']
#            if (not pd.isna(ww_in)) & (not pd.isna(ww_out)):
#                result[prod][op][ww_in].append(date_diff(ww_in, ww_out))

op_ww = {'FE': (0, 150, 10), 'Metrology': (0, 100, 10), 'PrePDP': (0, 50, 5), 'PDP': (0, 15, 2), 'BE': (0, 100, 5),
         'DS': (0, 30, 3), 'EOLQC': (0, 50, 5), 'FR': (0, 50, 5), 'Total': (0, 150, 15)}
op_info = {'FE': ('FEOSM Scribe', 'QC Recieve'), 'Metrology': ('QC Recieve', 'QC Staging'),
           'PrePDP': ('QC Staging', 'SA Lot Formation'), 'PDP': ('SA Lot Formation', 'Wafer Ship'),
           'BE': ('Wafer Ship', 'Final Packaging Receive'), 'DS': ('Final Packaging Receive', 'Dry Sort'),
           'EOLQC': ('Dry Sort', 'Functional Test Gate'), 'FR': ('Functional Test Gate', 'Final Release'),
           'Total': ('Final Packaging Receive', 'Final Release')}
for prod in ['Spider', 'Sequel']:
    for op in op_list_new:
        fig, ax = plt.subplots(figsize=(10, 5))
        abc = [a for key, value in result[prod][op].items() for a in value]  # if key > '2020-0'
        _ = plt.hist(abc, bins=np.arange(*op_ww[op]), density=True, color='b', Label='30 ww')  # bins='auto'
        abc = [a for key, value in result[prod][op].items() if key > '2020-0' for a in value]  #
        _ = plt.hist(abc, bins=np.arange(*op_ww[op][:2]), density=True, color='r', Label='4 ww')  # bins='auto'
        ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
        plt.title(prod + ': Hist of #day in ' + op + ' ' + str(op_info[op]))
        # plt.show()

        fig.savefig(os.path.join(save_folder, 'hist-' + prod + '-' + op + '.png'))
        plt.close(fig)
