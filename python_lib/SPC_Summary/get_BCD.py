# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 16:45:17 2020

@author: ltu
"""

import pandas as pd
import numpy as np
import pyodbc
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn import preprocessing
import matplotlib.patheffects as path_effects
import os


# FE_test = ['OQA','EOQC','EOQC_mode','BCD','OQA_C/F']  #only add new features at the end
# FE_test_name = ['SLT_OQA_DefectId','FTT_0000_DB_NOP_000','FTT_FIRST_FAILURE', 'PBI_bcd_fit', 'SLT_OQA_DefectId']
# grade = [{'C':'D21','F':['D29','D41']}, {'F':'9'}, {'A':'None'}, {'A':[85,125]}, {'A':'None'}]  #OQA {'A':['D{0:02}'.format(i) for i in list(range(1,21)) + list(range(22,28)) + list(range(30,41))]}
#    
# def FE_Defects_Plot(wafer = 'AX18075-20', idx = 4):

def get_BCD_fit_chip(chip_list):
    if os.name == 'nt':
        conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
    else:
        server = 'usmp-vm-campd8d\sql2012ods'
        # database = 'CamODS_6X'
        username = 'odbc'
        password = 'readonly'
        conn_MES = pyodbc.connect(
            'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)

    conn_MES.autocommit = True
    get_data_sql = '''SELECT ChipID, DataValue
    FROM [CamODS_6X].[insitedb_schema].[PBI_GetBCDModel1Data]
    WHERE DataName like 'PBI_bcd_fit%' and ChipID in ('{}')
    '''.format('\',\''.join(chip_list))
    data_fit = pd.read_sql(get_data_sql, conn_MES)
    data_fit['BCD (nm)'] = data_fit['DataValue'].astype('float') * 1e9
    data_fit = data_fit.drop(columns=['DataValue'])
    return data_fit


def get_BCD_fit_wafer(wafer):
    if os.name == 'nt':
        conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
    else:
        server = 'usmp-vm-campd8d\sql2012ods'
        # database = 'CamODS_6X'
        username = 'odbc'
        password = 'readonly'
        conn_MES = pyodbc.connect(
            'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)

    conn_MES.autocommit = True
    get_data_sql = '''SELECT ChipID, DataValue
    FROM [CamODS_6X].[insitedb_schema].[PBI_GetBCDModel1Data]
    WHERE DataName like 'PBI_bcd_fit%' and ChipID like '{0}%'
    '''.format(wafer)
    data_fit = pd.read_sql(get_data_sql, conn_MES)
    data_fit['BCD (nm)'] = data_fit['DataValue'].astype('float') * 1e9
    data_fit = data_fit.drop(columns=['DataValue'])
    data_fit['wafer'] = wafer
    return data_fit


if __name__ == '__main__':
    #    lots = {'SP6':'AX20130', 'SP7':'AX23065', 'SP8':'AX27088', 'SP9':'AX28101', 'SP10':'AX30071',
    #            'SP11':'AX32168', 'SP12':'AX34164', 'SP13':'AX36000', 'SP14':'AX38092', 'SP15':'AX42181',
    #            'SP16':'AX45216', 'SP17':'AX47173', 'SP18':'AX49205', 'SP19':'AX51177', 'SP20':'AX50159',
    #            'SP21':'AX48206', 'SP22':'AX52215', 'SP23':'AY01090', 'SP24':'AY02185', 'SP25':'AY03215', 'SP26':'AY06153'}
    lots = {'FL100': 'AY23268'}
    result = pd.DataFrame()
    for key, value in lots.items():
        result_lot = get_BCD_fit_wafer(value)
        if not result_lot.empty:
            result_lot['PBI_Lot'] = key
            result = result.append(result_lot)
    #    result['BCD_Grade'] = result.apply(lambda x: 'A' if x['BCD (nm)']<115 else 'C', axis = 1)
    result['ChipIndex'] = result['ChipID'].apply(lambda x: x[11:15])
    result['x'] = result.apply(lambda x: int(x['ChipIndex']) // 100, axis=1)
    result['y'] = result.apply(lambda x: int(x['ChipIndex']) % 100, axis=1)
    result.to_csv(os.path.join(r'\\pbi\dept\chip\ChipEng\Liang\Yield\BCD_Re_Estimation\Data', 'FL100_combined_data.csv'),
                  index=False)


def get_BCD_all():
    #
    conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')  # spidermetrology
    cursor = conn.cursor()
    conn.autocommit = True

    conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
    cursor = conn_MES.cursor()
    conn_MES.autocommit = True

    # data_all= pd.DataFrame(columns=['Wafer','Row','Col','Test', 'Value'])
    # wafer_list = data_raw['WaferName'].unique()

    #     GET BCD RAW from MES
    get_data_sql = '''SELECT *
    FROM [CamODS_6X].[insitedb_schema].[PBI_GetBCDInlineData]
    WHERE DataName like 'SLT_spider_ezmw_alo_etch_act_middle_cd%'
    '''
    data_raw = pd.read_sql(get_data_sql, conn_MES)
    data_raw.to_csv('BCD_raw_MES.csv')

    #     GET BCD FIT and Grade from MES
    get_data_sql = '''SELECT *
    FROM [CamODS_6X].[insitedb_schema].[PBI_GetBCDModel1Data]
    WHERE DataName like 'PBI_bcd_fit%' ---and ChipID like 'AX49206-09-%'
    '''
    data_raw = pd.read_sql(get_data_sql, conn_MES)
    data_raw['x'] = data_raw['ChipID'].apply(lambda x: x[11:13])
    data_raw['y'] = data_raw['ChipID'].apply(lambda x: x[13:15])
    data_raw.to_csv('BCD_FIT_Grade_MES.csv')

    #     GET BCD RAW from SpiderMetroloy with chip-idx
    get_data_sql = '''SELECT   [WaferName] AS [WaferID],    CONCAT([WaferName], '-',[ChipIndex]) AS [ChipID],   'Inline' AS [ParameterType],   
    CONCAT([DataName], '-', CAST(SUBSTRING([SiteName], 5, CHARINDEX('Of', [SiteName]) - 5) AS char)) AS [ParameterName],  
    CAST(SUBSTRING([SiteName],  CHARINDEX('Of', [SiteName]) + 2, DATALENGTH([SiteName])) AS char) as [TotalSite],
    CAST([DoubleValue] AS float) AS [ParameterValue],   'Float' AS [ParameterValueType] 
    FROM [SpiderMetrology].[dbo].[WaferData] AS [W1] 
    LEFT JOIN [SpiderMetrology].[dbo].[Wafer]   ON [W1].[WaferId] = [Wafer].[WaferId] 
    LEFT JOIN [SpiderMetrology].[dbo].[DataUploadSession] AS [D1]   ON [W1].[DataUploadSessionId] = [D1].[DataUploadSessionId] 
    LEFT JOIN [SpiderMetrology].[dbo].[DataName]   ON [W1].[DataNameId] = [DataName].[DataNameId] 
    LEFT JOIN [SpiderMetrology].[dbo].[Site]   ON [W1].[SiteId] = [Site].[SiteId] 
    INNER JOIN (
    SELECT   [WaferId],   [SiteId],   [DataNameId],   MAX([DataUploadTime]) AS 'MaxDataUploadTime' 
    FROM (
    	SELECT   [WaferId],   [SiteId],   [DataNameId],   [DataUploadTime] FROM [SpiderMetrology].[dbo].[WaferData] AS [W1] LEFT JOIN [SpiderMetrology].[dbo].[DataUploadSession] AS [D1]   ON [W1].[DataUploadSessionId] = [D1].[DataUploadSessionId]) 
    	AS [W2] 
    	GROUP BY [WaferId],          [SiteId],          [DataNameId]) 
    	AS [W2]   
    ON ([W1].[WaferId] = [W2].[WaferId]   AND [W1].[SiteId] = [W2].[SiteId]   AND [W1].[DataNameId] = [W2].[DataNameId]   AND [D1].[DataUploadTime] = [W2].[MaxDataUploadTime]) 
    WHERE [DataName] LIKE 'SLT_spider_ezmw_alo_etch_act_middle_cd%%' AND [WaferName] LIKE 'A%' 
    ---AND [ParameterName] like 'SLT_spider_ezmw_alo_etch_act_middle_cd%' '''
    data_raw = pd.read_sql(get_data_sql, conn)
    data_raw['x'] = data_raw['ChipID'].apply(lambda x: x[11:13])
    data_raw['y'] = data_raw['ChipID'].apply(lambda x: x[13:15])
    data_raw.to_csv('BCD_raw_SpiderMetrology.csv')

    #     GET BCD Grade from MES
    get_data_sql = '''SELECT
          C.ContainerName Chip,
          CD.pbi_substrateid,
          MD.PBI_OperationName,
          MD.ChangeCount,
          MD.PBI_ChipGrade,
          MD.PBI_LastUpdatedTime
          FROM [Container] C
          inner join [ContainerLevel] CL on C.LevelId = CL.ContainerLevelId
          inner join [PBI_MetrologyDetails] MD on C.ContainerId = MD.ParentId
          inner join [ContainerDetail] CD on C.DetailId = CD.ContainerDetailId
          inner join [PBI_ChipStatus] chipstatus on CD.PBI_ChipStatusId = chipstatus.PBI_ChipStatusId
          inner join [ContainerDetail] PCD on CD.PBI_CustomerWaferName = PCD.PBI_CustomerWaferName
          inner join [Container] PC on PCD.ContainerId = PC.ContainerId
          inner join [ContainerLevel] PCL on PC.LevelId = PCL.ContainerLevelId
          WHERE (MD.PBI_LastUpdatedTime > '06/01/19' )
          and PC.ContainerName like '%%[0-9][0-9]'
          and CL.ContainerLevelName = 'CHIP'
          and PCL.ContainerLevelName = 'WAFER'
          and MD.ChangeCount > 1
          and MD.PBI_OperationName like 'BCD%' '''
    data_raw = pd.read_sql(get_data_sql, conn_MES)
    data_raw.to_csv('BCD_Grade_MES.csv')

    #     GET BCD fit from SpiderMetrology
    product = 'Spider'
    test_name = 'PBI_bcd_fit'  # 'PBI_bcd_fit'
    get_data_sql = '''
    SET NOCOUNT ON
    DECLARE @LotName AS NVARCHAR(128)
    --SET @LotName = '{0}%'
    
    DECLARE @RawTable TABLE (
           WaferName NVARCHAR(255),
           BaeWaferName NVARCHAR(255),
           ChipIndex INT,
           SubstrateId CHAR(8),
           DataName NVARCHAR(255),
           DataValue real, --NVARCHAR(255),
           Unit NVARCHAR(255),
           DataUploadTime DATETIME,
           DataCollectionTime DATETIME,
           [FileName] NVARCHAR(255),
           [Description] NVARCHAR(255)
           )
    
    
    
    INSERT INTO @RawTable 
           (WaferName, BaeWaferName, ChipIndex,SubstrateId,DataName,DataValue,Unit,DataUploadTime,
                  DataCollectionTime,
                  [Description])
           SELECT 
                          WaferName,
                          BaeWaferName,
                          ChipIndex,
                          {1}Metrology.dbo.ConvertSubstrateIdToString(SubstrateId),
                          DataName,
                          DataValue,
                          Unit,
                          DataUploadTime,
                          DataCollectionTime,
                          [Description]
                  FROM (SELECT ChipId, DataUploadSessionId, DataNameId, CAST(CAST(ChipData.DataValue as NVARCHAR(255)) as real) as DataValue
                        FROM [{1}Metrology].[dbo].[ChipData] ) ChipData
                          JOIN [{1}Metrology].[dbo].[Chip] Chip ON ChipData.ChipId = Chip.ChipId
                          JOIN [{1}Metrology].[dbo].[Wafer] Wafer ON Chip.WaferId = Wafer.WaferId
                          JOIN [{1}Metrology].[dbo].[DataName] DataName ON ChipData.DataNameId = DataName.DataNameId
                          JOIN [{1}Metrology].[dbo].[DataUploadSession] DataUploadSession ON ChipData.DataUploadSessionId = DataUploadSession.DataUploadSessionId
                  --WHERE Wafer.WaferName LIKE @LotName
                  AND DataName LIKE '%{2}%';
    
    
    SELECT 
           rt.WaferName,
           BaeWaferName,
           rt.ChipIndex,
           SubstrateId,
           rt.DataName,
           DataValue,
           Unit,
           rt.DataUploadTime,
           DataCollectionTime,
           [Description]
    FROM @RawTable rt
    INNER JOIN (
                  select
                          WaferName,
                          ChipIndex,
                          DataName,
                          max(DataUploadTime) as DataUploadTime
                  from @RawTable group by WaferName,ChipIndex,DataName
             ) sd
    ON
           rt.WaferName = sd.WaferName AND
           rt.ChipIndex = sd.ChipIndex AND
           rt.DataName = sd.DataName AND
           rt.DataUploadTime = sd.DataUploadTime
           order by SubstrateId'''.format('', product, test_name)

    # abc = cursor.execute(get_data_sql).fetchall()
    data_raw = pd.read_sql(get_data_sql, conn)
    data_raw['chip_ID'] = data_raw['WaferName'] + '-' + data_raw['ChipIndex'].apply(str)
    data_raw.to_csv('BCD_data.csv')

    conn_MES.close()
    conn.close()

#     TEST: GET EOQC0 from SpiderMetrology
# get_data_sql = '''Select Wafer.LotName,DataName.DataName, Wafer.WaferName, Wafer.BaeWaferName, Chip.ChipIndex,ChipData.DoubleValue, ChipData.DataValue, ChipData.DataNameId, DataUploadSession.DataCollectionTime, DataUploadSession.DataCollectionHardwareID, Hardware.HardwareName
#    From 
#   (Select ChipData.ChipId, ChipData.DataNameId, MAX(ChipData.DataUploadSessionId) AS DataUploadSessionId
#   From [SpiderMetrology].[dbo].[ChipData]
#   Where (ChipData.ChipId like '%') and (ChipData.DataNameId IN (38626))
#   Group by ChipData.ChipId,ChipData.DataNameId) As latestcd
#
#    JOIN [SpiderMetrology].[dbo].[ChipData] On ChipData.ChipId=latestcd.ChipId
#    JOIN [SpiderMetrology].[dbo].[DataName] On DataName.DataNameId=ChipData.DataNameId
#    JOIN [SpiderMetrology].[dbo].[Chip] ON latestcd.ChipID=Chip.ChipID
#    JOIN [SpiderMetrology].[dbo].[Wafer] On Wafer.WaferId=Chip.WaferId
#    JOIN [SpiderMetrology].[dbo].[DataUploadSession] On DataUploadSession.DataUploadSessionId=ChipData.DataUploadSessionId
#    Join [SpiderMetrology].[dbo].[Hardware] On Hardware.HardwareId=DataUploadSession.DataCollectionHardwareID
#    Where (DataName.DataName like 'WS1_0000_D%') and ((Wafer.BaeWaferName like 'P%')) '''
# data_raw = pd.read_sql(get_data_sql, conn)
