import os, sys

dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if dir_path not in sys.path:
    sys.path.append(dir_path)

from util_liang.sql_collections import gen_wafer_dev

gen_wafer_dev()
