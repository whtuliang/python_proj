# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 14:02:42 2019

@author: ltu
"""

import os
import pandas as pd
import numpy as np
import pyodbc
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn import preprocessing
import matplotlib.patheffects as path_effects

wafer = 'AY23268-19'
idx = 4

FE_test = ['OQA', 'EOQC_Grade', 'EOQC_mode', 'EOQC_Failure_Bin', 'BCD', 'OQA_C/F',
           'EOQC0_Bin']  # only add new features at the end
FE_test_name = ['SLT_OQA_DefectId', 'FTT_0000_DB_NOP_000', 'FTT_FIRST_FAILURE', 'FTT_0000_DS_NOP_000', 'PBI_bcd_fit',
                'SLT_OQA_DefectId', 'WS1_0000_DS']  # 'FTT_FIRST_FAILURE'
grade = [{'C': 'D21', 'F': ['D29', 'D41']}, {'F': '9'}, {'A': 'None'}, {'A': 'None'}, {'A': [85, 115]}, {
    'A': 'None'}, {'A': [1,
                         7]}]  # OQA {'A':['D{0:02}'.format(i) for i in list(range(1,21)) + list(range(22,28)) + list(range(30,41))]}


def FE_Defects_Plot(wafer='AX28101-06', idx=2):
    conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')  # camstarsql_mes
    cursor = conn.cursor()
    conn.autocommit = True

    wafer_name = wafer  # 'AW33042-'
    if wafer_name[0] == 'P':
        product = 'Sequel'
    else:
        product = 'Spider'

    test_name = FE_test_name[idx]

    get_data_sql = '''
    SET NOCOUNT ON
    DECLARE @LotName AS NVARCHAR(128)
    SET @LotName = '{0}%'
    
    DECLARE @RawTable TABLE (
           WaferName NVARCHAR(255),
           BaeWaferName NVARCHAR(255),
           ChipIndex INT,
           SubstrateId CHAR(8),
           DataName NVARCHAR(255),
           DataValue NVARCHAR(255),
           Unit NVARCHAR(255),
           DataUploadTime DATETIME,
           DataCollectionTime DATETIME,
           [FileName] NVARCHAR(255),
           [Description] NVARCHAR(255)
           )
    
    
    
    INSERT INTO @RawTable 
           (WaferName, BaeWaferName, ChipIndex,SubstrateId,DataName,DataValue,Unit,DataUploadTime,
                  DataCollectionTime,
                  [Description])
           SELECT 
                          WaferName,
                          BaeWaferName,
                          ChipIndex,
                          {1}Metrology.dbo.ConvertSubstrateIdToString(SubstrateId),
                          DataName,
                          DataValue,
                          Unit,
                          DataUploadTime,
                          DataCollectionTime,
                          [Description]
                  FROM (SELECT ChipId, DataUploadSessionId, DataNameId, CAST(ChipData.DataValue as NVARCHAR(255)) as DataValue
                        FROM [{1}Metrology].[dbo].[ChipData] ) ChipData
                          JOIN [{1}Metrology].[dbo].[Chip] Chip ON ChipData.ChipId = Chip.ChipId
                          JOIN [{1}Metrology].[dbo].[Wafer] Wafer ON Chip.WaferId = Wafer.WaferId
                          JOIN [{1}Metrology].[dbo].[DataName] DataName ON ChipData.DataNameId = DataName.DataNameId
                          JOIN [{1}Metrology].[dbo].[DataUploadSession] DataUploadSession ON ChipData.DataUploadSessionId = DataUploadSession.DataUploadSessionId
                  WHERE Wafer.WaferName LIKE @LotName
                  AND DataName LIKE '%{2}%';
    
    
    SELECT 
           rt.WaferName,
           BaeWaferName,
           rt.ChipIndex,
           SubstrateId,
           rt.DataName,
           DataValue,
           Unit,
           rt.DataUploadTime,
           DataCollectionTime,
           [Description]
    FROM @RawTable rt
    INNER JOIN (
                  select
                          WaferName,
                          ChipIndex,
                          DataName,
                          max(DataUploadTime) as DataUploadTime
                  from @RawTable group by WaferName,ChipIndex,DataName
             ) sd
    ON
           rt.WaferName = sd.WaferName AND
           rt.ChipIndex = sd.ChipIndex AND
           rt.DataName = sd.DataName AND
           rt.DataUploadTime = sd.DataUploadTime
           order by SubstrateId'''.format(wafer_name, product, test_name)  # wafer_name

    # abc = cursor.execute(get_data_sql).fetchall()
    data_raw = pd.read_sql(get_data_sql, conn)
    data_all = pd.DataFrame(columns=['Wafer', 'Row', 'Col', 'Test', 'Value'])
    wafer_list = data_raw['WaferName'].unique()

    for wafer in wafer_list:
        data = data_raw[data_raw.WaferName == wafer]
        # data = data.loc[~data['ChipIndex'].duplicated(keep='last')]
        data['DataValue'] = data[['ChipIndex', 'DataValue']].groupby(['ChipIndex'])['DataValue'].transform(
            lambda x: '+'.join(x))
        data = data[['ChipIndex', 'DataValue']].drop_duplicates()
        if idx in (2, 5):
            labels = data['DataValue'].unique()
            labels = np.delete(labels, np.where(labels == 'None'))
            le = preprocessing.LabelEncoder()
            le.fit(labels)
            # le.classes_
            ids = le.fit_transform(labels)
            mapping = dict(zip(range(1, 1 + len(le.classes_)), le.classes_))

        if test_name in ('FTT_0000_DS_NOP_000', 'FTT_0000_DB_NOP_000'):
            EOQC_failure_mode = pd.read_csv(
                os.path.join(os.path.dirname(os.path.realpath(__file__)), 'FE_FailureMode.csv'))
            EOQC_dict = EOQC_failure_mode.set_index('Bin').T.to_dict('list')
            labels = data['DataValue'].unique()
            labels = np.delete(labels, np.where(labels == 'None'))
            mapping = {x: EOQC_dict[int(x)] for x in labels}

        grade_ind = grade[idx]

        for line in data.iterrows():
            r_c = line[1].ChipIndex
            col = r_c // 100  # - 50
            row = - r_c % 100  # - 50
            val_raw = line[1].DataValue
            val = 0  # 'A'
            if 'F' in grade_ind.keys():
                if val_raw in grade_ind['F']:
                    val = 1  # 'F'
            if 'C' in grade_ind.keys():
                if val_raw in grade_ind['C']:
                    val = 0.5  # 'C'
                elif idx in [2, 4, 5]:
                    if val > grade_ind['C'][0] and val < grade_ind['C'][1]:
                        val = 0.5
            if 'A' in grade_ind.keys():  # for EOQC_first_failure
                if val_raw not in grade_ind['A']:
                    if idx == 2 or idx == 5:
                        val = le.transform([val_raw]) + 1
                    if idx == 4:
                        val = float(val_raw) * 1e9
                    if idx in [3, 6]:
                        val = int(val_raw)

            data_chip = pd.Series({'Wafer': wafer, 'Row': row, 'Col': col, 'Test': FE_test[idx], 'Value': val})
            data_all = data_all.append(data_chip, ignore_index=True)

        data_all.set_index(['Row', 'Col'], inplace=True)
        fig, ax = plt.subplots(1, 1)
        center = data_all['Value'].mean()
        sigma = data_all['Value'].std()
        # data_all = data_all.loc[~data_all.index.duplicated(keep='last')]
        # data_all['Value'] = data_all.groupby(data_all.index)['Value'].sum()
        data_map = data_all['Value'].unstack(level=1)
        mask = data_map.isnull()
        data_map.fillna(-100, downcast=False, inplace=True)
        data_map = data_map.astype(float)
        if idx == 4:
            data_map = data_map.astype(int)
            sb.heatmap(data_map, vmax=center + 3 * sigma, vmin=center - 3 * sigma, cmap="jet",
                       ax=ax, mask=mask.values, annot=True, fmt="d")
        elif idx in (2, 3, 5, 6):
            # sb.heatmap(data_map, vmax=data_map.values.max() + 1, vmin=-1, cmap="coolwarm", ax=ax, mask=mask,
            #            linewidths=.5, cbar=False, annot=True)
            ax = sb.heatmap(data_map, vmax=16, vmin=-1, cmap="coolwarm", ax=ax, mask=mask,
                       linewidths=.5, cbar=False, annot=True)
            ax.invert_yaxis()
            # text = plt.text(0, 15, str(mapping).replace(',','\n'), path_effects=[path_effects.Normal()])
            text = plt.text(0, 17 + len(labels) * 0.5, str(mapping).replace(',', '\n'),
                            path_effects=[path_effects.Normal()])
        else:
            sb.heatmap(data_map, vmax=data_map.values.max() + 1, vmin=-1, cmap="coolwarm", ax=ax, mask=mask,
                       linewidths=.5, cbar=False, annot=True)
        plt.title(wafer_name + "   " + FE_test[idx] + "   " + FE_test_name[idx])
        #    plt.tight_layout()
        plt.show()
        return fig


def get_FE_parameter(wafer='AY15211-', idx=1):
    #     TEST: GET EOQC0 from SpiderMetrology
    #       Bin 1 and 7 are A
    FE_param = FE_test_name[idx]
    conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')  # camstarsql_mes
    cursor = conn.cursor()
    conn.autocommit = True

    wafer_name = wafer  # 'AW33042-'
    if wafer_name[0] == 'P':
        product = 'Sequel'
    else:
        product = 'Spider'
    get_data_sql = '''
       SET NOCOUNT ON
       DECLARE @LotName AS NVARCHAR(128)
       SET @LotName = '{0}%'
    
       DECLARE @RawTable TABLE (
              WaferName NVARCHAR(255),
              BaeWaferName NVARCHAR(255),
              ChipIndex INT,
              SubstrateId CHAR(8),
              DataName NVARCHAR(255),
              DataValue NVARCHAR(255),
              Unit NVARCHAR(255),
              DataUploadTime DATETIME,
              DataCollectionTime DATETIME,
              [FileName] NVARCHAR(255),
              [Description] NVARCHAR(255)
              )
    
       INSERT INTO @RawTable 
              (WaferName, BaeWaferName, ChipIndex,SubstrateId,DataName,DataValue,Unit,DataUploadTime,
                     DataCollectionTime,
                     [Description])
              SELECT 
                             WaferName,
                             BaeWaferName,
                             ChipIndex,
                             {1}Metrology.dbo.ConvertSubstrateIdToString(SubstrateId),
                             DataName,
                             DataValue,
                             Unit,
                             DataUploadTime,
                             DataCollectionTime,
                             [Description]
                     FROM (SELECT ChipId, DataUploadSessionId, DataNameId, CAST(ChipData.DataValue as NVARCHAR(255)) as DataValue
                           FROM [{1}Metrology].[dbo].[ChipData] ) ChipData
                             JOIN [{1}Metrology].[dbo].[Chip] Chip ON ChipData.ChipId = Chip.ChipId
                             JOIN [{1}Metrology].[dbo].[Wafer] Wafer ON Chip.WaferId = Wafer.WaferId
                             JOIN [{1}Metrology].[dbo].[DataName] DataName ON ChipData.DataNameId = DataName.DataNameId
                             JOIN [{1}Metrology].[dbo].[DataUploadSession] DataUploadSession ON ChipData.DataUploadSessionId = DataUploadSession.DataUploadSessionId
                     WHERE Wafer.WaferName LIKE @LotName
                     --DataUploadTime > '2020-01-01'
                     AND DataName LIKE '%{2}%';
    
    
       SELECT 
              rt.WaferName,
              BaeWaferName,
              rt.ChipIndex,
              SubstrateId,
              rt.DataName,
              DataValue,
              Unit,
              rt.DataUploadTime,
              DataCollectionTime,
              [Description]
       FROM @RawTable rt
       INNER JOIN (
                     select
                             WaferName,
                             ChipIndex,
                             DataName,
                             max(DataUploadTime) as DataUploadTime
                     from @RawTable group by WaferName,ChipIndex,DataName
                ) sd
       ON
              rt.WaferName = sd.WaferName AND
              rt.ChipIndex = sd.ChipIndex AND
              rt.DataName = sd.DataName AND
              rt.DataUploadTime = sd.DataUploadTime
              order by SubstrateId'''.format(wafer, product, FE_param)

    # conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')
    data_raw = pd.read_sql(get_data_sql, conn)
    conn.close()
    # data_raw.to_csv('EOQC0.csv')
    return data_raw


if __name__ == '__main__':
    fig = FE_Defects_Plot(wafer, idx=idx)

#
# def heatmap(data, row_labels, col_labels, ax=None,
#            cbar_kw={}, cbarlabel="", **kwargs):
#    """
#    Create a heatmap from a numpy array and two lists of labels.
#
#    Parameters
#    ----------
#    data
#        A 2D numpy array of shape (N, M).
#    row_labels
#        A list or array of length N with the labels for the rows.
#    col_labels
#        A list or array of length M with the labels for the columns.
#    ax
#        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
#        not provided, use current axes or create a new one.  Optional.
#    cbar_kw
#        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
#    cbarlabel
#        The label for the colorbar.  Optional.
#    **kwargs
#        All other arguments are forwarded to `imshow`.
#    """
#
#    if not ax:
#        ax = plt.gca()
#
#    # Plot the heatmap
#    im = ax.imshow(data, **kwargs)
#
#    # Create colorbar
#    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
#    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")
#
#    # We want to show all ticks...
#    ax.set_xticks(np.arange(data.shape[1]))
#    ax.set_yticks(np.arange(data.shape[0]))
#    # ... and label them with the respective list entries.
#    ax.set_xticklabels(col_labels)
#    ax.set_yticklabels(row_labels)
#
#    # Let the horizontal axes labeling appear on top.
#    ax.tick_params(top=True, bottom=False,
#                   labeltop=True, labelbottom=False)
#
#    # Rotate the tick labels and set their alignment.
#    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
#             rotation_mode="anchor")
#
#    # Turn spines off and create white grid.
#    for edge, spine in ax.spines.items():
#        spine.set_visible(False)
#
#    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
#    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
#    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
#    ax.tick_params(which="minor", bottom=False, left=False)
#
#    return im, cbar
#
#
# def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
#                     textcolors=["black", "white"],
#                     threshold=None, **textkw):
#    """
#    A function to annotate a heatmap.
#
#    Parameters
#    ----------
#    im
#        The AxesImage to be labeled.
#    data
#        Data used to annotate.  If None, the image's data is used.  Optional.
#    valfmt
#        The format of the annotations inside the heatmap.  This should either
#        use the string format method, e.g. "$ {x:.2f}", or be a
#        `matplotlib.ticker.Formatter`.  Optional.
#    textcolors
#        A list or array of two color specifications.  The first is used for
#        values below a threshold, the second for those above.  Optional.
#    threshold
#        Value in data units according to which the colors from textcolors are
#        applied.  If None (the default) uses the middle of the colormap as
#        separation.  Optional.
#    **kwargs
#        All other arguments are forwarded to each call to `text` used to create
#        the text labels.
#    """
#
#    if not isinstance(data, (list, np.ndarray)):
#        data = im.get_array()
#
#    # Normalize the threshold to the images color range.
#    if threshold is not None:
#        threshold = im.norm(threshold)
#    else:
#        threshold = im.norm(data.max())/2.
#
#    # Set default alignment to center, but allow it to be
#    # overwritten by textkw.
#    kw = dict(horizontalalignment="center",
#              verticalalignment="center")
#    kw.update(textkw)
#
#    # Get the formatter in case a string is supplied
#    if isinstance(valfmt, str):
#        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)
#
#    # Loop over the data and create a `Text` for each "pixel".
#    # Change the text's color depending on the data.
#    texts = []
#    for i in range(data.shape[0]):
#        for j in range(data.shape[1]):
#            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
#            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
#            texts.append(text)
#
#    return texts
# fig, ax = plt.subplots(1, 1)
# qrates = le.classes_
# c_n = len(qrates)
# norm = matplotlib.colors.BoundaryNorm(np.linspace(-3.5, 3.5, c_n+1), len(qrates))
# fmt = matplotlib.ticker.FuncFormatter(lambda x, pos: qrates[::-1][norm(x)])
# im, _ = heatmap(data_map.values, data_map.index, data_map.columns, ax=ax,
#                cmap=plt.get_cmap("PiYG", c_n), norm=norm,
#                cbar_kw=dict(ticks=np.arange(-5, 5), format=fmt),
#                cbarlabel="Quality Rating")
#
# annotate_heatmap(im, valfmt=fmt, size=9, fontweight="bold", threshold=-1,
#                 textcolors=["red", "black"])
# plt.tight_layout()
# plt.show()
