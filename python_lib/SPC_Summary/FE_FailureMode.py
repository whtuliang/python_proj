# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 16:19:21 2019

@author: ltu
"""

import pandas as pd
import numpy as np
import pyodbc 
import matplotlib.pyplot as plt
import seaborn as sb

conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')  #camstarsql_mes
cursor = conn.cursor()
conn.autocommit = True



FE_test = ['OQA','EOQC','EOQC','VQC1','VQC2','BCD']
FE_test_name = ['SLT_OQA_DefectId','FTT_0000_DB_NOP_000','FTT_FIRST_FAILURE','VQC1_Result','VQC2_Result', 'PBI_bcd_fit']
grade = [{'C':'D21','F':['D29','D41']}, {'F':'9'}, {'C':[0,22]}, {'C':[0,22]}, {'C':[85,125]}]
    
wafer_name = 'AW33042-13' #'AW33042-'

idx = 2
test_name = FE_test_name[idx]

get_data_sql =  '''
SET NOCOUNT ON
DECLARE @LotName AS NVARCHAR(128)
SET @LotName = '{}%'

DECLARE @RawTable TABLE (
       WaferName NVARCHAR(255),
       BaeWaferName NVARCHAR(255),
       ChipIndex INT,
       SubstrateId CHAR(8),
       DataName NVARCHAR(255),
       DataValue NVARCHAR(255),
       Unit NVARCHAR(255),
       DataUploadTime DATETIME,
       DataCollectionTime DATETIME,
       [FileName] NVARCHAR(255),
       [Description] NVARCHAR(255)
       )



INSERT INTO @RawTable 
       (WaferName, BaeWaferName, ChipIndex,SubstrateId,DataName,DataValue,Unit,DataUploadTime,
              DataCollectionTime,
              [Description])
       SELECT 
                      WaferName,
                      BaeWaferName,
                      ChipIndex,
                      SpiderMetrology.dbo.ConvertSubstrateIdToString(SubstrateId),
                      DataName,
                      DataValue,
                      Unit,
                      DataUploadTime,
                      DataCollectionTime,
                      [Description]
              FROM (SELECT ChipId, DataUploadSessionId, DataNameId, CAST(ChipData.DataValue as NVARCHAR(255)) as DataValue
                    FROM [SpiderMetrology].[dbo].[ChipData] ) ChipData
                      JOIN [SpiderMetrology].[dbo].[Chip] Chip ON ChipData.ChipId = Chip.ChipId
                      JOIN [SpiderMetrology].[dbo].[Wafer] Wafer ON Chip.WaferId = Wafer.WaferId
                      JOIN [SpiderMetrology].[dbo].[DataName] DataName ON ChipData.DataNameId = DataName.DataNameId
                      JOIN [SpiderMetrology].[dbo].[DataUploadSession] DataUploadSession ON ChipData.DataUploadSessionId = DataUploadSession.DataUploadSessionId
              WHERE Wafer.WaferName LIKE @LotName
              AND DataName LIKE '%{}%';


SELECT 
       rt.WaferName,
       BaeWaferName,
       rt.ChipIndex,
       SubstrateId,
       rt.DataName,
       DataValue,
       Unit,
       rt.DataUploadTime,
       DataCollectionTime,
       [Description]
FROM @RawTable rt
INNER JOIN (
              select
                      WaferName,
                      ChipIndex,
                      DataName,
                      max(DataUploadTime) as DataUploadTime
              from @RawTable group by WaferName,ChipIndex,DataName
         ) sd
ON
       rt.WaferName = sd.WaferName AND
       rt.ChipIndex = sd.ChipIndex AND
       rt.DataName = sd.DataName AND
       rt.DataUploadTime = sd.DataUploadTime
       order by SubstrateId'''.format(wafer_name, test_name)
       



#abc = cursor.execute(get_data_sql).fetchall()
data = pd.read_sql(get_data_sql, conn)
data_all= pd.DataFrame(columns=['Wafer','Row','Col','Test', 'Value'])
grade_ind = grade[idx]


for line in data.iterrows():
    r_c = line[1].ChipIndex
    row = r_c // 100 - 50
    col = r_c % 100 - 50
    val_raw = line[1].DataValue
    val = 0 #'A'
    if 'F'in grade_ind.keys():
        if val_raw in grade_ind['F']:
            val = 1 #'F'
    if 'C'in grade_ind.keys():
        if val_raw in grade_ind['C']:
            val = 0.5 #'C'
        elif idx in [2,3,4]:
            if val>grade_ind['C'][0] and val<grade_ind['C'][1]:
                val = 0.5
    
            
    data_chip = pd.Series({ 'Wafer':wafer_name, 'Row':row, 'Col':col, 'Test':FE_test[idx], 'Value':val})
    data_all = data_all.append(data_chip, ignore_index=True)
    

    
data_all.set_index(['Row','Col'],inplace=True)
fig, ax = plt.subplots(1, 1)
center=data_all['Value'].mean()
sigma=data_all['Value'].std()
data_map = data_all['Value'].unstack(level=1)
mask = data_map.isnull()
data_map.fillna(-100, downcast=False, inplace=True)
data_map = data_map.astype(float)
#sb.heatmap(data_map,vmax=center+3*sigma,vmin=center-3*sigma,cmap="jet", ax=ax, mask=mask.values)
sb.heatmap(data_map, vmax=2, vmin=-1,cmap="coolwarm", ax=ax, mask=mask, linewidths=.5, cbar=False)
plt.title(wafer_name + "   " + FE_test[idx])
plt.tight_layout()

#plt.pcolor(data_map)
#from bokeh.charts import HeatMap, bins, output_file, show, vplot
#from bokeh.palettes import RdYlGn6, RdYlGn9
#hm1 = HeatMap(data_map, x=bins('mpg'), y=bins('displ'))