# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 11:12:53 2019

@author: ltu
"""

from pptx import Presentation
from pptx.util import Inches
import csv, os


# ww = '31'  #NEED TO UPDATE
# folder_path = r'\\sharepoint\mfg\sequelcell\Documents\Manufacturing Engineering\Yield Reports\Weekly Yield Meetings\WW9'+ww
# data = csv.reader(open(os.path.join(folder_path, 'Data/ww_run_data_Spider.csv')))


def ppt_gen(path_from, path_to):
    folder_path = path_from
    prs = Presentation()
    title_slide_layout = prs.slide_layouts[0]
    slide = prs.slides.add_slide(title_slide_layout)
    title = slide.shapes.title
    subtitle = slide.placeholders[1]

    title.text = "Yield Report"
    subtitle.text = "template"

    bullet_slide_layout = prs.slide_layouts[1]
    slide = prs.slides.add_slide(bullet_slide_layout)
    shapes = slide.shapes
    title_shape = shapes.title
    body_shape = shapes.placeholders[1]
    title_shape.text = 'Adding a Bullet Slide'
    tf = body_shape.text_frame
    tf.text = 'Find the bullet slide layout'
    p = tf.add_paragraph()
    p.text = 'Use _TextFrame.text for first bullet'
    p.level = 1
    p = tf.add_paragraph()
    p.text = 'Use _TextFrame.add_paragraph() for subsequent bullets'
    p.level = 2

    blank_slide_layout = prs.slide_layouts[2]
    slide = prs.slides.add_slide(blank_slide_layout)
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_CCYA_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(1), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_CCYA_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(3), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_CCYA_1_locf.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(5), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.5), top=Inches(1), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.5), top=Inches(3), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_1_locf.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.5), top=Inches(5), height=Inches(1.5), width=Inches(4.5))

    blank_slide_layout = prs.slide_layouts[3]
    slide = prs.slides.add_slide(blank_slide_layout)
    height_inch, width_inch = 1.2, 3.6
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_FE_A_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(0), height=Inches(height_inch),
                                   width=Inches(width_inch))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_PDP_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch), height=Inches(height_inch),
                                   width=Inches(width_inch))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_BE_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch * 2), height=Inches(height_inch),
                                   width=Inches(width_inch))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_DS_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch * 3), height=Inches(height_inch),
                                   width=Inches(width_inch))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_EOLQC_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch * 4), height=Inches(height_inch),
                                   width=Inches(width_inch))

    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_FE_A_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(0), height=Inches(height_inch),
                                   width=Inches(width_inch))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_BCD_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(height_inch * 1), height=Inches(height_inch),
                                   width=Inches(width_inch))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_BE_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(height_inch * 2), height=Inches(height_inch),
                                   width=Inches(width_inch))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_DS_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(height_inch * 3), height=Inches(height_inch),
                                   width=Inches(width_inch))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_EOLQC_13.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(height_inch * 4), height=Inches(height_inch),
                                   width=Inches(width_inch))

    blank_slide_layout = prs.slide_layouts[4]
    slide = prs.slides.add_slide(blank_slide_layout)
    height_inch, width_inch = 1.2, 3.6
    img_path = os.path.join(folder_path, 'Plots/Sequel_Lots_CCY_A_Finished.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(0), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_Lots_FE_Yield_A.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_Lots_PDP_Yield.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch * 2), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_Lots_BE_Yield.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch * 3), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_Lots_DS_Yield.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch * 4), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_Lots_EOLQC_Yield.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(height_inch * 5), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_Lots_CCY_A_Finished.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(0), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_Lots_FE_Yield_A.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(height_inch * 1), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_Lots_BCD_Yield.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(height_inch * 2), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_Lots_BE_Yield.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(height_inch * 3), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_Lots_DS_Yield.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(height_inch * 4), height=Inches(height_inch),
                                   width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_Lots_EOLQC_Yield.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(height_inch * 5), height=Inches(height_inch),
                                   width=Inches(3.6))

    blank_slide_layout = prs.slide_layouts[5]
    slide = prs.slides.add_slide(blank_slide_layout)
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_FE_Yield_A_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(0), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_PDP_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(1.2), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_BE_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(2.4), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_DS_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(3.6), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_EOLQC_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(4.8), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_VQC1_g.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.2), top=Inches(0), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_VQC2_g.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.2), top=Inches(1.2), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_OQC1_g.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.2), top=Inches(2.4), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_EOQC1_g.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.2), top=Inches(3.6), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_FR_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.2), top=Inches(4.8), height=Inches(1.2), width=Inches(3.6))

    # blank_slide_layout = prs.slide_layouts[5]
    # slide = prs.slides.add_slide(blank_slide_layout)
    # img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_13.png')
    # pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(1), height=Inches(2), width=Inches(6))
    # img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_4.png')
    # pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(3), height=Inches(2), width=Inches(6))
    # img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_1_locf.png')
    # pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(5), height=Inches(2), width=Inches(6))

    blank_slide_layout = prs.slide_layouts[6]
    slide = prs.slides.add_slide(blank_slide_layout)
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_FE_Yield_A_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(0), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_PDP_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(1.2), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_BE_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(2.4), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_DS_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(3.6), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_EOLQC_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(4.8), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_FR_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(6), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_VQC1_g.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(0), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_OQA_g.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(1.2), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_OQC1_g.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(2.4), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_EOQC1_g.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(3.6), height=Inches(1.2), width=Inches(3.6))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_BCD_Yield_y1.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(4.5), top=Inches(4.8), height=Inches(1.2), width=Inches(3.6))

    blank_slide_layout = prs.slide_layouts[7]
    slide = prs.slides.add_slide(blank_slide_layout)
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_FE_A_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(1), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_BE_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(2.5), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_DS_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(4), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_EOLQC_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(5.5), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_FE_A_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(1), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_BE_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(2.5), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_DS_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(4), height=Inches(1.5), width=Inches(4.5))
    img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_EOLQC_4.png')
    pic = slide.shapes.add_picture(img_path, left=Inches(5.2), top=Inches(5.5), height=Inches(1.5), width=Inches(4.5))

    prs.save(os.path.join(path_to, 'test.pptx'))


if __name__ == '__main__':
    current_path = os.path.dirname(os.path.abspath(__file__))
    folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield'
    ppt_gen(path_from=folder_path, path_to=current_path)
