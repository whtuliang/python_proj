1# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 16:52:51 2019

@author: ltu
"""

import pandas as pd
import os

#import importlib
#spec = importlib.util.spec_from_file_location("module.name", r"\\pbi\dept\chip\ChipEng\Liang\Python-Lib\util_liang\spc_util.py".replace('\\','/'))
#spc_util = importlib.util.module_from_spec(spec)
#spec.loader.exec_module(spc_util)
dir_path = os.path.dirname(os.path.realpath(__file__))
package_path = os.path.dirname(dir_path)
sp_path = os.path.join(package_path, 'site_packages')
if package_path not in os.sys.path:
    os.sys.path.append( package_path)
if sp_path not in os.sys.path:
    os.sys.path.append( sp_path)

import util_liang.spc_util as spc_util

prod_list =  ['Sequel', 'Spider']

#prod = prod_list[1]

def get_FE_summary(prod=None):
    if os.name=='nt':
        folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'
    else:
        folder_path = r'//pbi/dept/chip/ChipEng/Liang/Yield/Data'
    data_name = 'all_chip_level_data.csv'
    data_raw = pd.read_csv( os.path.join(folder_path, data_name) )
    
    date_max, data_ww = spc_util.get_FE_maxWW(data_raw)
    #del data_raw 
    
    ww_all = data_raw.FE_QC_Cumulative_WW.unique()
    ww_all = ww_all[~ pd.isnull(ww_all)]
    
    def df_stack(df):
        df_all_sub1 = df.stack()
        df_all_sub1.name = 'Count'
        df_all_sub1.sort_values( ascending=False, inplace=True)
        df_all_sub1 = pd.DataFrame(df_all_sub1)
        df_all_sub1['Count_cumsum'] = df_all_sub1.Count.cumsum()
        df_all_sub1['Count_cumsum_%'] = df_all_sub1['Count_cumsum'] / sum(df_all_sub1['Count']) * 100
        df_all_sub1 = df_all_sub1.reset_index()
        return df_all_sub1
        
        
    df_all = pd.DataFrame()
    df_all_byWaf = pd.DataFrame()
    for ww in ww_all:
        data_ww = data_raw[data_raw.FE_QC_Cumulative_WW == ww] 
        for prod in prod_list:
            data_sub_finished = spc_util.summary_FE_CDEF(data_ww, prod)
            if len(data_sub_finished) >0:
                df_all_sub = spc_util.pivot_FE_failure(data_sub_finished).reset_index()
                if len(df_all_sub) > 0:
                    df_all_sub1 = df_all_sub.groupby('PBI_Lot').apply(df_stack).reset_index()
#                    df_all_sub1 = df_all_sub.groupby('PBI_Lot').stack()
#                    df_all_sub1.name = 'Count'
#                    df_all_sub1.sort_values( ascending=False)
#                    df_all_sub1 = pd.DataFrame(df_all_sub1)
#                    df_all_sub1['Count_cumsum'] = df_all_sub1.Count.cumsum()
#                    df_all_sub1['Count_cumsum_%'] = df_all_sub1['Count_cumsum'] / sum(df_all_sub1['Count']) * 100
#                    df_all_sub1 = df_all_sub1.reset_index()
                    df_all_sub1['prod'] = prod
                    df_all_sub1['FE_QC_Cumulative_WW'] = ww
                    df_all = df_all.append(df_all_sub1)
#                    df_all[len(df_all)] = df_all_sub
            
            
                PBI_Lot_list = data_sub_finished['PBI_Lot'].unique()
                for PBI_lot in PBI_Lot_list:
                    df_lot_byWaf = spc_util.pivot_FE_failure(data_sub_finished, PBI_lot).reset_index() 
                    if len(df_lot_byWaf) > 0:
                        df_lot_sub = df_lot_byWaf.groupby('Wafer').apply(df_stack).reset_index()
                        df_lot_sub['PBI_lot'] = PBI_lot
                        df_lot_sub['prod'] = prod
                        df_lot_sub['FE_QC_Cumulative_WW'] = ww
                        df_all_byWaf = df_all_byWaf.append(df_lot_sub.reset_index())
     
        
    ww_all = data_raw.FEWorkWeekOut.unique()
    ww_all = ww_all[~ pd.isnull(ww_all)]
    df_all_wwout = pd.DataFrame()
    for ww in ww_all:
        data_ww = data_raw[data_raw.FEWorkWeekOut == ww] 
        for prod in prod_list:
            data_sub_finished = spc_util.summary_FE_CDEF(data_ww, prod)
            if len(data_sub_finished) >0:
                df_all_sub = spc_util.pivot_FE_failure(data_sub_finished).reset_index()
                if len(df_all_sub) > 0:
                    df_all_sub1 = df_all_sub.groupby('PBI_Lot').apply(df_stack).reset_index()
                    df_all_sub1['prod'] = prod
                    df_all_sub1['FEWorkWeekOut'] = ww
                    df_all_wwout = df_all_wwout.append(df_all_sub1)
                    
            
#            df_lot_list = [spc_util.pivot_FE_failure(data_sub_finished, lot) for lot in PBI_Lot_list]
    
    df_all.to_csv(os.path.join(folder_path, 'FE_ww_summary.csv'), index=False)
    df_all_byWaf.to_csv(os.path.join(folder_path, 'FE_ww_summary_byWaf.csv'), index=False)
    df_all_wwout.to_csv(os.path.join(folder_path, 'FE_wwout_summary.csv'), index=False)
    return df_all



#data_ww = data_raw[data_raw.FE_QC_Cumulative_WW == '2019-40'] 
#data_sub_finished = spc_util.summary_FE_failure(data_ww, 'Spider')

if __name__ == '__main__':
    get_FE_summary()