# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 11:05:31 2019

@author: ltu
"""

#import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
#import statsmodels.api as sm # import statsmodels 
import os
from utiliang import spc_util
#from os import listdir
#from os.path import isfile, isdir, join
#from datetime import datetime, timedelta
#from dateutil.relativedelta import relativedelta
#from openpyxl import load_workbook

#folder_path = os.path.dirname(os.path.abspath('__file__'))
#ww = '31'  #NEED TO UPDATE
#folder_path = r'\\sharepoint\mfg\sequelcell\Documents\Manufacturing Engineering\Yield Reports\Weekly Yield Meetings\WW9'+ww+r'\Data'
folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'

data_name = 'all_chip_level_data.csv'
data_raw = pd.read_csv( os.path.join(folder_path, data_name) )


date_max, data_ww = spc_util.get_FE_maxWW(data_raw)
#data_ww = data_ww[~np.isnan(data_ww.CCY_AC_Finished)]   #remove 
del data_raw 

prod = 'Spider'
data_sub_finished = spc_util.summary_FE_failure(data_ww, prod)

failure_pivot_valid = spc_util.pivot_FE_failure(data_sub_finished)
fig, ax = plt.subplots(figsize=(8, 5))
failure_pivot_valid.plot.bar(stacked=True, rot=0, ax=ax)
ax.set_title(prod + ' @ ' + date_max)
ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
ax.set_ylabel('Chip_Count')
ax.legend(loc='upper left', bbox_to_anchor=(1, 0.9))
plt.tight_layout()


#abc = data_sub.groupby(['PBI_Lot','Failure_Cause']).agg({'Failure_Cause': ['count']})
#failure_sum = data_sub_finished.groupby(['PBI_Lot','Failure_Cause'])['Chip'].count().reset_index().rename(columns={'Chip':'Failure_Count'})
#failure_pivot = failure_sum.pivot(index='PBI_Lot', columns='Failure_Cause', values='Failure_Count')
if prod == 'Spider':
    plot_spec = 77
else:
    plot_spec = 83

PBI_Lot_list = data_sub_finished['PBI_Lot'].unique()
for lot in PBI_Lot_list:
    failure_pivot = spc_util.pivot_FE_failure(data_sub_finished, lot)
    
    fig, ax = plt.subplots(figsize=(8, 5))
    failure_pivot.plot.bar(stacked=True, ax=ax)
    ax.set_ylim(0, max(93, failure_pivot.values.max()))
    ax.set_title(prod + '  ' + lot +   ' @ ' + date_max)
    ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
    ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
    ax.set_ylabel('Chip_Count')
    ax.legend(loc='upper left', bbox_to_anchor=(1, 0.9))
    ax.axhline(y=plot_spec,color='k')
    plt.tight_layout()