# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 11:33:07 2019

@author: ltu
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import statsmodels.api as sm
import os
import shutil
from os import listdir
from os.path import isfile, isdir, join
import datetime
from dateutil.relativedelta import relativedelta
from openpyxl import load_workbook


def run_file():
    year = datetime.date.today().year
    ww = datetime.date.today().isocalendar()[1] - 1
    # year = 2020
    # ww = 53
    year_ww = '{:0>2d}'.format(ww)  # str(year)[-1] + str(ww)

    sharepoint_dir = r"\\sharepoint\mfg\sequelcell\Documents\Manufacturing Engineering\Yield Reports\Weekly Yield Meetings"
    yield_dir = r'\\pbi\dept\chip\ChipEng\Liang\Yield'
    if os.name != 'nt':
        sharepoint_dir = sharepoint_dir.replace('\\', '/')
        yield_dir = yield_dir.replace('\\', '/')

    path = os.path.join(sharepoint_dir, str(year), 'WW{:0>2d}'.format(ww))

    try:
        os.mkdir(path)
    except OSError:
        print("Creation of the directory %s failed" % path)

    if not os.path.isdir(os.path.join(path, 'Data')):
        shutil.copytree(os.path.join(yield_dir, 'Data'), os.path.join(path, 'Data'))

    path_ppt = os.path.join(path,  'Chip_MFG_Yield_WW' + year_ww + '.pptx')
    if not os.path.isdir(path_ppt):
        shutil.copy(os.path.join(yield_dir, 'Chip_MFG_Yield_template.pptx'), path_ppt)

    import SPC_Summary.ppt_gen as ppt_gen
    ppt_gen.ppt_gen(path_from=yield_dir, path_to=os.path.join(path, 'Data'))

    import SPC_Summary.generate_10ww_summary as generate_10ww_summary
    generate_10ww_summary.generate_summary_1(path_from=os.path.join(yield_dir, 'Data'), path_to=os.path.join(path, 'Data'))

    import util_liang.send_email_weekly
    util_liang.send_email_weekly.main(year_ww)


# utiliang.send_email_weekly.send_group_emails(year_ww)


email_list = '''
Jelena Pesic <jpesic@pacificbiosciences.com>; Mark Phillips <maphillips@pacificbiosciences.com>; Allan Bonilla <abonilla@pacificbiosciences.com>; Ann Gabrys <agabrys@pacificbiosciences.com>; Brian Porter <bporter@pacificbiosciences.com>; 
Hai Le <hle@pacificbiosciences.com>; Jinsong Gao <jgao@pacificbiosciences.com>; Kun Hou <khou@pacificbiosciences.com>; Loreto Cantillep <lcantillep@pacificbiosciences.com>; Minh Ngoc Tran <mntran@pacificbiosciences.com>; Nicolas Dyer <ndyer@pacificbiosciences.com>; 
Remy Santiago <rsantiago@pacificbiosciences.com>; Andrew Jae <ajae@pacificbiosciences.com>; Robert Beckman <rbeckman@pacificbiosciences.com>; Naining Yin <nyin@pacificbiosciences.com>; Shabbir Shahdawala <sshahdawala@pacificbiosciences.com>; Denis Zaccarin <DZaccarin@pacificbiosciences.com>; 
Mathieu Foquet <mfoquet@pacificbiosciences.com>; Ravi Saxena <rsaxena@pacificbiosciences.com>; Ezgi Evcik <eevcik@pacificbiosciences.com>; Mike Goloubef <mgoloubef@pacificbiosciences.com>
'''

# group_list = utiliang.send_email_weekly.get_group_emails()
# utiliang.send_email_weekly.main(year_ww, group_list)


if __name__ == '__main__':
    dir_path = os.path.dirname(os.path.realpath(__file__))
    package_path = os.path.dirname(dir_path)
    sp_path = os.path.join(package_path, 'site_packages')
    if package_path not in os.sys.path:
        os.sys.path.append(package_path)
    if sp_path not in os.sys.path:
        os.sys.path.append(sp_path)

    run_file()

