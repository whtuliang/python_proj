# -*- coding: utf-8 -*-
"""
Created on Thu Jul 25 11:16:10 2019

@author: ltu
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import os
import pyodbc 





wafer_name = 'AX51177-12' #'AW47070-25' #'AW33042-'
wafer_name_list = ['AX52215-03']
#product = 'Spider' 
def ods_laser(x):
    if x['ODS_LaserScatterMean600nm']<400 or x['ODS_LaserScatterMean600nm']>2050 or x['ODS_LaserScatterPercent600nm']<100 or x['ODS_LaserScatterMean600nm']>2050 or x['ODS_LaserScatterPercent640nm']<100 or x['ODS_LaserScatterPercent640nm']>2050:
        return 'Fail'
    else:
        return 'Pass'

#get_data_sql =  '''
#Select PD.ContainerName, PD.DataName, PD.DataValue,PD.TxnDate
#From PBI_ParametricData As PD
#Inner Join (Select ContainerName, Max(TxnDate) As MaxDate 
#      From PBI_ParametricData
#	  Where ContainerName like '{0}%'
#	  Group by ContainerName) As PD2 
#	  ON (PD.ContainerName=PD2.ContainerName AND PD.TxnDate=PD2.MaxDate)
#Where PD.ContainerName like '{0}%' and PD.DataCollectionDefName='Dry Sort' and (PD.DataName='ODS_CalibrateClockPasses' or 
#		PD.DataName='ODS_PowerOnCheckPasses' or PD.DataName='ODS_RegisterReadBackPasses' or PD.DataName='DrySortResult')
#order by PD.ContainerName, PD.TxnDate, PD.DataName'''.format( wafer_name)
#data_raw = pd.read_sql(get_data_sql, conn1)


# Get the most up-to-date ODS from MES
data = pd.DataFrame()
from util_liang.spc_util import get_prodtype
for wafer_name in wafer_name_list:
    if get_prodtype(wafer_name) == 'Spider':
        data_name = 'Dry Sort Spider'
    else:
        data_name = 'Dry Sort'
    get_data_sql =  ''' with tb as (Select ContainerName,  DataName, DataValue,TxnDate
    From PBI_ParametricData as PD
    Where PD.ContainerName like '{0}%' and PD.DataCollectionDefName='{1}'  )
    --Where PD.ContainerName in ('{0}') and PD.DataCollectionDefName='{1}'  )
    
    Select tb1.ContainerName, tb1.DataName, tb1.DataValue, tb1.TxnDate
    From tb As tb1
        Inner Join (Select ContainerName, Max(TxnDate) As MaxDate 
                    From tb 
                    Group by ContainerName) As tb2 
        ON (tb1.ContainerName=tb2.ContainerName AND tb1.TxnDate=tb2.MaxDate)
    '''.format(wafer_name, data_name) #'\',\''.join(wafer_name_list)
    conn1 = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly;Database=CamODS_6X')  #camstarsql_mes
    conn1.autocommit = True
    data_raw1 = pd.read_sql(get_data_sql, conn1)
    data1_pivot = data_raw1.pivot_table(index=['ContainerName', 'TxnDate'], columns='DataName', values='DataValue', aggfunc=np.sum)
    valid_columns = ['DrySortResult', 'ODS_LaserScatterMean600nm','ODS_LaserScatterPercent600nm','ODS_LaserScatterPercent640nm', 'ODS_CalibrateClockPasses', 'ODS_PowerOnCheckPasses', 'ODS_RegisterReadbackPasses',  'ODS_NonFunctionalWaveguides', 'ODS_max_loss', 'ODS_std_loss', 'ODS_FinalChipGrade']
    data1_pivot_valid = data1_pivot[valid_columns].reset_index().groupby('ContainerName').apply(lambda x: x.sort_values(['TxnDate']))
    data1_pivot_valid = data1_pivot_valid.astype({'ODS_LaserScatterMean600nm': 'float', 'ODS_LaserScatterPercent600nm': 'float', 'ODS_LaserScatterPercent640nm': 'float'})
    data1_pivot_valid['ODS_LaserFailure'] = data1_pivot_valid.apply(ods_laser, axis=1)
    data1_pivot_valid['x'] = data1_pivot_valid['ContainerName'].apply(lambda x: x[11:13])
    data1_pivot_valid['y'] = data1_pivot_valid['ContainerName'].apply(lambda x: x[13:15])
    data1_pivot_valid['wafer'] = data1_pivot_valid['ContainerName'].apply(lambda x: x[:10])
    data = data.append(data1_pivot_valid)
data.to_csv('ods_data.csv')


# Get the most all ODS from MES
get_data_sql =  ''' Select ContainerName,  DataName, DataValue,TxnDate
From PBI_ParametricData as PD
Where PD.ContainerName like '{0}%' and PD.DataCollectionDefName='Dry Sort' 
'''.format( wafer_name)


conn1 = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly;Database=CamODS_6X')  #camstarsql_mes
conn1.autocommit = True
data_raw1 = pd.read_sql(get_data_sql, conn1)
data1_pivot = data_raw1.pivot_table(index=['ContainerName', 'TxnDate'], columns='DataName', values='DataValue', aggfunc=np.sum)
#valid_columns = ['DrySortResult','ODS_FinalChipGrade', 'ODS_Electrical_Result', 'ODS_CalibrateClockPasses', 'ODS_PowerOnCheckPasses', 'ODS_RegisterReadbackPasses', 'ODS_AlignmentPasses', 'ODS_NonFunctionalWaveguides']
valid_columns = ['DrySortResult', 'ODS_LaserScatterMean600nm','ODS_LaserScatterPercent600nm','ODS_LaserScatterPercent640nm', 'ODS_CalibrateClockPasses', 'ODS_PowerOnCheckPasses', 'ODS_RegisterReadbackPasses',  'ODS_NonFunctionalWaveguides']
data1_pivot_valid = data1_pivot[valid_columns].reset_index().groupby('ContainerName').apply(lambda x: x.sort_values(['TxnDate']))
data1_pivot_valid = data1_pivot_valid.astype({'ODS_LaserScatterMean600nm': 'float', 'ODS_LaserScatterPercent600nm': 'float', 'ODS_LaserScatterPercent640nm': 'float'})
data1_pivot_valid['ODS_LaserFailure'] = data1_pivot_valid.apply(ods_laser, axis=1)

data1_pivot_valid.to_csv('temp.csv')


# Get the most all ODS from Metrology
data = pd.DataFrame()
for wafer_name in wafer_name_list:
    get_data_sql =  ''' 
    Select CAST(ChipData.DataValue as NVARCHAR(255)) as DataValue, DataName.DataName, Wafer.WaferName, Chip.ChipIndex, DataUploadSession.DataCollectionTime, DataUploadSession.DataUploadTime, DataUploadSession.DataCollectionHardwareID, Hardware.HardwareName
    From Chip
    JOIN ChipData On ChipData.ChipId=Chip.ChipId
    JOIN DataName On DataName.DataNameId=ChipData.DataNameId
    JOIN Wafer On Wafer.WaferId=Chip.WaferId
    JOIN DataUploadSession On DataUploadSession.DataUploadSessionId=ChipData.DataUploadSessionId
    Join Hardware On Hardware.HardwareId=DataUploadSession.DataCollectionHardwareID
    Where (DataName.DataName like '%ODS%') and (Wafer.WaferName like '{0}%') and DataUploadSession.DataCollectionTime >'2020-01-01 00:00' 
    --      ((DataName.DataName like 'ODS_PowerOn%') or (DataName.DataName like 'ODS_Calibrate%') or (DataName.DataName like 'ODS_Register%') 
    --	    or (DataName.DataName like 'ODS_Electrical%') or (DataName.DataName like 'ODS_Final%') or (DataName.DataName like 'ODS_Align%'))
    '''.format( wafer_name)
    conn2 = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly; Database=SequelMetrology ')  #camstarsql_mes
    conn2.autocommit = True
    data_raw2 = pd.read_sql(get_data_sql, conn2)
    #data_raw2['DataUploadTime'].hist(bins=10) 
    #test = data_raw2[data_raw2['ChipIndex']==5148].sort_values('DataCollectionTime')
    data_raw2['ContainerName'] = data_raw2.apply(lambda x: x['WaferName'] + '-' + str(x['ChipIndex']), axis=1)
    data2_pivot = data_raw2.pivot_table(index=['WaferName','ContainerName', 'DataCollectionTime','DataUploadTime'], columns='DataName', values='DataValue', aggfunc=np.sum)
    valid_columns = ['ODS_CDS_Result','ODS_FinalChipGrade', 'ODS_Electrical_Result', 'ODS_CalibrateClockPasses', 'ODS_PowerOnCheckPasses', 'ODS_RegisterReadbackPasses', 'ODS_AlignmentPasses', 'ODS_NonFunctionalWaveguides']
    data2_pivot_valid = data2_pivot[valid_columns].reset_index().groupby('ContainerName').apply(lambda x: x.sort_values(['DataUploadTime']))
    data = data.append(data2_pivot_valid)
data.to_csv('ods_data_metrology.csv')

