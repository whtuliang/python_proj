import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import os
import pyodbc

conn1 = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly;Database=CamODS_6X')  # camstarsql_mes
conn1.autocommit = True

get_data_sql = ''' SELECT distinct MovedContainer, Chip, Comment
  FROM [CamODS_6X].[insitedb_schema].[View_MoveHistoryByOpAndDateRange]
  WHERE FromOperation = 'FEOSM Scribe' and MoveDate>'2020-07-01' and MoveDate<'2020-10-01'
'''
data_FEOSM_Scribe = pd.read_sql(get_data_sql, conn1)
data_FEOSM_Scribe['PBI_Lot'] = data_FEOSM_Scribe['MovedContainer'].apply(lambda x: x[:7])
data_FEOSM_Scribe_Summary = data_FEOSM_Scribe.groupby('PBI_Lot').agg(['count'])
data_FEOSM_Scribe_Summary.to_csv(r'\\pbi\dept\chip\ChipEng\Liang\Yield\Inventory_pipeline\Data\Wafer_Count\FEOSM_Scribe.csv')

get_data_sql = ''' SELECT distinct MovedContainer --, Chip
  FROM [CamODS_6X].[insitedb_schema].[View_MoveHistoryByOpAndDateRange]
  WHERE FromOperation = 'SA Lot Formation' and MovedContainer not like 'SA-%'
  and MoveDate>'2020-07-01' and MoveDate<'2020-10-01' 
'''
data_SA_Lot = pd.read_sql(get_data_sql, conn1)
data_SA_Lot['PBI_Lot'] = data_SA_Lot['MovedContainer'].apply(lambda x: x[:7] if len(x) == 10 else x)
data_SA_PBILot_Summary = data_SA_Lot.groupby(['PBI_Lot']).agg(['count']).reset_index()
data_SA_PBILot_Summary.to_csv(r'\\pbi\dept\chip\ChipEng\Liang\Yield\Inventory_pipeline\Data\Wafer_Count\SA_PBILot_Summary.csv')

get_data_sql = ''' SELECT distinct MovedContainer, Chip
  FROM [CamODS_6X].[insitedb_schema].[View_MoveHistoryByOpAndDateRange]
  WHERE FromOperation = 'SA Lot Formation' and MovedContainer like 'SA-%'
  and MoveDate>'2020-07-01' and MoveDate<'2020-10-01' 
'''
data_SA_Lot = pd.read_sql(get_data_sql, conn1)
data_SA_Lot['Wafer'] = data_SA_Lot['Chip'].apply(lambda x: x[:10])
data_SA_Lot_Summary = data_SA_Lot.groupby(['MovedContainer','Wafer']).agg(['count']).reset_index()
data_SA_Lot_Summary = data_SA_Lot_Summary.groupby('MovedContainer').agg(['count']).reset_index()
data_SA_Lot_Summary.to_csv(r'\\pbi\dept\chip\ChipEng\Liang\Yield\Inventory_pipeline\Data\Wafer_Count\SA_Lot_Summary.csv')


data = data_raw.groupby('Chip').apply(lambda x: x.sort_values(by='MoveDate'))

conn1.close()
