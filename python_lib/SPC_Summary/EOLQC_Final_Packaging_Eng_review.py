# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 18:04:43 2020

@author: ltu
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import os
import pyodbc
from datetime import datetime, timedelta


def EOLQC_FP_Eng_Review():
    today = datetime.today().date()
    end_days = str(today - timedelta(today.weekday() + 1))
    start_days = str(today - timedelta(today.weekday() + 8))

    # chip = ('AX30071-10-4754')  # 'AW33042-'

    get_data_sql = ''' select distinct Chip, Substrate, FromOperation, MoveDate
          from [insitedb_schema].[View_MoveHistoryByOpAndDateRange]
          where FromOperation in ('Final Packaging Eng Review') --Functional Test Gate
          and (Comments IS NULL OR Comments NOT LIKE '%MES 9.8 Release%')
          and MoveDate>='{0}' 
          and MoveDate<'{1}'
          order by MoveDate
    '''.format(start_days, end_days)

    conn1 = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly;Database=CamODS_6X')  # camstarsql_mes
    conn1.autocommit = True
    data_raw = pd.read_sql(get_data_sql, conn1)
    # data = data_raw.groupby('Chip').apply(lambda x:x.sort_values(by='MoveDate'))
    data = data_raw

    chip_list = list(data['Chip'].unique())

    # get_data_sql = ''' select *  from [insitedb_schema].[PBI_GetPDProductiveData]
    #     where TxnDate >= '2020-01-01 22:28:27.000'
    #     and ContainerName in ('{}')
    # '''.format('\',\''.join(chip_list))

    get_data_sql = ''' SELECT [ContainerName],[GradeName], [NewGrade] grade,[TxnDate]
  FROM [CamODS_6X].[insitedb_schema].[PBI_GradeHistory]
  WHERE GradeName in ('WMPdProductive', 'PKMID') and NewGrade is not null and ContainerName in ('{}')
        '''.format('\',\''.join(chip_list))
    data_raw = pd.read_sql(get_data_sql, conn1)

    test = data_raw.groupby(['ContainerName', 'GradeName']).agg({'TxnDate': [max, min]}).reset_index()
    test.columns = ['Chip', 'GradeName', 'TxnDate_end', 'TxnDate_start']
    test = test.merge(data_raw[['ContainerName', 'TxnDate', 'GradeName', 'grade']],
                      left_on=['TxnDate_start', 'Chip', 'GradeName'], right_on=['TxnDate', 'ContainerName', 'GradeName'], how='left')
    test = test.drop(columns=['TxnDate', 'ContainerName'])
    test = test.rename(columns={'grade': 'grade_start'})
    test = test.merge(data_raw[['ContainerName', 'TxnDate', 'GradeName', 'grade']],
                      left_on=['TxnDate_end', 'Chip', 'GradeName'], right_on=['TxnDate', 'ContainerName', 'GradeName'], how='left')
    test = test.drop(columns=['TxnDate', 'ContainerName'])
    test = test.rename(columns={'grade': 'grade_end'})
    test = test.merge(data[['Chip', 'MoveDate']], left_on='Chip', right_on='Chip', how='left')
    test = test.rename(columns={"MoveDate": "Final Packaging Eng Review"})

    test['wafer'] = test['Chip'].apply(lambda x: x[:10])
    test = test.drop(columns=['Chip'])
    test_final = test.drop_duplicates()
    test_final = test_final[['GradeName', 'wafer', 'TxnDate_start', 'grade_start',  'TxnDate_end', 'grade_end',
                             'Final Packaging Eng Review']]

    test_final['grade_changed'] = test_final.apply(lambda x: True if x['grade_start']!=x['grade_end'] else False, axis=1)
    test_result = test_final.sort_values(by=['grade_changed', 'GradeName', 'wafer'], ascending=False)
    return test_result


if __name__ == '__main__':
    result = EOLQC_FP_Eng_Review()
