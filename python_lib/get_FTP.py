# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 13:57:13 2019

@author: ltu
"""
import os
from ftplib import FTP
from openpyxl import load_workbook
import pandas as pd

ftp = FTP('ftp.pacificbiosciences.com', timeout=3)
ftp.login(user='hana', passwd='Liun3lai')  
ftp.cwd("/Spider SPC and QA/Weekly in-Line Measurement") 
file_list = ftp.nlst() 
folder_path = r'\\pbi\dept\chip\ChipEng\Liang\FTP\Spider_SPC_QA1'
bad_file_list= ['2019-05-27 Die tilt with respect to package cavity base.xls', '2019-05-27 Top plate placement accuracy Y.xls']
for file in file_list:
    if file not in bad_file_list:
        gFile = open(os.path.join(folder_path, file), "wb")
        try:
            ftp.retrbinary('RETR '+file, gFile.write, 1024)
        except:
            print(file)
        gFile.close() 
ftp.quit()


onlyfiles = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
for file in onlyfiles:
    category = file[11:]
    data = pd.read_excel(os.path.join(folder_path, file), index_col=0) 
#    book = load_workbook(os.path.join(folder_path, file))
#    writer = pd.ExcelWriter('Summary_10days.xlsx', engine='openpyxl') 
#    writer.book = book
#    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)