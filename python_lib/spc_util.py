# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 13:13:10 2019

@author: ltu
"""

#from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import os
import datetime
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

def get_FE_maxWW(data_raw):
    date_str_list = data_raw['FE_QC_Cumulative_WW'].unique()
#    date_str_list = data_raw['FEWorkWeekOut'].unique()
    date_str_list = date_str_list [~ pd.isnull(date_str_list)]
    date_max = date_str_list.max()
    data_ww = data_raw[data_raw.FE_QC_Cumulative_WW == date_max] 
    return date_max, data_ww

def summary_FE_failure(data_ww, prod):
    col_names = ['Product','PBI_Lot','Wafer','Chip','FE_QC_Cumulative_WW','FE_QC_Cumulative_Grade',
              'VQC1','VQC2','EOQC1','OQC1','OQA','BCDINLINE','CCY_Status','CCY_WorkWeek', 'CCY_AC_Finished']
    data_ww = data_ww[col_names]
    #prod = 'Spider'
    data_sub = data_ww[data_ww.Product == prod]
    if prod == 'Sequel':
        grade_names = ['EOQC1','OQC1', 'VQC1']
    else:
        grade_names = ['EOQC1','OQA','BCDINLINE',  'OQC1', 'VQC1', 'VQC2'] # 'VQC1' only 5 waf per lot, 'VQC2' is included in PDP
    def failure_mode(x):
        index =  [True if x[g]=='F' else False  for g in grade_names ]
        if np.any(index):
            return str(np.array(grade_names)[index])
        else:
            if x['Product']=='Spider':
                index =  [True if pd.isnull(x[g]) else False  for g in ['EOQC1','OQC1', 'OQA'] ]  #grade_names
            else:
                index =  [True if pd.isnull(x[g]) else False  for g in ['EOQC1','OQC1'] ]
            if np.any(index):
                return 'Not Finished'
            else:
                return 'Pass'
    if len(data_sub)==0:
        data_sub['Failure_Cause'] = pd.np.nan
        data_sub_finished = data_sub
    else:
        data_sub['Failure_Cause'] = data_sub.apply(failure_mode, axis=1)
#        data_sub_finished = data_sub
        data_sub_finished = data_sub[data_sub['Failure_Cause']!='Not Finished']

    return data_sub_finished

def pivot_FE_failure(*args):
    argCount = len(args)
    if argCount == 1:
        data_sub_finished = args[0]
        failure_sum = data_sub_finished.groupby(['PBI_Lot','Wafer','Failure_Cause'])['Chip'].count().reset_index().rename(columns={'Chip':'Failure_Count'})
        failure_pivot = failure_sum.pivot(index='Wafer', columns='Failure_Cause', values='Failure_Count')
        wafer_list_valid = failure_pivot[failure_pivot.sum(axis=1, skipna=True)>80].index.values
        data_sub_finished_valid = data_sub_finished[data_sub_finished['Wafer'].isin (wafer_list_valid)]
        failure_sum_valid = data_sub_finished_valid.groupby(['PBI_Lot','Failure_Cause'])['Chip'].count().reset_index().rename(columns={'Chip':'Failure_Count'})
        failure_pivot_valid = failure_sum_valid.pivot(index='PBI_Lot', columns='Failure_Cause', values='Failure_Count')
        return failure_pivot_valid
    elif argCount == 2:
        data_sub_finished = args[0]
        lot = args[1]
        data_lot = data_sub_finished[data_sub_finished['PBI_Lot']==lot]
        failure_sum = data_lot.groupby(['Wafer','Failure_Cause'])['Chip'].count().reset_index().rename(columns={'Chip':'Failure_Count'})
        failure_pivot = failure_sum.pivot(index='Wafer', columns='Failure_Cause', values='Failure_Count')
        return failure_pivot
    else:
        raise Exception('Too many args: {}'.format(args))


def generate_lot_summary_chart_alt():
    folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'

    data_name = 'all_chip_level_data.csv'
    data_raw = pd.read_csv( os.path.join(folder_path, data_name) )
    
    year = datetime.date.today().year
    ww = datetime.date.today().isocalendar()[1] - 1
    year_ww = str(year) + '-' + str(ww)
    
    data_raw['CCY_Status'].fillna(value = '0_NA', inplace = True )
    data_raw['CCY_Status_wafer'] = data_raw.groupby('Wafer')['CCY_Status'].transform(max)
    
    param_name_list = ['FE_Yield_A','FE_Yield_AC','PDP_Yield','BE_Yield','DS_Yield','EOLQC_Yield','FR_Yield', "CCY_A_Finished", "CCY_AC_Finished"]
    param_ww_list = ['FE_QC_Cumulative_WW','FE_QC_Cumulative_WW','PDPWorkWeekOut','BEWorkWeekOut','DSWorkWeekOut','EOLQCWorkWeekOut','FRWorkWeekIn', 'CCY_WorkWeek', 'CCY_WorkWeek']
    param_list = zip(param_name_list, param_ww_list)
    
    data_sum = pd.DataFrame()
    for prod in ['Sequel','Spider']:
        data = data_raw[data_raw['Product']==prod]
        for param in param_list:
           # data[param[1]].fillna(value = '0_NA', inplace = True )
            data_sub = data[~np.isnan(data[param[0]])]
            data_sub = data_sub[~pd.isna(data_sub[param[1]])]
            data_sub = data_sub.groupby('Wafer')
            data_sum[param[1]+'_wafer'] = data_sub[param[1]].max()
            data_sum[param[0]+'_count'] = data_sub[param[0]].count()
            data_sum[param[0]+'_sum'] = data_sub[param[0]].sum() 
            data_sum[param[0]+'_yield'] =  data_sum[param[0]+'_sum'] / data_sum[param[0]+'_count'] * 100
            data_sum['CCY_Status_wafer'] = data_sub['CCY_Status_wafer'].max()
            data_sum['PBI_Lot'] = data_sub['PBI_Lot'].max()
            
            fig = make_subplots(rows=1, cols=1)
            
            fig.add_trace(go.Scatter(x=data_sum['PBI_Lot'], y=data_sum[param[0]+'_yield'], mode="markers",  hovertext=data_sum.index + '<br>' + data_sum[param[1]+'_wafer'], hoverinfo="text",
                                          marker=dict(size=6,line=dict(width=2,color='DarkSlateGrey'),  opacity=0.7, cauto=True) ), row=1, col=1)  
            
            fig.update_layout(
                title=go.layout.Title(
                    text=prod + ' - ' + param[0],  xref="paper", x=0
                ),
                xaxis=go.layout.XAxis(
                    title=go.layout.xaxis.Title(
                        font=dict(family="Courier New, monospace", size=18,color="#7f7f7f")
                    )
                ),
                yaxis=go.layout.YAxis(
                    title=go.layout.yaxis.Title(
                        text=param[0],
                        font=dict(family="Courier New, monospace", size=18,color="#7f7f7f")
                    )
                )
            )
                        
            fig.show()
    