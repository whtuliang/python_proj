6  # -*- coding: utf-8 -*-
"""
Created on Sun Jun 30 11:43:00 2019

@author: ltu
"""

# import numpy as np
import pandas as pd
# from matplotlib import pyplot as plt
# import statsmodels.api as sm # import statsmodels
import os
# from os import listdir
# from os.path import isfile, isdir, join
# import urllib, json
# import re
# import zipfile
import datetime
# import pyodbc
# from pandas.io.json import json_normalize
# import tempfile
from datetime import timedelta

# from dateutil.relativedelta import relativedelta
# from openpyxl import load_workbook
# import field_util

fetch_from_API = True

# conn2 = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')  #camstarsql_mes spidermetrology
# cursor2 = conn2.cursor()
# conn2.autocommit = True


# folder_path = os.path.dirname(os.path.abspath(__file__))
dir_path = os.path.dirname(os.path.realpath(__file__))
package_path = os.path.dirname(dir_path)
sp_path = os.path.join(package_path, 'site_packages')
if package_path not in os.sys.path:
    os.sys.path.append(package_path)
if sp_path not in os.sys.path:
    os.sys.path.append(sp_path)

today = datetime.date.today()
date_start = datetime.date(today.year - 1, today.month, 1)
date_start_1mo = today - timedelta(days=60)  # datetime.date(today.year,today.month-2,1)
if os.name == 'nt':
    save_folder = r'\\pbi\dept\chip\ChipEng\Liang\Field Data'
else:
    save_folder = r'//pbi/dept/chip/ChipEng/Liang/Field Data'

# import importlib
# spec = importlib.util.spec_from_file_location("module.name", r"\\pbi\dept\chip\ChipEng\Liang\Python-Lib\field_util.py".replace('\\','/'))
# field_util = importlib.util.module_from_spec(spec)
# spec.loader.exec_module(field_util)

# from pathlib import Path
# os.chdir(Path(folder_path).parent )  #r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib'
# if '.' not in os.sys.path:
#    os.sys.path.append('.')
from util_liang import field_util
from util_liang.field_util import FieldPlot

if fetch_from_API:
    data_instr = field_util.fetch_field_instr(staging='False')

for prod_type in ['Sequel', 'Spider']:  # 'Spider' 'Sequel'

    if fetch_from_API:
        data = field_util.fetch_field_data(prod_type, days=500)  # field_util.fetch_staging_data

        # merge field data with instrument
        data = data.merge(data_instr.loc[:,
                          ['x_Serial', 'Account Region', 'account_name', 'location.lat', 'location.lon', 'Instr_Type']],
                          left_on='x_Serial', right_on='x_Serial', how='left')

        data['year'], data['month'], data['day'], data['week'], data['date'] = zip(
            *data['context'].apply(field_util.context_to_time))
        data['Yr-Mth'] = data['date'].apply(lambda x: x.strftime('%Y-%m'))
        data['ICS'] = data['ics_version'].fillna('000').apply(lambda x: x[0:3])

        data = field_util.fieldData_mapTo_wafer(data)
        data = data[~data['CCY_Status'].isin(['2_Fail', '8_MechTray'])]

        data = field_util.generate_failure_mode(data)

        field_util.save_field_data(data, os.path.join(save_folder, prod_type))

    else:
        data = pd.read_pickle(os.path.join(save_folder, prod_type + r'.pkl'))

    # acquisition_list = data['acquisition_status'].unique() #['Complete','Failed','InPrep',  'TransferFailed', 'Aborted','Acquiring', 'PostPrimaryAnalysis', 'Initializing','TransferringResults', 'Error', 'TransferPending', 'Aborting','PostPrimaryPending', 'Aligning']
    acquisition_list = ['Complete', 'Failed', 'Minilid', 'Hard_Elec', 'Soft_Elec']
    data1 = data[data['acquisition_status'].isin(acquisition_list)]
    data1 = data1[data1['date'] >= datetime.date(2017, 1, 1)]
    # del data

    field_plot = FieldPlot(data1, prod_type, date_start)

    result_month = field_plot.plot_failure_month('World', acquisition_list, save_folder)
    result_month.to_csv(os.path.join(save_folder, prod_type + '_failure_summary.csv'))
    Region_list = ['APAC', 'EMEA', 'North America - West', 'North America - East', 'South America']
    for region in Region_list:
        try:
            field_plot.plot_failure_month(region, acquisition_list, save_folder)
        except:
            pass

    # Save Failure in 1 month    
#    field_plot.save_failure_instr(save_folder, date_start_1mo)


#    data1['full_name'] = data1['full_name'].apply(lambda x: x[0:20])

#    #Plot P1 distribution by Month
#    field_plot.plot_p1_dist_byMonth()
#    
#    # All Failure by Instr in 1 month
#    field_plot.plot_failure_instr(date_start_1mo)
#
#    
#    # Hard Elec Failure by Instr in 1 year
#    field_plot.plot_hardElec_failure_instr()
#        
#    
#    # Bases Hist by RL in 1 year
#    field_plot.plot_ctrlRL_by_bases()
#    
#    
#    # Low Performer by Instr in 1 year
#    field_plot.plot_lowPerf_instr()


# instr_list = data['full_name'].unique()
#
# for i in instr_list:
#    instr_data = data[data['full_name']==i] 
#    fig, ax = plt.subplots()
#    colors = ['b', 'c', 'y', 'm', 'r']
#    ics_list = instr_data['ics_version_sem_ver'].unique()
#    for idx, ics in enumerate(ics_list):
#        subdata = instr_data[instr_data['ics_version_sem_ver']==ics]
#        ax.scatter(subdata['acquisition_date_utc'],subdata['total_bases'], c=colors[idx],label=ics)
#    ax.legend(loc="upper left",title="ics")
#
