# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 14:26:16 2019

@author: ltu
"""

import pandas as pd
#from matplotlib import pyplot as plt
#import statsmodels.api as sm # import statsmodels 
import os
#from os import listdir
#from os.path import isfile, isdir, join
#import urllib, json
#import re
#import zipfile
import datetime

save_folder = r'\\pbi\dept\chip\ChipEng\Liang\Field Data'   
os.chdir(r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib')  #r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib'
from util_liang import field_util

data = pd.read_pickle(os.path.join(save_folder, 'Sequel' + r'.pkl'))
data['ICS_'] = data['ICS'].apply(lambda x: x if x=='6.0' else '0ld')
failure_sequel = data.groupby(['ICS_','hard_failure_cause']).size().reset_index().rename(columns={0:'count'})
failure_sequel['prod'] = 'Sequel'


data = pd.read_pickle(os.path.join(save_folder, 'Spider' + r'.pkl'))
data['ICS_'] = str(data['ICS'].unique())
failure_spider = data.groupby(['ICS_','hard_failure_cause']).size().reset_index().rename(columns={0:'count'})
failure_spider['prod'] = 'Spider'


failure_pivot = pd.pivot_table(failure_sequel.append(failure_spider), values='count', index=['hard_failure_cause'], columns=[ 'prod', 'ICS_'])
failure_pivot = failure_pivot.sort_values(by = [failure_pivot.columns[1], failure_pivot.columns[2]], ascending=False)

failure_pivot.to_csv('field_failure_mode.csv')