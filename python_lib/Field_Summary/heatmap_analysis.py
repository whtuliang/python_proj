# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 17:54:20 2020

@author: ltu
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
# import statsmodels.api as sm # import statsmodels
import os
from util_liang.GetData import get_movie_rsts
from util_liang.WaferMapGen import ChipMap
from util_liang.MyFunc import image_fft_ifft


def heatmap_gen():
    wafer_list = {'AX16003-17', 'AX17033-13', 'AX17033-17', 'AX17033-19', 'AX18075-13', 'AX18075-18', 'AX18075-24',
                  'AX18075-04', 'AX20130-02'}

    chip_list = ['AX18075-24-4848', 'AX18075-24-4954', 'AX18075-24-5246', 'AX18075-24-5351']
    chip_list = ['AX18075-03-4952', 'AX18075-03-5052', 'AX18075-03-5152', 'AX18075-03-5049', 'AX18075-03-4849']
    chip_list = ['AX17033-13-4948', 'AX17033-13-5148', 'AX17033-13-4850']
    chip_list = ['AX17033-17-5448', 'AX17033-17-5449', 'AX17033-17-5549', 'AX17033-17-5550',
                 'AX17033-19-4648', 'AX17033-19-4651', 'AX17033-19-4746', 'AX17033-19-5549',
                 'AX18075-04-4650', 'AX18075-04-4653', 'AX18075-13-4653', 'AX18075-13-4746',
                 'AX18075-13-4751', 'AX18075-13-4753', 'AX18075-24-5451', 'AX20130-02-4648']
    chip_str = '''AX30071-15-4748 AX30071-15-5147 AX30071-15-5248 AX30071-15-5249
AX30071-17-5253 AX30071-22-4550 AX30071-22-4747 AX30071-22-4749
AX30071-22-4847 AX30071-22-4848 AX30071-22-4853 AX30071-22-4949
AX30071-22-4954 AX30071-22-5054 AX30071-22-5055 AX30071-22-5147
AX30071-22-5153 AX30071-22-5349 AX38092-06-4946 AX38092-06-4953
AX38092-06-5047 AX38092-06-5052 AX38092-06-5153 AX38092-23-5550'''
    chip_str = chip_str.replace('\n', ' ')
    chip_list = chip_str.split(' ')

    data = pd.read_csv(os.path.join(r'\\pbi\dept\chip\ChipEng\Liang\Field Data', 'Spider.csv'))

    # chip_list_str = 'AX08107-08-5045	AW43049-13-5447	AW43049-24-5447	AW47070-10-5447	AW33040-18-4750	AW39035-05-5351	AW39035-06-5350	AW39035-13-4754	AW39035-16-4848	AW39035-16-5347	AW39035-19-5046	AW43049-02-5449	AW43049-03-5550	AW43049-13-5247	AW43049-21-4749	AW43049-24-5551	AW43049-25-4750	AW47070-01-5152	AW47106-08-5550	AW47106-09-4648	AW47106-22-5055	AW47106-25-4948	AX01021-24-5246	AX08107-24-5246	AX08107-24-5254	AX08107-24-5550	AX08108-21-4551	AX08108-22-5151	AX08108-25-5146	AX08109-01-5352	AX08109-06-4653	AX08109-16-5351	AX08109-19-5550	AX08110-01-5046	AX08110-05-4949	AX08110-16-5451	AX08110-18-5254	AX08110-21-5449	AX08110-25-5147	AX14095-01-5150	AX14095-03-5350	AX14095-06-5145	AX14095-10-5447	AX14095-17-4946	AX14095-24-4748	AX14095-24-5447	AX15006-10-5447	AX15006-11-4852	AX15006-15-5450	AX16003-01-5147	AX16003-05-5150	AX16003-13-5353	AX16003-21-4848	AX16003-22-5450	AX17033-10-5551	AX17033-15-5448	AX17033-18-4953	AX17033-20-4750	AX18075-11-4652	AX18075-14-5450	AX18075-16-4754	AX18075-16-4854	AX20130-06-4551	AX27088-12-4652	AX28101-04-4652	AX28101-14-4751	AX28101-16-5151'
    # chip_list = chip_list_str.split('	')
    # chip_i = 'AX08107-08-5045' #AX08107-08-5045  AW47070-10-5447
    for chip_i in chip_list:
        data_chip = data[data['Chip'] == chip_i]
        substrate_i = data_chip.iloc[0]['Substrate']
        # chip_folder = '\\' + data_chip['CollectionPathUri'].iloc[0].replace('/','\\')
        # onlyfile = [f for f in os.listdir(chip_folder) if os.path.isfile(os.path.join(chip_folder, f)) and f[-6:]=='sts.h5']
        # movie_i = onlyfile[0].split('.')[0]

        # os.sys.path.insert(0,r'C:\Users\ltu\Documents\Python Scripts\Python-Lib')

        for movie_i in data_chip['MovieContext']:
            property_plot_list = ['P1_prod']  # , 'P1_ReadLength', 'HQPKmid', 'P1_prod', 'BaseRate'
            for property_plot in property_plot_list:
                try:
                    data_chip_P1 = get_movie_rsts(movie_i, chip_i, 'Spider', property_plot, isEOLQC=False)
                    data_chip_P1[np.isnan(data_chip_P1)] = 0

                    data_chip_filter = image_fft_ifft(data_chip_P1)
                    fig, ax = plt.subplots()  # figsize=(15, 15)
                    plt.imshow(data_chip_P1)
                    plt.title(chip_i + ' ' + property_plot)
                    plt.colorbar()
                    # plt.show()
                except:
                    print(chip_i + ': not found')

            # data_chip = (data_chip_P1 - data_chip_P1.mean()) / data_chip_P1.std()
            # data_chip = -np.exp(-data_chip * 4 - 0)
            # data_chip[data_chip > -4] = 0
            # if data_chip_P1 is not None:
            #     chipmap = ChipMap()
            #     chipmap.set_cm('Spider')
            #     chipmap.img_ind(data_chip, title=chip_i + ' \n' + movie_i)  # , clim=[-10,0]
            #     chipmap.show_cbar(pos=[0.91, 0.45, 0.04, 0.4])
    # plt.show()
                plt.savefig(os.path.join(r'\\pbi\dept\chip\ChipEng\Liang\FA\Liquid_Drop\Images\P1', chip_i+'_'+movie_i +'_'+ substrate_i +'.png'))
                plt.close()


if __name__ == '__main__':
    heatmap_gen()
