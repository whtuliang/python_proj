# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 10:12:54 2019

@author: ltu
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 10:42:39 2019

@author: ltu
"""
import sys
sys.path.append('.\\site_packages')
import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
#import statsmodels.api as sm # import statsmodels 
import os
#from os import listdir
#from os.path import isfile, isdir, join
#import urllib, json
#import math
#import zipfile
import datetime
#import h5py
#import shutil, tempfile
#import pyodbc 
from util_liang.WaferMapGen import radius_prof 
from util_liang.WaferMapGen import WaferMap
from util_liang.GetData import get_movie_rsts
from util_liang.MyFunc import get_range, image_rescale as image_special
from tqdm import tqdm

from util_liang.database_conn import connect_db, get_netdrive_url
from util_liang.color_list import gen_colorList

prod_type = 'Sequel' # 'Spider' 'Sequel'

#movie_c = 'm54193_190802_195625'
#cell_lot = '414831' #'324450'
#wafer = 'PV43001-06'

plot_ma = False

conn1 = connect_db('MES')
conn1.autocommit = True
save_folder = get_netdrive_url(r'\\pbi\dept\chip\ChipEng\Liang\Field Data')
save_file = prod_type + r'.pkl'
data_field = pd.read_pickle( os.path.join(save_folder, save_file) )
 
#dict_cell_wafer = pd.Series(data_field['Wafer'].values, index=data_field['Cell_Lot'])
#dict_cell_wafer['324283'].iloc[0]

data_1 = data_field[data_field['date']>=datetime.date(2020,1,1)]
wafer_list = list(data_1['Wafer'].unique())
wafer_list += ['PW11000-20','PV45127-01']  #,'PV44118-03', 'PV43001-06', 'PV51025-15', 'PV43158-03', 'PV42026-11', 'PV42024-08', 'PV43158-13', 'PV19005-11'     ,'PV52005-03'

#abc = data_1['Wafer'].value_counts()
#wafer_list += abc.sort_values()[-10:].index.to_list()

properties = ['P1_prod', 'P1_loading' , 'NumBases', 'HQP1_start','HQPKmid', 'HQSNR', 'P1_ReadLength', 'BaseLine']

#wafer_list = [wafer_lot+"-{:02d}".format(i) for i in [6,23]]  #range(1,26)



date_object = datetime.date.today()
folder_directory = os.path.join(get_netdrive_url(r'\\pbi\dept\chip\ChipEng\Liang\FA\ChipCenterWorse') , str(date_object) )
if not os.path.exists(folder_directory):
    os.makedirs(folder_directory)

rad_line_list = []
rad_max_list = []
image_1d_list = []

for wafer in tqdm(wafer_list):
    get_data_sql =  ''' select containername as Chip,DataName,DataValue as MovieContext, TxnDate from 
        [insitedb_schema].[PBI_ParametricData] where containername like'{}%' and DataName='moviename' '''.format(wafer)
    data_movie_eolqc = pd.read_sql(get_data_sql, conn1)
    
#    get_data_sql =  ''' select containername as Chip, FromOperation, ToOperation, TransactionDateTime from [DWHView].[GetMoveHistoryDetails] where wafercontainername ='{}' and ToOperation='Functional Test Complete' '''.format(wafer)
#    chip_eolqc = pd.read_sql(get_data_sql, conn2)

    data_field_sel = data_field[data_field['Wafer']==wafer]
    #data_field_sel = data_field[data_field['Cell_Lot']==cell_lot]
    #wafer = data_field_sel['Wafer'].iloc[0]
    if len(data_field_sel)>=1:
        
        for plot_property in ['P0_prod','P1_prod','P2_prod']:
    
            wafermap_P1 = WaferMap()
            wafermap_fit_r = WaferMap()
#            wafermap_fft = WaferMap()
            if plot_ma:
                fig_new = wafermap_P1.create_wafer_map(title= plot_property + ': ' + wafer)
                fig_new1 = wafermap_fit_r.create_wafer_map(title= plot_property + ': ' + wafer)
#                fig_new_fft = wafermap_fft.create_wafer_map(title= plot_property + '_fft: ' + wafer)
           
            # Analyze EOLQC images
            for movie_i,chip_i in zip(data_movie_eolqc['MovieContext'], data_movie_eolqc['Chip']):        
                data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property, isEOLQC=True)
                acount_i = 'EOLQC'
                if data_final is not None:
                    image_special_out = image_special(data_final)
                    if plot_ma:
#                        data_final[0][0] = 100
                        wafermap_P1.img_ind(data_final, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), clim=get_range(data_final))
#                        wafermap_fft.img_ind(image_special_out, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4])) 
                    
                    xm, ym = radius_prof(data_final)[:]    
                    z = np.polyfit(xm, ym, 3)
                    p = np.poly1d(z)
                    xp = np.linspace(5, 80, 20)
                    if plot_ma:
                        wafermap_fit_r.plotxy_ind(xp, p(xp), int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]) )
                    
                    xp_f = p(xp)
                    rad_max_list.append([wafer, chip_i, movie_i, xp[np.argmax(xp_f)], xp[np.argmin(xp_f)], max(xp_f)-min(xp_f), 'EOLQC', acount_i, plot_property])
                    rad_line_list.append([wafer, chip_i, movie_i, 'EOLQC', xp_f, acount_i, plot_property])
                    image_1d_list.append([wafer, chip_i, movie_i, 'EOLQC', image_special_out.flatten(), acount_i, plot_property])
                    
                if plot_ma:    
                    wafermap_P1.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), line='-')
                    wafermap_fit_r.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), line='-')
#                    wafermap_fft.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), line='-')
            
                
            # Analyze Field images
            account_list = data_field_sel['full_name'].unique()
            color_list = gen_colorList(len(account_list))
            for a_idx, account in enumerate(account_list):
                color = color_list[a_idx]
                data_field_account = data_field_sel[data_field_sel['full_name']==account]
                for movie_i,chip_i in zip(data_field_account['MovieContext'], data_field_account['Chip']):
                    data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property)
                    if data_final is not None:
                        image_special_out = image_special(data_final)
                        chip_x = int(chip_i.split('-')[2][0:2])
                        chip_y = int(chip_i.split('-')[2][2:4])
                        if plot_ma:
                            wafermap_P1.img_ind(data_final, chip_x, chip_y)   #, clim=get_range(data_final)
#                            wafermap_fft.img_ind(image_special_out, chip_x, chip_y) 
                            wafermap_P1.show_boarder(chip_x, chip_y, color = color)
#                            wafermap_fft.show_boarder(chip_x, chip_y, color = color)
                        xm, ym = radius_prof(data_final)[:]    
                        z = np.polyfit(xm, ym, 3)
                        p = np.poly1d(z)
                        xp = np.linspace(5, 80, 20)
                        if plot_ma:
                            wafermap_fit_r.plotxy_ind(xp, p(xp), chip_x, chip_y ) 
                            wafermap_fit_r.show_boarder(chip_x, chip_y, color = color)
                        
                        xp_f = p(xp)
                        rad_max_list.append([wafer, chip_i, movie_i, xp[np.argmax(xp_f)], xp[np.argmin(xp_f)], max(xp_f)-min(xp_f), 'Field', account, plot_property])
                        rad_line_list.append([wafer, chip_i, movie_i, 'Field', xp_f, account, plot_property])
                        image_1d_list.append([wafer, chip_i, movie_i, 'Field', image_special_out.flatten(), account, plot_property])
                    
#            wafermap_P1.show_cbar(title='Field')    
            
            if plot_ma:
                wafermap_P1.show_cbar(pos=[0.1, 0.7, 0.02, 0.2], title='cbar')
                fig_new.savefig(os.path.join(folder_directory , wafer + ' - ' + plot_property +'.png') )
                fig_new1.savefig(os.path.join(folder_directory , wafer + ' - ' + plot_property +'_fit.png') )
#                fig_new_fft.savefig(os.path.join(folder_directory , wafer + ' - ' + plot_property +'_fft.png') )
                plt.close(fig_new)
                plt.close(fig_new1)
#                plt.close(fig_new_fft)

                
       
result = pd.DataFrame(image_1d_list, columns=['Wafer','Chip','Movie', 'Test', 'Data', 'Account', 'Property'])
result.to_pickle(os.path.join(folder_directory, 'image_small.pkl'))

result = pd.DataFrame(rad_line_list, columns=['Wafer','Chip','Movie', 'Test', 'Data', 'Account', 'Property'])
result.to_pickle(os.path.join(folder_directory, 'image_rad_line.pkl'))

#import pickle
#with open(os.path.join(folder_directory, 'image_small.pkl'), 'wb') as f:
#    pickle.dump(image_1d_list, f)

result = pd.DataFrame(rad_max_list, columns=['Wafer','Chip','Movie','Pos_Max','Pos_Min','Range', 'Test', 'Account', 'Property'])
result.to_csv(os.path.join(folder_directory, 'heatmap_ring_pattern_summary.csv' ) )

#result = pd.DataFrame(image_1d_list, columns=['Wafer','Chip','Movie', 'Test', 'Data'])
#result.to_csv(os.path.join(folder_directory, 'image_small.csv'))
#
#result = pd.DataFrame(rad_line_list, columns=['Wafer','Chip','Movie'





        
        