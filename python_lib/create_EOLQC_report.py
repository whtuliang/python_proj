# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 10:31:03 2019

@author: ltu
"""

import json, os
import requests
import re
import pandas as pd
import numpy as np
import plotly.graph_objects as go
from matplotlib import pyplot as plt
from PIL import Image
from io import BytesIO
import matplotlib.image as mpimg
from pptx import Presentation
from pptx.util import Inches, Cm
import csv
import warnings

warnings.filterwarnings("ignore")
from IPython.core.display import display, HTML

# response = requests.get('http://itg/metrics-exp/scc/tables/FunctionalTest')
# table = pd.read_json(response.content)
# response = requests.get('http://itg/metrics-exp/scc/tables/Wafer')
# table = pd.read_json(response.content)

from SPC_Summary.get_BCD import get_BCD_fit_chip


class EOLQC_Report:
    def __init__(self):
        self.wafer_str = ""
        self.wafer_list = []
        self.df_wafer = pd.DataFrame()
        self.df_eolqc = pd.DataFrame()
        self.chip_images = ''

    def set_waferlist(self, wafer_str='AX08107-24, PV44120-24'):
        self.wafer_str = wafer_str
        self.wafer_list = list(map(lambda x: x.strip(), wafer_str.split(',')))

    def get_wafer_info(self):
        ################ get Wafer summary from SmrtCentral
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'text/csv'  # application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        }
        df_wafer = pd.DataFrame()
        for wafer in self.wafer_list:
            query = {
                "start": 0,
                "length": 3,
                "columns": [
                    {
                        "name": "ContainerName",
                        "search": wafer
                    },
                    {"name": "CustomerLot"},
                    {"name": "BatchAlias"},
                    {"name": "SALot"},
                    {"name": "Owner"},
                    {"name": "QtyA"},
                    {"name": "QtyB"},
                    {"name": "QtyC"},
                    {"name": "QtyD"},
                    {"name": "QtyFTC"},
                    {"name": "QtyFTD"},
                    {"name": "MetrologyStatus"},
                    {"name": "Operation"},
                    {"name": "DeviationLot"},
                    {"name": "NCR"}
                ],
                "order": {
                    "column": 1,
                    "dir": "desc"
                }
            }
            data = json.dumps(query)
            response = requests.post('http://itg/metrics-dev/scc/tables/Wafer/export', headers=headers, data=data)
            abc = response.content
            aaa = abc.decode().split('\n')

            # data_wafer = [aaa[i][1:-1].split('","')  for i in range(1, len(aaa))]
            def get_section(s, idx):
                if idx >= len(s):
                    return None
                elif s[idx] == '"':
                    end = s[idx + 1:].find('"')
                    return (s[idx + 1: idx + 1 + end].replace(',', ' '), idx + 1 + end + 2)
                else:
                    end = s[idx:].find(',')
                    return (s[idx: idx + end], idx + end + 1)

            data_list = []
            for i in range(1, len(aaa)):
                line = aaa[i]
                idx = 0
                line_list = []
                while get_section(line, idx):
                    item = get_section(line, idx)
                    line_list += [item[0]]
                    idx = item[1]
                data_list += [line_list]

            col_str = aaa[0].split(',')
            columns = [b.strip('"') for b in col_str]
            df_wafer = df_wafer.append(pd.DataFrame(data_list, columns=columns), ignore_index=True)

            # render df
            df_wafer['SALot'] = df_wafer['SALot'].apply(lambda x: '..' + x[-3:])
            df_wafer['NCR'] = df_wafer['NCR'].apply(lambda x: x.replace('NCR-00000', '..'))
            df_wafer['Metrology'] = df_wafer['Metrology'].apply(lambda x: x.replace(' ', ', '))
            col_list = ['QtyA', 'QtyB', 'QtyC', 'QtyD']
            df_wafer['Qty A/B/C/D'] = df_wafer[col_list].apply(lambda x: ', '.join(x[col_list]), axis=1)
            df_wafer = df_wafer.drop(columns=col_list)
            df_wafer = df_wafer[
                ['Wafer', 'CustomerLot', 'Alias', 'SALot', 'Owner', 'Qty A/B/C/D', 'FTC', 'FTData', 'Metrology',
                 'Operation', 'Deviation', 'NCR']]

        self.df_wafer = df_wafer
        return df_wafer

    @classmethod
    def get_all_EOLQC(cls, date='2020-04-01'):
        # Check Tab table information
        #        response = requests.get('http://itg/metrics-exp/scc/tables/FunctionalTest')
        #        table = pd.read_json(response.content)
        df_eolqc = pd.DataFrame()
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'text/csv'  # application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        }
        query = {
            "start": 0,
            "length": 3,
            "columns": [
                {"name": "EOLUploadDate",
                 "search": date
                 },
                {"name": "WaferContainerName"},
                {"name": "ContainerName"},
                {"name": "Substrate"},
                {"name": "moviename"},
                {"name": "ODS_IllumImage"},
                {"name": "pd_Empty_Heatmap"},
                {"name": "pd_Productive_Heatmap"},
                {"name": "pd_Other_Heatmap"},
                {"name": "pdProd_pct"},
                {"name": "coupler_laser_power_1"},
                {"name": "coupler_laser_power_2"},
                {"name": "HQPkMidMedian_C"},
                {"name": "CustomerLot"},
                {"name": "BatchAlias"},
                {"name": "Owner"},
                {"name": "Project"}
            ],
            "order": {
                "column": 1,
                "dir": "desc"
            }
        }
        data = json.dumps(query)
        response = requests.post('http://itg/metrics-dev/scc/tables/FunctionalTest/export', headers=headers, data=data)

        abc = response.content
        aaa = abc.replace(b'"', b'').decode().split('\n')

        data_eolqc_wafer = [aaa[i].split(',')[:17] for i in range(1, len(aaa))]
        df_eolqc = df_eolqc.append(pd.DataFrame(data_eolqc_wafer, columns=aaa[0].split(',')), ignore_index=True)
        #        df_eolqc = df_eolqc[df_eolqc['EOLUploadDate'] > date]
        return list(df_eolqc['Wafer'].unique())

    @classmethod
    def get_chip_Grade(self, chip_list):
        df_chip = pd.DataFrame()
        for chip in chip_list:
            headers = {
                'Content-Type': 'application/json',
                'Accept': 'text/csv'  # application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            }
            query = {
                "start": 0,
                "length": 3,
                "columns": [
                    {
                        "name": "ContainerName",
                        "search": chip
                    },
                    {"name": "GradeChip"}
                ],
                "order": {
                    "column": 0,
                    "dir": "desc"
                }
            }
            data = json.dumps(query)
            response = requests.post('http://itg/metrics-dev/scc/tables/Chip/export', headers=headers, data=data)

            abc = response.content
            aaa = abc.replace(b'"', b'').decode().split('\n')

            data_eolqc_wafer = [aaa[i].split(',')[:len(query['columns'])] for i in range(1, len(aaa))]
            data_eolqc_wafer = pd.DataFrame(data_eolqc_wafer, columns=aaa[0].split(','))

            df_chip = df_chip.append(data_eolqc_wafer, ignore_index=True)
        #             df_eolqc = df_eolqc.astype({'pdProd%':float, "LP1": float, "LP2": float, "PkMidC": float, "BCD (nm)": float})
        #        self.df_eolqc = df_eolqc
        return df_chip

    @classmethod
    def get_chip_defect_owner_proj(cls, chip_list):
        import pyodbc
        if os.name == 'nt':
            conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
        else:
            server = 'usmp-vm-campd8d\sql2012ods'
            username = 'odbc'
            password = 'readonly'
            conn_MES = pyodbc.connect(
                'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)

        conn_MES.autocommit = True
        get_data_sql = '''SELECT [Container] ContainerName,[DefectCode],[Comments] as DefectComments
            FROM [CamODS_6X].[insitedb_schema].[PBI_EOLDefectCode]
            WHERE Container in ('{}')
        '''.format('\',\''.join(chip_list))
        data = pd.read_sql(get_data_sql, conn_MES)

        get_data_sql = '''SELECT [ContainerName],[OwnerId]
            FROM [CamODS_6X].[insitedb_schema].[Container]
            WHERE ContainerName in ('{}')
        '''.format('\',\''.join(chip_list))
        data_owner = pd.read_sql(get_data_sql, conn_MES)

        get_data_sql = '''SELECT  [OwnerId],[OwnerName]
            FROM [CamODS_6X].[insitedb_schema].[Owner]
        '''
        data_owne_id = pd.read_sql(get_data_sql, conn_MES)

        data_owner = data_owner.merge(data_owne_id, left_on='OwnerId', right_on='OwnerId', how='left')
        data = data.merge(data_owner, left_on='ContainerName', right_on='ContainerName', how='outer')

        get_data_sql = '''SELECT [ContainerName],[DataName],[DataValue],[TxnDate],[DataCollectionDefName]
            FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
            WHERE ContainerName in ('{}') and DataName = 'Project'
        '''.format('\',\''.join(chip_list))
        data_proj = pd.read_sql(get_data_sql, conn_MES)
        data_proj = data_proj[data_proj['DataCollectionDefName'] == 'Functional Test_A']
        data_proj = data_proj.sort_values(by='TxnDate', ascending=False).drop_duplicates(subset=['ContainerName','DataValue'], keep='first')

        data = data.merge(data_proj[['ContainerName', 'DataValue']], left_on='ContainerName', right_on='ContainerName',
                          how='left')

        data = data.rename(columns={'ContainerName': 'Chip', 'OwnerName': 'Owner', 'DataValue': 'Project'})
        data = data.drop(columns=['OwnerId'])

        conn_MES.close()
        return data

    @classmethod
    def get_Sample_Prep_Lot(cls, chip_list):
        import pyodbc
        if os.name == 'nt':
            conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
        else:
            server = 'usmp-vm-campd8d\\sql2012ods'
            username = 'odbc'
            password = 'readonly'
            conn_MES = pyodbc.connect(
                'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)

        conn_MES.autocommit = True
        get_data_sql = '''SELECT [ContainerName] chip , [DataValue] Sample_Prep_Lot_Num, [TxnDate]
            FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
            WHERE DataName = 'Sample Prep Lot Number' and  ContainerName in ('{}')
        '''.format('\',\''.join(chip_list))
        data = pd.read_sql(get_data_sql, conn_MES)
        data = data.loc[data.groupby(["chip"])["TxnDate"].idxmax()]
        return data

    def get_wafer_EOLQC(self):
        df_eolqc = pd.DataFrame()
        for wafer in self.wafer_list:
            headers = {
                'Content-Type': 'application/json',
                'Accept': 'text/csv'  # application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            }
            query = {
                "start": 0,
                "length": 3,
                "columns": [
                    {
                        "name": "WaferContainerName",
                        "search": wafer
                    },
                    {"name": "ContainerName"},
                    {"name": "Substrate"},
                    {"name": "moviename"},
                    {"name": "ODS_IllumImage"},
                    {"name": "pd_Empty_Heatmap"},
                    {"name": "pd_Productive_Heatmap"},
                    {"name": "pd_Other_Heatmap"},
                    {"name": "pdProd_pct"},
                    {"name": "coupler_laser_power_1"},
                    {"name": "coupler_laser_power_2"},
                    {"name": "HQPkMidMedian_C"},
                    {"name": "CustomerLot"},
                    {"name": "BatchAlias"},
                    {"name": "Owner"},
                    {"name": "LIMSRuncode"},
                    {"name": "Project"}
                ],
                "order": {
                    "column": 1,
                    "dir": "desc"
                }
            }
            data = json.dumps(query)
            response = requests.post('http://itg/metrics-dev/scc/tables/FunctionalTest/export', headers=headers,
                                     data=data)

            abc = response.content
            aaa = abc.replace(b'"', b'').decode().split('\n')

            data_eolqc_wafer = [aaa[i].split(',')[:len(query['columns'])] for i in range(1, len(aaa))]
            data_eolqc_wafer = pd.DataFrame(data_eolqc_wafer, columns=aaa[0].split(','))

            data_eolqc_BCD = get_BCD_fit_chip(list(data_eolqc_wafer['Chip']))
            data_eolqc_wafer = data_eolqc_wafer.merge(data_eolqc_BCD, how='left', left_on='Chip', right_on='ChipID')

            df_eolqc = df_eolqc.append(data_eolqc_wafer, ignore_index=True)
            df_eolqc = df_eolqc.astype(
                {'pdProd%': float, "LP1": float, "LP2": float, "PkMidC": float, "BCD (nm)": float})
        self.df_eolqc = df_eolqc
        return df_eolqc

    @classmethod
    def get_runcode_MES(cls, chip_list):
        import pyodbc
        if os.name == 'nt':
            conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
        else:
            server = 'usmp-vm-campd8d/sql2012ods'
            username = 'odbc'
            password = 'readonly'
            conn_MES = pyodbc.connect(
                'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)
        conn_MES.autocommit = True
        get_data_sql = '''SELECT [ContainerName] ContainerName,[DataValue],[TxnDate] 
                    FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
                    WHERE ContainerName in ('{}') and DataName = 'sts_xml'
                '''.format('\',\''.join(chip_list))
        data = pd.read_sql(get_data_sql, conn_MES)
        conn_MES.close()
        data = data.sort_values(by=['TxnDate'], ascending=False)
        data = data.drop_duplicates(subset=['ContainerName'])
        data['RunCode'] = data['DataValue'].apply(lambda x: x.split('/')[4])
        return data

    @classmethod
    def get_runcode_chips_MES(cls, runcode_list):
        import pyodbc
        if os.name == 'nt':
            conn_MES = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly')  # camstarsql_mes
        else:
            server = 'usmp-vm-campd8d\sql2012ods'
            username = 'odbc'
            password = 'readonly'
            conn_MES = pyodbc.connect(
                'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';UID=' + username + ';PWD=' + password)
        conn_MES.autocommit = True
        chip_list = pd.DataFrame()
        if runcode_list != ['']:
            for runcode in runcode_list:
                get_data_sql = '''SELECT [ContainerName] ContainerName,[DataValue],[TxnDate] 
                                FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData]
                                WHERE DataValue like '/pbi/collections/%{}%.sts.xml' and DataName = 'sts_xml' ---and TxnDate > '2020-01-01'
                            '''.format(runcode)
                data = pd.read_sql(get_data_sql, conn_MES)
                data = data.sort_values(by=['TxnDate'], ascending=False)
                data = data.drop_duplicates(subset=['ContainerName'])
                data['RunCode'] = data['DataValue'].apply(lambda x: x.split('/')[4])
                chip_list = chip_list.append(data[['RunCode', 'ContainerName']])
        # chip_list['wafer'] = chip_list['ContainerName'].apply(lambda x:x[:10])
        conn_MES.close()
        from util_liang import sql_collections
        df_eolqc = sql_collections.SCC_EOLQC(chip_list['ContainerName'].unique().tolist())
        # df_eolqc = df_eolqc.rename(columns = {'Chip':'ContainerName', 'ODS_Image':'ODS_IllumImage','':''} )
        df_chip_grade = cls.get_chip_Grade(list(df_eolqc['Chip']))
        df_eolqc = df_eolqc.merge(df_chip_grade, left_on='Chip', right_on='Chip', how='left')
        df_chip_defect = cls.get_chip_defect_owner_proj(list(df_eolqc['Chip']))
        df_eolqc = df_eolqc.merge(df_chip_defect, left_on='Chip', right_on='Chip', how='left')
        df_eolqc = df_eolqc.rename(
            columns={'wafer': 'Wafer', 'moviename': 'Movie', 'BCD_estimate': 'BCD (nm)', 'ALPStop_L1Power_mw': 'LP1',
                     'ALPStop_L2Power_mw': 'LP2', 'HQPkMidMedian_C': 'PkMidC'})  # 'Owner',  'Project'
        df_eolqc['ChipID'] = df_eolqc['Chip']

        df_eolqc['pdProdMap'] = "http://usmp-vm-sequelwebapp/heatmaps/EOLQC/" + df_eolqc['Instrument'] + "/" + df_eolqc[
            'Movie'] + "." + df_eolqc['Substrate'] + ".P1.gif"
        df_eolqc['pdEmptyMap'] = "http://usmp-vm-sequelwebapp/heatmaps/EOLQC/" + df_eolqc['Instrument'] + "/" + \
                                 df_eolqc['Movie'] + "." + df_eolqc['Substrate'] + ".P0.gif"
        df_eolqc['pdOtherMap'] = "http://usmp-vm-sequelwebapp/heatmaps/EOLQC/" + df_eolqc['Instrument'] + "/" + \
                                 df_eolqc['Movie'] + "." + df_eolqc['Substrate'] + ".P2.gif"

        return df_eolqc

    @classmethod
    def get_runcode_EOLQC(cls, runcode_list):
        df_eolqc = pd.DataFrame()
        #        columns= ['EOLUploadDate','Wafer','Chip','Substrate','Movie','ODS_Image',
        #        'pdEmptyMap','pdProdMap','pdOtherMap','pdProd%','LP1','LP2','PkMidC','CustomerLot','Alias','Owner','Project','Runcode']
        if runcode_list != ['']:
            for runcode in runcode_list:
                headers = {
                    'Content-Type': 'application/json',
                    'Accept': 'text/csv'  # application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                }
                query = {
                    "start": 0,
                    "length": 3,
                    "columns": [
                        {
                            "name": "WaferContainerName"
                        },
                        {"name": "ContainerName"},
                        {"name": "Substrate"},
                        {"name": "moviename"},
                        {"name": "ODS_IllumImage"},
                        {"name": "pd_Empty_Heatmap"},
                        {"name": "pd_Productive_Heatmap"},
                        {"name": "pd_Other_Heatmap"},
                        {"name": "pdProd_pct"},
                        {"name": "coupler_laser_power_1"},
                        {"name": "coupler_laser_power_2"},
                        {"name": "HQPkMidMedian_C"},
                        {"name": "CustomerLot"},
                        {"name": "BatchAlias"},
                        {"name": "Owner"},
                        {"name": "LIMSRuncode", "search": runcode},
                        {"name": "Project"}
                    ],
                    "order": {
                        "column": 1,
                        "dir": "desc"
                    }
                }
                data = json.dumps(query)
                response = requests.post('http://itg/metrics-dev/scc/tables/FunctionalTest/export', headers=headers,
                                         data=data)

                abc = response.content
                aaa = abc.replace(b'"', b'').decode().split('\n')

                data_eolqc_wafer = [aaa[i].split(',')[:len(query['columns'])] for i in range(1, len(aaa))]
                data_eolqc_wafer = pd.DataFrame(data_eolqc_wafer, columns=aaa[0].split(','))

                data_eolqc_BCD = get_BCD_fit_chip(list(data_eolqc_wafer['Chip']))
                data_eolqc_wafer = data_eolqc_wafer.merge(data_eolqc_BCD, how='left', left_on='Chip', right_on='ChipID')

                df_eolqc = df_eolqc.append(data_eolqc_wafer, ignore_index=True)
                df_eolqc = df_eolqc.astype(
                    {'pdProd%': float, "LP1": float, "LP2": float, "PkMidC": float, "BCD (nm)": float})
        return df_eolqc

    def gen_chip_image(self, df_eolqc1, owner, ODS_enlarged=True):

        # df_eolqc = self.df_eolqc
        # df_eolqc1 = df_eolqc[df_eolqc['Owner']== owner]
        #        df1 = df_eolqc1.drop(['Wafer','ODS_Image', 'pdEmptyMap','pdProdMap','pdOtherMap'], axis=1)
        img_list = ['ODS_Image', 'pdEmptyMap', 'pdProdMap', 'pdOtherMap']

        def find_valid_ODS_path(ods_path):  # df_eolqc['ODS_Image'][2]
            abc = ods_path.split('/')
            sections = abc[-1].split('_')
            if os.name == 'nt':
                folder_dir = r"\\pbi\dept\chip\ChipMfgQC"
            else:
                folder_dir = "//pbi/dept/chip/ChipMfgQC"
            if 'spider' in abc[3]:
                ODS_dir = os.path.join(folder_dir, 'SpiderWipData', 'OpticalDrySort')
            else:
                ODS_dir = os.path.join(folder_dir, 'WipData', 'OpticalDrySort')
            file_list = os.listdir(os.path.join(ODS_dir, abc[-2]))
            for file in file_list:
                if re.search("^{0}.*{1}$".format(sections[0], sections[-1]), file):
                    return os.path.join(ODS_dir, abc[-2], file)
            return None

        def find_ODS_path(wafer, substrate):  # df_eolqc['ODS_Image'][2]
            wafer = wafer[:7]
            from util_liang.spc_util import get_prodtype
            if os.name == 'nt':
                folder_dir = r"\\pbi\dept\chip\ChipMfgQC"
            else:
                folder_dir = "//pbi/dept/chip/ChipMfgQC"
            if get_prodtype(wafer) == 'Spider':
                ODS_dir = os.path.join(folder_dir, 'SpiderWipData', 'OpticalDrySort')
                http_dir = 'http://usmp-vm-sequelwebapp/drysort_spider'
            else:
                ODS_dir = os.path.join(folder_dir, 'WipData', 'OpticalDrySort')
                http_dir = 'http://usmp-vm-sequelwebapp/drysort'

            file_list = os.listdir(os.path.join(ODS_dir, wafer))
            for file in file_list:
                if re.search("^{0}.*{1}$".format(wafer + '-' + substrate, '_IllumImage.bmp'), file):
                    return http_dir + '/' + wafer + '/' + file
            return None

        #        print(len(df_eolqc1))
        if not df_eolqc1.empty:
            fig, ax = plt.subplots(len(df_eolqc1), len(img_list), sharex='none', sharey='none',
                                   figsize=(12, 12 * len(df_eolqc1) / len(img_list)), squeeze=False)
            for i in range(len(df_eolqc1)):
                ii = df_eolqc1.index[i]
                if not df_eolqc1['ODS_Image'][ii]:
                    df_eolqc1['ODS_Image'][ii] = find_ODS_path(df_eolqc1['Wafer'][ii], df_eolqc1['Substrate'][ii])
                for j in range(len(img_list)):
                    try:
                        img_link = df_eolqc1[img_list[j]][ii]
                        response = requests.get(img_link)
                        if (response.status_code == 404) and (j != 0):
                            img_link = img_link.split('/')
                            img_link[-2] = 'alpha9'
                            img_link = '/'.join(img_link)
                            response = requests.get(img_link)
                        ax[i][j].set_xticks([])
                        ax[i][j].set_yticks([])
                        ax[i][j].set_url(img_link)
                        if i == 0:
                            ax[i][j].set_title(img_list[j], fontsize=16)

                        if j == 0:
                            ax[i][j].set_ylabel(df_eolqc1['Chip'][ii], fontsize=12)
                            try:
                                img = Image.open(BytesIO(response.content))
                                img = np.log2((img.astype(float)) / 50 + 1)
                                ax[i][j].imshow(img, cmap='gray', aspect='auto')
                            except:
                                # print(df_eolqc1['Chip'][ii] + ' ' + img_list[j] + ' not found: ' + df_eolqc1[img_list[j]][ii])
                                chip_gif = find_valid_ODS_path(img_link)
                                if chip_gif:
                                    img = mpimg.imread(chip_gif)
                                    img = np.log2((img.astype(float)) / 50 + 1)
                                    ax[i][j].imshow(img, cmap='gray', aspect='auto')
                        else:
                            try:
                                ax[i][j].imshow(Image.open(BytesIO(response.content)))
                            except:
                                print(
                                    df_eolqc1['Chip'][ii] + ' ' + img_list[j] + ' not found: ' + df_eolqc1[img_list[j]][
                                        ii])
                    except:
                        print('Wrong path: ' + img_list[j] + ' ' + df_eolqc1['Chip'][ii])
            #        fig.show()
            fig.savefig('temp.png')
            self.chip_images = os.path.join(os.getcwd(), 'temp.png')

            #        display(HTML('<h2> ODS Images (enlarged) </h2>'))
            if ODS_enlarged:
                row_n = (len(df_eolqc1) + 1) // 2
                fig, ax = plt.subplots(row_n, 2, sharex='none', sharey='none', figsize=(16, 8 * row_n), squeeze=False)
                for i, ax_row in enumerate(ax):
                    for j, ax_ind in enumerate(ax_row):
                        ax_ind.set_xticks([])
                        ax_ind.set_yticks([])
                        if (len(df_eolqc1) % 2 == 1) and (i == row_n - 1) and (j == 1):
                            ax_ind.set_visible(False)
                for i in range(len(df_eolqc1)):
                    ii = df_eolqc1.index[i]
                    j = 0
                    img_link = df_eolqc1[img_list[j]][ii]
                    try:
                        response = requests.get(img_link)
                        ax_chip = ax[i // 2][i % 2]
                        #                    ax_chip.set_xticks([])
                        #                    ax_chip.set_yticks([])
                        ax_chip.set_url(img_link)
                        # ax_chip.set_ylabel(df_eolqc1['Chip'][ii], fontsize=12)
                        ax_chip.set_title(df_eolqc1['Chip'][ii], fontsize=16)
                        try:
                            img = Image.open(BytesIO(response.content))
                            img = np.log2((img.astype(float)) / 50 + 1)
                            ax_chip.imshow(img, cmap='gray', aspect='equal')
                        except:
                            chip_gif = find_valid_ODS_path(img_link)
                            if chip_gif:
                                img = mpimg.imread(chip_gif)
                                img = np.log2((img.astype(float)) / 50 + 1)
                                ax_chip.imshow(img, cmap='gray', aspect='equal')
                    except:
                        pass

    def gen_wafer_image(self):  # Etiene's image
        if os.name == 'nt':
            os.chdir(r"\\pbi\dept\chip\ChipEng\Liang\Python-Lib")
        else:
            os.chdir(r"\\pbi\dept\chip\ChipEng\Liang\Python-Lib".replace('\\', '/'))
        from util_liang import WaferMapGen
        for wafer in self.wafer_list:
            # file link: wafer_gif = WaferMapGen.get_wafer_HTMLimage(wafer)#
            wafer_gif = WaferMapGen.get_wafer_image(wafer).replace('\\', '/')
            fig, ax = plt.subplots(figsize=(14, 14))
            img = mpimg.imread(wafer_gif)
            ax.set_axis_off()
            imgplot = ax.imshow(img)
            plt.show()

    @classmethod
    def get_wafer_image_Jelena(cls, wafer):  # Jelena's image
        if os.name == 'nt':
            folder_path = r'\\pbi\dept\chip\ChipEng'
        else:
            folder_path = '//pbi/dept/chip/ChipEng'

        file_path = os.path.join(folder_path, 'LoadingMapRConnect', wafer + '.jpeg')
        if os.path.exists(file_path):
            fig, ax = plt.subplots(figsize=(16, 16))
            img = mpimg.imread(file_path)
            ax.set_axis_off()
            _ = ax.imshow(img)
        return file_path

    # reate_ppt_table(self):

    def gen_EOLQC_ppt(self):
        prs = Presentation()
        title_slide_layout = prs.slide_layouts[0]
        slide = prs.slides.add_slide(title_slide_layout)  # title_slide_layout
        title = slide.shapes.title
        subtitle = slide.placeholders[1]
        title.text = "EOLQC FA Report"
        subtitle.text = self.wafer_str

        bullet_slide_layout = prs.slide_layouts[1]
        slide = prs.slides.add_slide(bullet_slide_layout)
        shapes = slide.shapes
        title_shape = shapes.title
        body_shape = shapes.placeholders[1]
        title_shape.text = 'Adding a Bullet Slide'
        tf = body_shape.text_frame
        tf.text = 'Find the bullet slide layout'
        p = tf.add_paragraph()
        p.text = 'Use _TextFrame.text for first bullet'
        p.level = 1
        p = tf.add_paragraph()
        p.text = 'Use _TextFrame.add_paragraph() for subsequent bullets'
        p.level = 2

        if os.name == 'nt':
            folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Script\EOLQC_summary'
        else:
            folder_path = '//pbi/dept/chip/ChipEng/Liang/Yield/Script/EOLQC_summary'
        blank_slide_layout = prs.slide_layouts[6]
        slide = prs.slides.add_slide(blank_slide_layout)
        rows_n, cols_n = self.df_wafer.shape
        graph_frame1 = slide.shapes.add_table(rows=cols_n, cols=rows_n + 1, left=Cm(0), top=Cm(2), width=Cm(20),
                                              height=Cm(10))
        table = graph_frame1.table
        table.columns[0].width = Cm(5)
        for j in range(cols_n):
            for i in range(rows_n):
                table.cell(j, 0).text = self.df_wafer.columns[j]
                table.cell(j, i + 1).text = self.df_wafer.iloc[i, j]

        blank_slide_layout = prs.slide_layouts[6]
        slide = prs.slides.add_slide(blank_slide_layout)
        img_path = self.chip_images
        pic = slide.shapes.add_picture(img_path, left=Cm(2), top=Cm(2), width=Cm(20))

        prs.save(os.path.join(folder_path, 'test.pptx'))


#        img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_CCYA_13.png')
#        pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(1), height=Inches(1.5), width=Inches(4.5))
#        img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_CCYA_4.png')
#        pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(3), height=Inches(1.5), width=Inches(4.5))
#        img_path = os.path.join(folder_path, 'Plots/Sequel_WorkWeek_CCYA_1_locf.png')
#        pic = slide.shapes.add_picture(img_path, left=Inches(0.5), top=Inches(5), height=Inches(1.5), width=Inches(4.5))
#        img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_13.png')
#        pic = slide.shapes.add_picture(img_path, left=Inches(5.5), top=Inches(1), height=Inches(1.5), width=Inches(4.5))
#        img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_4.png')
#        pic = slide.shapes.add_picture(img_path, left=Inches(5.5), top=Inches(3), height=Inches(1.5), width=Inches(4.5))
#        img_path = os.path.join(folder_path, 'Plots/Spider_WorkWeek_CCYA_1_locf.png')
#        pic = slide.shapes.add_picture(img_path, left=Inches(5.5), top=Inches(5), height=Inches(1.5), width=Inches(4.5))

if __name__ == '__main__':
    report = EOLQC_Report()
    report.set_waferlist(wafer_str='AX08107-24')
    df_wafer = report.get_wafer_info()
    df_eolqc = report.get_wafer_EOLQC()
    report.gen_chip_image(df_eolqc, 'Sample')
    # report.gen_chip_image(df_eolqc, 'ENG')
    # report.gen_wafer_image()
    report.gen_EOLQC_ppt()
