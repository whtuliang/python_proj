# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 10:42:39 2019

@author: ltu
"""
import sys
import os
#sys.path.append(os.path.dirname(os.path.abspath(__file__)))

import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
#import statsmodels.api as sm # import statsmodels 

#from os import listdir
#from os.path import isfile, isdir, join
import urllib, json
import math
import zipfile
import datetime
import h5py
import shutil, tempfile
import pyodbc 
import matplotlib.patches as mpatches
#import util
from util_liang.WaferMapGen import WaferMap
from util_liang.GetData import get_movie_rsts
from util_liang.MyFunc import get_range
from util_liang.spc_util import get_prodtype
from util_liang.spc_util import get_EOLQC_chips


#from WaferMapGen import WaferMap
#from GetData import get_movie_rsts
#from MyFunc import get_range

color_list = ['blue', 'red', 'green', 'aqua','gold','cyan', 'black', 'brown', 'lime', 'orchid']
    

prod_type = 'Spider' # 'Spider' 'Sequel'

movie_c = 'm54193_190802_195625'
cell_lot = '414831' #'324450'
wafer = 'PV43001-06'



#cdict = {'red':  ((0.0, 0.0, 0.0),
#                   (0.25, 0.0, 0.0),
#                   (0.5, 0.8, 1.0),
#                   (0.75, 1.0, 1.0),
#                   (1.0, 0.4, 1.0)),
#
#         'green': ((0.0, 0.0, 0.0),
#                   (0.25, 0.0, 0.0),
#                   (0.5, 0.9, 0.9),
#                   (0.75, 0.0, 0.0),
#                   (1.0, 0.0, 0.0)),
#
#         'blue':  ((0.0, 0.0, 0.4),
#                   (0.25, 1.0, 1.0),
#                   (0.5, 1.0, 0.8),
#                   (0.75, 0.0, 0.0),
#                   (1.0, 0.0, 0.0))
#        }

#save_folder = r'\\sharepoint\mfg\sequelcell\Documents\Manufacturing Engineering\Yield Reports\Weekly Yield Meetings\Data\Field Data'


 
#dict_cell_wafer = pd.Series(data_field['Wafer'].values, index=data_field['Cell_Lot'])
#dict_cell_wafer['324283'].iloc[0]




#EOLQC_chip_gif('P151015-24')
wafer_lot = 'AX01021-04'
properties = ['P1_prod', 'P1_loading' , 'NumBases', 'HQP1_start','HQPKmid', 'HQSNR', 'P1_ReadLength', 'BaseLine']

wafer_list = [wafer_lot+"-{:02d}".format(i) for i in [3]]  #range(1,26)

#if os.name == 'nt':
#    conn1 = pyodbc.connect('DSN=camstarsql_mes;UID=odbc;PWD=readonly;Database=CamODS_6X')  #camstarsql_mes
#else:
#    server = 'usmp-vm-campd8d\sql2012ods'
#    database = 'CamODS_6X' 
#    username = 'odbc' 
#    password = 'readonly' 
#    conn1 = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
#    
#conn1.autocommit = True

date_object = datetime.date.today()
#folder_directory = r'\\pbi\dept\chip\ChipEng\Liang\FA\ChipCenterWorse\\' + str(date_object)
#if not os.path.exists(folder_directory):
#    os.makedirs(folder_directory)
if os.name == 'nt':
    folder_directory = r'\\pbi\dept\chip\ChipEng\Liang\EOLQC\P1_images'
else:
    folder_directory = r'\\pbi\dept\chip\ChipEng\Liang\EOLQC\P1_images'.replace('\\','/')


def generate_wafermap(wafer_list: list, properties = ['P1_prod']):
    save_folder = r'\\pbi\dept\chip\ChipEng\Liang\Field Data'
    prod_type = wafer_list[0]
    save_file = get_prodtype(prod_type) + r'.pkl'
    data_field = pd.read_pickle( os.path.join(save_folder, save_file) )
    
    plots_record = pd.DataFrame(columns = properties, index = wafer_list)
    for wafer in wafer_list:  #wafer_list:
        data_movie_local = get_EOLQC_chips(wafer)
        data_movie_local['MovieContext'] = data_movie_local['moviename']
        data_movie_eolqc = data_movie_local[ data_movie_local['type']=='EOLQC' ]
        data_movie_eng = data_movie_local[ data_movie_local['type']=='Eng Run' ]
#        get_data_sql =  ''' select containername as Chip,DataName,DataValue as MovieContext, TxnDate from 
#            [insitedb_schema].[PBI_ParametricData] where containername like'{}%' and DataName='moviename' '''.format(wafer)
#        data_movie_eolqc = pd.read_sql(get_data_sql, conn1)
        data_field_sel = data_field[data_field['Wafer']==wafer]
        #data_field_sel = data_field[data_field['Cell_Lot']==cell_lot]
        #wafer = data_field_sel['Wafer'].iloc[0]
        if len(data_field_sel)>=1 or len(data_movie_local)>=1:
            
            for plot_property in properties:
                plot_dir = folder_directory + r'\\'+wafer + ' - ' + plot_property +'.png'
                if not os.path.isfile(plot_dir):
                    wafermap_P1 = WaferMap()
                    fig_new = wafermap_P1.create_wafer_map(title= plot_property + ': ' + wafer)
                    for movie_i,chip_i in zip(data_movie_eolqc['MovieContext'], data_movie_eolqc['Chip']):        
                        data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property, isEOLQC=True)
                        if data_final is not None:
                            wafermap_P1.img_ind(data_final, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), clim=get_range(data_final))
                        wafermap_P1.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]))
                    wafermap_P1.show_cbar(pos=[0.1, 0.7, 0.02, 0.2], title='EOLQC')
                    
                    for movie_i,chip_i in zip(data_movie_eng['MovieContext'], data_movie_eng['Chip']):        
                        data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property, isEOLQC=True)
                        if data_final is not None:
                            wafermap_P1.img_ind(data_final, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), clim=get_range(data_final))
                        wafermap_P1.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), color = 'blue')
    #                wafermap_P1.show_cbar(pos=[0.1, 0.7, 0.02, 0.2], title='Engineer_Run')
                        
                    for movie_i,chip_i in zip(data_field_sel['MovieContext'], data_field_sel['Chip']):
                        data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property)
                        if data_final is not None:
                            wafermap_P1.img_ind(data_final, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), clim=get_range(data_final))
                    wafermap_P1.show_cbar(title='Field')    
                    
                    fig_new.savefig(plot_dir)
                    plt.close(fig_new)
                
                plots_record.loc[wafer, plot_property] = plot_dir

    return plots_record
        

def generate_wafermap_smrtcellcentral(wafer_df, properties = ['P1_prod']):
    if os.name == 'nt':
        save_folder = r'\\pbi\dept\chip\ChipEng\Liang\Field Data'
        plot_folder = r'\\pbi\dept\chip\ChipEng'
    else:
        save_folder = '//pbi/dept/chip/ChipEng/Liang/Field Data'
        plot_folder = '//pbi/dept/chip/ChipEng'
#    prod_type = wafer_list[0]
    wafer_list = list(wafer_df['Wafer'].unique())
    prod_type = get_prodtype(wafer_list[0])
    save_file = prod_type + r'.pkl'
    data_field = pd.read_pickle( os.path.join(save_folder, save_file) )
    data_field.dropna(subset=['Wafer'], inplace=True)
    data_field['Wafer'] = data_field['Wafer'].apply(lambda x:x[:10])
    
#    wafer_df['group'] = wafer_df['Owner'] + ': ' +  wafer_df['Project'] 
    wafer_df['group'] = wafer_df.apply(lambda x: 'EOLQC' if x['Owner']=='Sample' else x['Owner'] + ': ' +x['Project'], axis=1)  
    
    plots_record = pd.DataFrame(columns = properties, index = wafer_list)
    for wafer in wafer_list:  #wafer_list:
        data_movie_local = get_EOLQC_chips(wafer)
        data_movie_local['MovieContext'] = data_movie_local['moviename']
#        data_movie_eolqc = data_movie_local[ data_movie_local['type']=='EOLQC' ]
#        data_movie_eng = data_movie_local[ data_movie_local['type']=='Eng Run' ]
#        get_data_sql =  ''' select containername as Chip,DataName,DataValue as MovieContext, TxnDate from 
#            [insitedb_schema].[PBI_ParametricData] where containername like'{}%' and DataName='moviename' '''.format(wafer)
#        data_movie_eolqc = pd.read_sql(get_data_sql, conn1)
        
        data_field_sel = data_field[data_field['Wafer']==wafer]
        #data_field_sel = data_field[data_field['Cell_Lot']==cell_lot]
        #wafer = data_field_sel['Wafer'].iloc[0]
        if len(data_field_sel)>=1 or len(data_movie_local)>=1:
            
            for plot_property in properties:
                plot_dir = os.path.join(plot_folder,'P1-Map', wafer + ' - ' + plot_property +'.png')   #folder_directory + r'\\'+wafer + ' - ' + plot_property +'.png'
#                if not os.path.isfile(plot_dir):
                wafermap_P1 = WaferMap()
                wafermap_P1.set_cm(prod_type)
                fig_new = wafermap_P1.create_wafer_map(title= plot_property + ': ' + wafer)
                
                patches = []
                for idx, group in enumerate(sorted(list(wafer_df['group'].unique()), reverse=True)):
                    data_movie_group = wafer_df[ wafer_df['group']==group ]
                    for movie_i,chip_i in zip(data_movie_group['Movie'], data_movie_group['Chip']):  
                        data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property, isEOLQC=True)
                        if data_final is not None:
                            data_final[0][0] = 100
                            wafermap_P1.img_ind(data_final, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]) , filter_img='Median')  #clim=get_range(data_final)
#                            wafermap_P1.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]))
                        #print(chip_i)
                        wafermap_P1.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), color = color_list[idx])
#                        wafermap_P1.show_cbar(pos=[0.1, 0.7, 0.02, 0.2], title='EOLQC')
                    patches.append(mpatches.Patch(color=color_list[idx], label=group))
                fig_new.get_axes()[110].legend(handles=patches, loc='upper left',  bbox_to_anchor=(0, 0))  #'upper left'
                fig_new.get_axes()[110].set_zorder(1)
#                    fig_new.get_axes()[2].set_zorder(-1)
#                    fig_new.get_axes()[11].set_zorder(-1)
#                    fig_new.get_axes()[12].set_zorder(-1)
#                    for movie_i,chip_i in zip(data_movie_eolqc['MovieContext'], data_movie_eolqc['Chip']):        
#                        data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property, isEOLQC=True)
#                        if data_final is not None:
#                            wafermap_P1.img_ind(data_final, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), clim=get_range(data_final))
#                        wafermap_P1.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]))
#                    wafermap_P1.show_cbar(pos=[0.1, 0.7, 0.02, 0.2], title='EOLQC')
#                    
#                    for movie_i,chip_i in zip(data_movie_eng['MovieContext'], data_movie_eng['Chip']):        
#                        data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property, isEOLQC=True)
#                        if data_final is not None:
#                            wafermap_P1.img_ind(data_final, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), clim=get_range(data_final))
#                        wafermap_P1.show_boarder(int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), color = 'blue')
#    #                wafermap_P1.show_cbar(pos=[0.1, 0.7, 0.02, 0.2], title='Engineer_Run')
                    
                for movie_i,chip_i in zip(data_field_sel['MovieContext'], data_field_sel['Chip']):
                    data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property)
                    if data_final is not None:
                        wafermap_P1.img_ind(data_final, int(chip_i.split('-')[2][0:2]), int(chip_i.split('-')[2][2:4]), clim=get_range(data_final), filter_img='Median')
                wafermap_P1.show_cbar(title='ColorMap')   
                
#                    print('test')
#                    plt.show()
#                    fig_new.show()
#                print(plot_dir)
                fig_new.savefig(plot_dir, dpi=600)
                plt.close(fig_new)

                
                plots_record.loc[wafer, plot_property] = plot_dir

    return plots_record


def save_chipmap_field(wafer_list, properties = ['P1_prod']):
    if os.name == 'nt':
        save_folder = r'\\pbi\dept\chip\ChipEng\Liang\Field Data'
        plot_folder = r'\\pbi\dept\chip\ChipEng'
    else:
        save_folder = '//pbi/dept/chip/ChipEng/Liang/Field Data'
        plot_folder = '//pbi/dept/chip/ChipEng'
#    prod_type = wafer_list[0]
    prod_type = get_prodtype(wafer_list[0])
    save_file = prod_type + r'.pkl'
    data_field = pd.read_pickle( os.path.join(save_folder, save_file) )
    print(prod_type)
    for wafer in wafer_list:  #wafer_list:
        data_field_sel = data_field[data_field['Wafer']==wafer]
        if len(data_field_sel)>=1 :
            for plot_property in properties:
                for movie_i,chip_i in zip(data_field_sel['MovieContext'], data_field_sel['Chip']):
                    data_final = get_movie_rsts(movie_i,chip_i, prod_type, plot_property)
                    if data_final is not None:
                        data_final.dump(os.path.join(plot_folder, 'Liang','FA','Field_Chipmap',chip_i + '.pkl'))
                      #  data_final.to_pickle(os.path.join(plot_folder, 'Liang','FA','Field_Chipmap',chip_i + '.pkl'))

if __name__ == '__main__':
#    print(generate_wafermap(['PV44120-24'])   )     
#    generate_wafermap_smrtcellcentral(wafer_df, properties = ['P1_prod'])
    save_chipmap_field(wafer_list = ['PU40314-16','PU40330-22','PU43243-15','PU43244-04','PU43244-13','PU43244-22'] )   #['PV43001-06', 'PV42026-13','PV42024-08']
    