# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 16:43:20 2019

@author: ltu
"""

#Virtual Path:	Physical Path:
#usmp-vm-sequelwebapp/VQC1_sequel	\\pbi\dept\chip\ChipMfgQC\WipData\VqcData\SequEl\VQC1
#usmp-vm-sequelwebapp/VQC2_sequel	\\pbi\dept\chip\ChipMfgQC\WipData\VqcData\SequEl\VQC2
#usmp-vm-sequelwebapp/VQC1_spider	\\pbi\dept\chip\ChipMfgQC\SpiderWipData\VQCData\Spider\VQC1
#usmp-vm-sequelwebapp/VQC2_spider	\\pbi\dept\chip\ChipMfgQC\SpiderWipData\VQCData\Spider\VQC2


from PIL import Image
import os
#folder = r'\\pbi\dept\chip\ChipMfgQC\SpiderWipData\VQCData\Spider\VQC1\WAW29027-01_PVQC1_SM6_D20181207'

folders = [r'\\pbi\dept\chip\ChipMfgQC\WipData\VqcData\SequEl\VQC1', r'\\pbi\dept\chip\ChipMfgQC\WipData\VqcData\SequEl\VQC2',
           r'\\pbi\dept\chip\ChipMfgQC\SpiderWipData\VQCData\Spider\VQC1',r'\\pbi\dept\chip\ChipMfgQC\SpiderWipData\VQCData\Spider\VQC2']
folder = folders[0]
for folder in folders:
    for directory in os.listdir(folder): 
        dir_temp = os.path.join(folder, directory) #.replace('\\','/')
        if os.path.isdir(dir_temp):
            for file in os.listdir(dir_temp):
                file_temp = os.path.join(dir_temp, file)
                if (os.path.isfile(file_temp)) and (file_temp[-4:]=='.tif'):
                    im = Image.open(file_temp)
                    file_temp_bmp = file_temp.replace('.tif','.bmp')
                    if os.path.isfile(file_temp_bmp):
                        im.save(file_temp_bmp)
                 #   print(file_temp.replace('.tif','.bmp'))