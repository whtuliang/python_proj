# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 12:07:04 2019

@author: ltu
"""

import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
import plotly.tools as tls
import os
import util_liang.spc_util as spc_util
from dash.dependencies import Input, Output

app = dash.Dash()
colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}

folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Yield\Data'.replace('\\', '/')
data_name = 'all_chip_level_data.csv'
data_raw = pd.read_csv(os.path.join(folder_path, data_name))

date_max, data_ww = spc_util.get_FE_maxWW(data_raw)
# del data_raw
prod_list = ['Sequel', 'Spider']

prod = prod_list[1]
data_sub_finished = spc_util.summary_FE_failure(data_ww, prod)
PBI_Lot_list = data_sub_finished['PBI_Lot'].unique()
df_all = spc_util.pivot_FE_failure(data_sub_finished)
df_lot_list = [spc_util.pivot_FE_failure(data_sub_finished, lot) for lot in PBI_Lot_list]

app.layout = html.Div([

    html.Div(dcc.RadioItems(
        id='radio-items',
        options=[
            {'label': 'Sequel', 'value': 'Sequel'},
            {'label': 'Spider', 'value': 'Spider'}
        ],
        value='Sequel'
    )),

    html.Div(html.H2('Plot in Current Workweek')),

    html.Div(dcc.Graph(
        id='bar-chart',
        figure={
            'data': [go.Bar(name=i, x=df_all.index, y=df_all[i]) for i in df_all.columns],
            'layout': {
                'barmode': 'stack',
                'title': prod + ': FE Status'
            }
        }
    )),

    html.Div(html.H2('Plots By Each Lot')),

    html.Div(
        [dcc.Graph(id=PBI_Lot_list[idx],
                   figure={
                       #             'id':  PBI_Lot_list[idx],
                       'data': [go.Bar(name=i, x=df.index, y=df[i]) for i in df.columns],
                       'layout': {
                           'barmode': 'stack',
                           'title': prod + ': FE Status @ Lot: ' + PBI_Lot_list[idx]
                       }
                   }

                   ) for idx, df in enumerate(df_lot_list)], id='bar-charts'),
    #    html.Div( dcc.Graph(
    #            id =  'bar-charts')
    #        ),

    #    html.Div(id='hidden-div', style={'display':'none'})
])


@app.callback(Output('bar-chart', 'figure'), [Input('radio-items', 'value')])
def render_chart(value):
    prod = value
    data_sub_finished = spc_util.summary_FE_failure(data_ww, prod)
    df_all = spc_util.pivot_FE_failure(data_sub_finished)
    return {'data': [go.Bar(name=i, x=df_all.index, y=df_all[i]) for i in df_all.columns],
            'layout': {
                'barmode': 'stack',
                'title': prod + ': FE Status'
            }
            }


@app.callback(Output('bar-charts', 'children'), [Input('radio-items', 'value')])
def render_charts(value):
    prod = value
    data_sub_finished = spc_util.summary_FE_failure(data_ww, prod)
    PBI_Lot_list = data_sub_finished['PBI_Lot'].unique()
    df_lot_list = [spc_util.pivot_FE_failure(data_sub_finished, lot) for lot in PBI_Lot_list]

    graph_alt = html.Div(
        [dcc.Graph(
            id=PBI_Lot_list[idx],
            figure={
                'data': [
                    go.Bar(name=i, x=df.index, y=df[i])
                    for i in df.columns],
                'layout': {
                    'barmode': 'stack',
                    'title': prod + ': FE Status @ Lot: ' + PBI_Lot_list[idx]
                }
            }

        ) for idx, df in enumerate(df_lot_list)])
    return graph_alt


if __name__ == '__main__':
    # app.run_server(debug=False, use_reloader=False, port=8060, host='0.0.0.0')  #localhost:8080  127.0.0.1
    app.run_server(debug=False, use_reloader=False, port=8060)
