# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 14:33:45 2020

@author: ltu
"""


import dash
from dash.exceptions import PreventUpdate
import dash_html_components as html
import dash_core_components as dcc
import os, sys, time
#import webbrowser
import pickle
if os.name == 'nt':
    folder_dir = r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib'
    folder_output = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
else:
    folder_dir = '//pbi/dept/chip/ChipEng/Liang/Python-Lib'
    folder_output = '//pbi/dept/chip/ChipEng/EOLQC-Report'
    
#sys.path.insert(0, folder_dir)
#from util_liang.Trie import TrieEOLQC 
dir_path = os.path.dirname(os.path.realpath(__file__))
package_path = os.path.dirname(dir_path)
if package_path not in os.sys.path:
    os.sys.path.append( package_path)
from util_liang.Trie import TrieEOLQC

######
## TODO: rename one of these files to local_config.py
##configs/windows_config.py
#folder_dir = r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\EOLQC_Report'
#folder_output = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
##configs/linux_config.py
#folder_dir = r'//pbi/dept/chip/ChipEng/Liang/Python-Lib/EOLQC_Report'
#folder_output = r'//pbi/dept/chip/ChipEng/EOLQC-Report'
######
#
##### this file
#from config.local_config import folder_dir, folder_ouput
#####

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
####

####
    
filehandler = open(os.path.join(folder_dir, 'EOLQC_Report', 'trie_EOLQC.obj'), 'rb') 
trie_obj = pickle.load(filehandler)
filehandler.close()

#folder_dir = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
#listOfFolders = [f for f in os.listdir(folder_dir) if not os.path.isfile(f)]
#options = []
#for folder in listOfFolders:
#    folder_path = os.path.join(folder_dir, folder)
#    listOfFiles = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path,f))]
#    for file in listOfFiles:
#        file_path = os.path.join(folder_path, file)
#        options.append({"label": file.split('.')[0], "value":file, "ts":os.path.getmtime(file_path)})
#
#options.sort(key = lambda x: x["ts"], reverse = True)
def struct_time_to_str(o):
    result = str(o["ts"].tm_year)+'-'+str(o["ts"].tm_mon)+'-'+str(o["ts"].tm_mday) + ' ' + str(o["ts"].tm_hour) + ':' + str(o["ts"].tm_min) + ':' + str(o["ts"].tm_sec)
    return result
#trie_obj.sort()
#options = [{"label": o["value"].split('.')[0] + '  (' + struct_time_to_str(o) + ')', "value":o["value"]} for o in trie_obj.get('')]
options = [{"label": o["value"]               + '  (' + struct_time_to_str(o) + ')', "value":o["value"]} for o in trie_obj.get('')]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

def dropdown_layout():
#__________the script below generate file list trie_obj everytime you refresh brower_______
#    t = TrieEOLQC()
#        
#    if os.name == 'nt':
#      folder1_dir = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
#      save1_dir = r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\EOLQC_Report'
#    else:
#      folder1_dir = '//pbi/dept/chip/ChipEng/EOLQC-Report'
#      save1_dir = '//pbi/dept/chip/ChipEng/Liang/Python-Lib/EOLQC_Report'
#      
#    listOfFolders = [f for f in os.listdir(folder1_dir) if not os.path.isfile(f)]
#    for folder in listOfFolders:
#        folder_path = os.path.join(folder1_dir, folder)
#        listOfFiles = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path,f))]
#        for file in listOfFiles:
#            file_path = os.path.join(folder_path, file)
#            t.insert(file, time.localtime(os.path.getmtime(file_path)))
#    t.sort()
#    file_trie = open(os.path.join(save1_dir, 'trie_EOLQC.obj'), 'wb') 
#    pickle.dump(t, file_trie)
#    file_trie.close()
#    options = [{"label": o["value"] + '  (' + struct_time_to_str(o) + ')', "value":o["value"]} for o in t.get('')]
#_________________________________________________________________________________________  
    filehandler = open(os.path.join(folder_dir, 'EOLQC_Report', 'trie_EOLQC.obj'), 'rb') 
    trie_obj = pickle.load(filehandler)
    filehandler.close()
    options = [{"label": o["value"] + '  (' + struct_time_to_str(o) + ')', "value":o["value"]} for o in trie_obj.get('')]

    
    return html.Div([
        #html.Button('Update EOLQC Chips', id='button_reset'),
        html.Label(["Chip EOLQC Report", dcc.Dropdown(id="my-dynamic-dropdown", options = options, value = '', placeholder="Select a chip" )]),
        html.Button('Generate EOLQC Report', id='button'),
        html.Div(id='page-content'),
        html.Br(),html.Br(),
        html.Br(),html.Br(),
        html.Br(),html.Br(),
        html.Label("Multiple Reports"),
        html.Label([ dcc.Textarea(id="my-input", value = 'AW33040-16, AW33040-20', placeholder="Enter wafers ..." , style={'width': '100%'})]),
        html.Button('Generate EOLQC Reports', id='button1'),
        html.Div(id='page-content1'),
        html.Br(),html.Br(),
        html.Br(),html.Br(),
        html.Br(),html.Br(),
        html.A(html.Label('you must enable local file links in browser', className='three columns'), href ='https://chrome.google.com/webstore/detail/enable-local-file-links/nikfmfgobenbhmocjaaboihbeocackld')
        ]
)


app.layout = dropdown_layout   #Use function instance can update on Page Load. Otherwise, app.layout is only computed once.

  
#@app.callback( dash.dependencies.Output("my-dynamic-dropdown", "options"), [dash.dependencies.Input("my-dynamic-dropdown", "value")] )
#def update_options(search_value):
#    if not search_value:
#        raise PreventUpdate
#    elif len(search_value)>=10:
#        return options
#    return trie_obj.get(search_value)
#    return [o for o in options if search_value in o["label"]]

#@app.callback( dash.dependencies.Output('page-content', 'children'), [dash.dependencies.Input("my-dynamic-dropdown", "value")] )
#def gen_link(search_value):
#    if not search_value:
#        raise PreventUpdate
#    else:
#        if len(search_value)==10:
#            link = os.path.join(folder_output, search_value[:7], search_value)
#            return html.A(html.Button('Click to Open Report', className='three columns'), href ='file:///'+link)  
#        else:
#            return html.A(html.Button('pending', className='three columns'))
   
#@app.callback( dash.dependencies.Output("my-dynamic-dropdown", "options") , [dash.dependencies.Input('button_reset', 'n_clicks')])
#def resetOption(n_clicks):
#    if n_clicks:
#        t = TrieEOLQC()
#        
#        if os.name == 'nt':
#          folder1_dir = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
#          save1_dir = r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\EOLQC_Report'
#        else:
#          folder1_dir = '//pbi/dept/chip/ChipEng/EOLQC-Report'
#          save1_dir = '//pbi/dept/chip/ChipEng/Liang/Python-Lib/EOLQC_Report'
#          
#        listOfFolders = [f for f in os.listdir(folder1_dir) if not os.path.isfile(f)]
#        #options = []
#        for folder in listOfFolders:
#            folder_path = os.path.join(folder1_dir, folder)
#            listOfFiles = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path,f))]
#            for file in listOfFiles:
#                file_path = os.path.join(folder_path, file)
#                t.insert(file, time.localtime(os.path.getmtime(file_path)))
#        t.sort()
#        file_trie = open(os.path.join(save1_dir, 'trie_EOLQC.obj'), 'wb') 
#        pickle.dump(t, file_trie)
#        file_trie.close()
#        
#        options = [{"label": o["value"].split('.')[0] + '  (' + struct_time_to_str(o) + ')', "value":o["value"]} for o in t.get('')]
#        return options
        
        
#@app.callback( dash.dependencies.Output("my-dynamic-dropdown", "options") , [dash.dependencies.Input('button', 'n_clicks')], [dash.dependencies.State("my-dynamic-dropdown", "options")])
#def updateOption(n_clicks,  options):
#    if n_clicks:
#        if n_clicks==1:
#            filehandler = open(os.path.join(folder_dir, 'EOLQC_Report','trie_EOLQC.obj'), 'rb') 
#            trie_obj = pickle.load(filehandler)
#            filehandler.close()
#            #options = [{"label": '1', "value":'a'}, {"label": '2', "value":'b'}]
#            options = [{"label": o["value"].split('.')[0] + '  (' + struct_time_to_str(o) + ')', "value":o["value"]} for o in trie_obj.get('')]
#            return options
                
            
@app.callback(dash.dependencies.Output('page-content', 'children'), [dash.dependencies.Input('button', 'n_clicks')], [dash.dependencies.State('my-dynamic-dropdown', 'value')])
def plot(n_clicks, value):
    if n_clicks:
        if not value:
            raise PreventUpdate
        else:
            if len(value)==15 or len(value)==14:
                link = os.path.join(folder_output, value[:7], value)
                #link = (folder_dir + '\\' + value[:7] + '\\' +  value)
                #webbrowser.open_new_tab('file://' + r'\\pbi\dept\chip\ChipEng\EOLQC-Report\AX17033\AX17033-08.html')
                return html.A(html.Button('Click to Open '+value[:10], className='three columns'), href ='file:///'+link)  
            else:
                return html.A(html.Button('invalid length:' + str(len(value)) , className='three columns'))
  

@app.callback(dash.dependencies.Output('page-content1', 'children'), [dash.dependencies.Input('button1', 'n_clicks')], [dash.dependencies.State('my-input', 'value')])
def plot1(n_clicks, value):
    if n_clicks:
#        value = 'AW33040-16, AW33040-20'
        wafer_list = [a.strip() for a in value.split(',')]
        if len(wafer_list)==1:
            value = wafer_list[0]
            link_html = os.path.join(folder_output, value[:7], value+'.html')
            link_pdf = os.path.join(folder_output, value[:7], value+'.pdf')
            #link = (folder_dir + '\\' + value[:7] + '\\' +  value)
            #webbrowser.open_new_tab('file://' + r'\\pbi\dept\chip\ChipEng\EOLQC-Report\AX17033\AX17033-08.html')
            return [html.A(html.Button('Click to Open '+value[:10], className='three columns'), href ='file:///'+link_html),
                    html.A(html.Button('Click to Open '+value[:10], className='three columns'), href ='file:///'+link_pdf)]
                    
        elif len(wafer_list)>1:
            result = []
            for wafer in wafer_list:
                link = os.path.join(folder_output, wafer[:7], wafer+'.html')
                result.append(html.A(html.Button(wafer[:10]+'.html' , className='three columns'), href ='file:///'+link))
                link = os.path.join(folder_output, wafer[:7], wafer+'.pdf')
                result.append(html.A(html.Button(wafer[:10]+'.pdf' , className='three columns'), href ='file:///'+link))
            return result
        
        
#        webbrowser.open_new_tab('file://' + link)
#webbrowser.open_new_tab(r'//pbi/dept/chip/ChipEng/EOLQC-Report/AX17033/AX17033-08.html')


#@app.callback(
#    dash.dependencies.Output('output-container-button', 'children'),
#    [dash.dependencies.Input('button', 'n_clicks')],
#    [dash.dependencies.State('my-dynamic-dropdown', 'value')])
#def update_output(n_clicks, value):
#    return 'The input value was "{}" and the button has been clicked {} times'.format(
#        value,
#        n_clicks
#    )

if __name__ == "__main__":
    app.run_server(debug=False, port=8080, use_reloader=False)
    # app.run_server(debug=False, port=9002, use_reloader=False, host='0.0.0.0')