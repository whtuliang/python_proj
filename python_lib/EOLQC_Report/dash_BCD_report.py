# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 14:33:45 2020

@author: ltu
"""

import dash
from dash.exceptions import PreventUpdate
import dash_html_components as html
import dash_core_components as dcc
import os, sys, time
import pandas as pd
import numpy as np
import pyodbc
import matplotlib
import seaborn as sb
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn import preprocessing
import matplotlib.patheffects as path_effects
import plotly.express as px
import re

dir_path = os.path.dirname(os.path.realpath(__file__))
package_path = os.path.dirname(dir_path)
if package_path not in os.sys.path:
    os.sys.path.append(package_path)
from config.local_config import dir_pbi_liang
from SPC_Summary.FE_EOQC_FirstFailure import FE_Defects_Plot
from util_liang import database_conn

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# movie_serial = 'r64166_20201014_222407'

app.layout = html.Div([
    html.Plaintext(r'Anything before SP27: \\pbi\dept\chip\ChipEng\Liang\Others\InlineBcdFit\BCDfit_before_SP27'),
    html.Div(dcc.Input(id='wafer_id', type='text', size=20,
                       placeholder='AW35066-17')),
    html.Button('Submit', id='submit-val', n_clicks=0),
    html.Div(id='container-plot', children='...'),
    html.Div(children=[dcc.Graph(id='graph-output-raw', style={'display': 'inline-block'}),
                       dcc.Graph(id='graph-output-fit', style={'display': 'inline-block'})]),
    html.Div(id='textarea-log', style={'whiteSpace': 'pre-line'})
    # html.Div(id='intermediate-value', style={'display': 'none'})
])


@app.callback(
    dash.dependencies.Output('graph-output-raw', 'figure'),
    [dash.dependencies.Input('submit-val', 'n_clicks')],
    [dash.dependencies.State('wafer_id', 'value')])
def display_status(n_clicks, wafer):
    # conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')  # camstarsql_mes
    # cursor = conn.cursor()
    # conn.autocommit = True
    conn = database_conn.connect_db('SpiderMetrology')
    if wafer:
        wafer_name = wafer  # 'AW33042-'
    else:
        wafer_name = 'AW35066-17'
    if wafer_name[0] == 'P':
        product = 'Sequel'
    else:
        product = 'Spider'

    test_name = 'PBI_bcd_fit'

    get_data_sql = '''SELECT   [WaferName] AS [WaferID],    CONCAT([WaferName], '-',[ChipIndex]) AS [ChipID],   'Inline' AS [ParameterType],   
        CONCAT([DataName], '-', CAST(SUBSTRING([SiteName], 5, CHARINDEX('Of', [SiteName]) - 5) AS char)) AS [ParameterName],  
        CAST(SUBSTRING([SiteName],  CHARINDEX('Of', [SiteName]) + 2, DATALENGTH([SiteName])) AS char) as [TotalSite],
        CAST([DoubleValue] AS float) AS [ParameterValue],   'Float' AS [ParameterValueType] 
        FROM [SpiderMetrology].[dbo].[WaferData] AS [W1] 
        LEFT JOIN [SpiderMetrology].[dbo].[Wafer]   ON [W1].[WaferId] = [Wafer].[WaferId] 
        LEFT JOIN [SpiderMetrology].[dbo].[DataUploadSession] AS [D1]   ON [W1].[DataUploadSessionId] = [D1].[DataUploadSessionId] 
        LEFT JOIN [SpiderMetrology].[dbo].[DataName]   ON [W1].[DataNameId] = [DataName].[DataNameId] 
        LEFT JOIN [SpiderMetrology].[dbo].[Site]   ON [W1].[SiteId] = [Site].[SiteId] 
        INNER JOIN (
        SELECT   [WaferId],   [SiteId],   [DataNameId],   MAX([DataUploadTime]) AS 'MaxDataUploadTime' 
        FROM (
        	SELECT   [WaferId],   [SiteId],   [DataNameId],   [DataUploadTime] FROM [SpiderMetrology].[dbo].[WaferData] AS [W1] LEFT JOIN [SpiderMetrology].[dbo].[DataUploadSession] AS [D1]   ON [W1].[DataUploadSessionId] = [D1].[DataUploadSessionId]) 
        	AS [W2] 
        	GROUP BY [WaferId],          [SiteId],          [DataNameId]) 
        	AS [W2]   
        ON ([W1].[WaferId] = [W2].[WaferId]   AND [W1].[SiteId] = [W2].[SiteId]   AND [W1].[DataNameId] = [W2].[DataNameId]   AND [D1].[DataUploadTime] = [W2].[MaxDataUploadTime]) 
        WHERE [DataName] LIKE 'SLT_spider_ezmw_alo_etch_act_middle_cd%%' AND [WaferName] LIKE '{0}%' 
        ---AND [ParameterName] like 'SLT_spider_ezmw_alo_etch_act_middle_cd%' '''.format(wafer_name)
    data_raw = pd.read_sql(get_data_sql, conn)

    # abc = cursor.execute(get_data_sql).fetchall()
    data_raw = pd.read_sql(get_data_sql, conn)
    data_raw['ChipIndex'] = data_raw['ChipID'].apply(lambda x: int(x[-4:]))
    data_raw['DataValue'] = data_raw['ParameterValue']
    data_all = pd.DataFrame(columns=['Wafer', 'Row', 'Col', 'Test', 'Value'])
    wafer_list = data_raw['WaferID'].unique()

    for wafer in wafer_list:
        data = data_raw[data_raw.WaferID == wafer]
        # data = data.loc[~data['ChipIndex'].duplicated(keep='last')]
        # data['DataValue'] = data[['ChipIndex', 'DataValue']].groupby(['ChipIndex'])['DataValue'].transform(
        #     lambda x: '+'.join(x))
        data = data[['ChipIndex', 'DataValue']].drop_duplicates()

        grade_ind = {'A': [85, 115]}

        for line in data.iterrows():
            r_c = line[1].ChipIndex
            col = r_c // 100  # - 50
            row = - (r_c % 100)  # - 50
            val_raw = line[1].DataValue
            val = 0  # 'A'

            if 'A' in grade_ind.keys():  # for EOQC_first_failure
                if val_raw not in grade_ind['A']:
                    val = float(val_raw) * 1e9

            data_chip = pd.Series({'Wafer': wafer, 'Row': row, 'Col': col, 'Test': 'PBI_bcd_fit', 'Value': val})
            data_all = data_all.append(data_chip, ignore_index=True)

        data_all.set_index(['Row', 'Col'], inplace=True)
        center = data_all['Value'].mean()
        sigma = data_all['Value'].std()
        data_map = data_all['Value'].unstack(level=1)
        mask = data_map.isnull()

        fig = px.imshow(data_map, zmin=center - 3 * sigma, zmax=center + 3 * sigma,
                        color_continuous_scale='jet',
                        labels=dict(x="", y="", color="BCD_Inline"),
                        width=500, height=500, origin='upper',
                        )
        # fig.update_xaxes(side="top")

    return fig


@app.callback(
    dash.dependencies.Output('graph-output-fit', 'figure'),
    [dash.dependencies.Input('submit-val', 'n_clicks')],
    [dash.dependencies.State('wafer_id', 'value')])
def display_status(n_clicks, wafer):
    # conn = pyodbc.connect('DSN=spidermetrology;UID=odbc;PWD=readonly')  # camstarsql_mes
    # cursor = conn.cursor()
    # conn.autocommit = True
    conn = database_conn.connect_db('SpiderMetrology')
    if wafer:
        wafer_name = wafer  # 'AW33042-'
    else:
        wafer_name = 'AW35066-17'
    if wafer_name[0] == 'P':
        product = 'Sequel'
    else:
        product = 'Spider'

    test_name = 'PBI_bcd_fit'

    get_data_sql = '''
        SET NOCOUNT ON
        DECLARE @LotName AS NVARCHAR(128)
        SET @LotName = '{0}%'

        DECLARE @RawTable TABLE (
               WaferName NVARCHAR(255),
               BaeWaferName NVARCHAR(255),
               ChipIndex INT,
               SubstrateId CHAR(8),
               DataName NVARCHAR(255),
               DataValue NVARCHAR(255),
               Unit NVARCHAR(255),
               DataUploadTime DATETIME,
               DataCollectionTime DATETIME,
               [FileName] NVARCHAR(255),
               [Description] NVARCHAR(255)
               )



        INSERT INTO @RawTable 
               (WaferName, BaeWaferName, ChipIndex,SubstrateId,DataName,DataValue,Unit,DataUploadTime,
                      DataCollectionTime,
                      [Description])
               SELECT 
                              WaferName,
                              BaeWaferName,
                              ChipIndex,
                              {1}Metrology.dbo.ConvertSubstrateIdToString(SubstrateId),
                              DataName,
                              DataValue,
                              Unit,
                              DataUploadTime,
                              DataCollectionTime,
                              [Description]
                      FROM (SELECT ChipId, DataUploadSessionId, DataNameId, CAST(ChipData.DataValue as NVARCHAR(255)) as DataValue
                            FROM [{1}Metrology].[dbo].[ChipData] ) ChipData
                              JOIN [{1}Metrology].[dbo].[Chip] Chip ON ChipData.ChipId = Chip.ChipId
                              JOIN [{1}Metrology].[dbo].[Wafer] Wafer ON Chip.WaferId = Wafer.WaferId
                              JOIN [{1}Metrology].[dbo].[DataName] DataName ON ChipData.DataNameId = DataName.DataNameId
                              JOIN [{1}Metrology].[dbo].[DataUploadSession] DataUploadSession ON ChipData.DataUploadSessionId = DataUploadSession.DataUploadSessionId
                      WHERE Wafer.WaferName LIKE @LotName
                      AND DataName LIKE '%{2}%';


        SELECT 
               rt.WaferName,
               BaeWaferName,
               rt.ChipIndex,
               SubstrateId,
               rt.DataName,
               DataValue,
               Unit,
               rt.DataUploadTime,
               DataCollectionTime,
               [Description]
        FROM @RawTable rt
        INNER JOIN (
                      select
                              WaferName,
                              ChipIndex,
                              DataName,
                              max(DataUploadTime) as DataUploadTime
                      from @RawTable group by WaferName,ChipIndex,DataName
                 ) sd
        ON
               rt.WaferName = sd.WaferName AND
               rt.ChipIndex = sd.ChipIndex AND
               rt.DataName = sd.DataName AND
               rt.DataUploadTime = sd.DataUploadTime
               order by SubstrateId'''.format(wafer_name, product, test_name)  # wafer_name

    # abc = cursor.execute(get_data_sql).fetchall()
    data_raw = pd.read_sql(get_data_sql, conn)
    data_all = pd.DataFrame(columns=['Wafer', 'Row', 'Col', 'Test', 'Value'])
    wafer_list = data_raw['WaferName'].unique()

    for wafer in wafer_list:
        data = data_raw[data_raw.WaferName == wafer]
        # data = data.loc[~data['ChipIndex'].duplicated(keep='last')]
        data['DataValue'] = data[['ChipIndex', 'DataValue']].groupby(['ChipIndex'])['DataValue'].transform(
            lambda x: '+'.join(x))
        data = data[['ChipIndex', 'DataValue']].drop_duplicates()

        grade_ind = {'A': [85, 115]}

        for line in data.iterrows():
            r_c = line[1].ChipIndex
            col = r_c // 100  # - 50
            row = -(r_c % 100)  # - 50
            val_raw = line[1].DataValue
            val = 0  # 'A'

            if 'A' in grade_ind.keys():  # for EOQC_first_failure
                if val_raw not in grade_ind['A']:
                    val = float(val_raw) * 1e9

            data_chip = pd.Series({'Wafer': wafer, 'Row': row, 'Col': col, 'Test': 'PBI_bcd_fit', 'Value': val})
            data_all = data_all.append(data_chip, ignore_index=True)

        data_all.set_index(['Row', 'Col'], inplace=True)
        # fig, ax = plt.subplots(1, 1)
        center = data_all['Value'].mean()
        sigma = data_all['Value'].std()
        # data_all = data_all.loc[~data_all.index.duplicated(keep='last')]
        # data_all['Value'] = data_all.groupby(data_all.index)['Value'].sum()
        data_map = data_all['Value'].unstack(level=1)
        mask = data_map.isnull()
        # data_map.fillna(-100, downcast=False, inplace=True)
        # data_map = data_map.astype(float)

        # data_map = data_map.astype(int)
        # sb.heatmap(data_map, vmax=center + 3 * sigma, vmin=center - 3 * sigma, cmap="jet",
        #            ax=ax, mask=mask.values, annot=True, fmt="d")
        #
        # plt.title(wafer_name + " : BCD_Fit " )

        # import base64
        # import io
        # pic_IObytes = io.BytesIO()
        # plt.savefig(pic_IObytes, format='png')
        # pic_IObytes.seek(0)
        # pic_hash = base64.b64encode(pic_IObytes.read())

        fig = px.imshow(data_map, zmin=center - 3 * sigma, zmax=center + 3 * sigma,
                        color_continuous_scale='jet',
                        labels=dict(x="", y="", color="BCD_Fit"),
                        width=600, height=600, origin='upper',
                        )
        # fig.update_xaxes(side="top")

    return fig


@app.callback(
    dash.dependencies.Output('textarea-log', 'children'),
    [dash.dependencies.Input('submit-val', 'n_clicks')],
    [dash.dependencies.State('wafer_id', 'value')])
def display_status(n_clicks, wafer):
    log_dir = r'\\pbi\dept\chip\ChipMfgQC\SpiderWipData\PBI_Fit\Log'
    if os.name !='nt':
        log_dir = log_dir.replace('\\','/')

    log_str = 'log not found'
    for root, dirs, files in os.walk(log_dir):
        for file in files:
            if (file[:10] == wafer) and (file[-4:]=='.txt'):
                with open(os.path.join(log_dir, file), 'r') as f:
                    log_str = f.read()
                break

    return log_str





if __name__ == "__main__":
    # app.run_server(debug=False, port=8080, use_reloader=False)
    app.run_server(debug=False, port=9005, use_reloader=False, host='0.0.0.0')
