# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 14:33:45 2020

@author: ltu
"""

import dash
from dash.exceptions import PreventUpdate
import dash_html_components as html
import dash_core_components as dcc
import os, sys, time
# import webbrowser
import pickle
# from config.local_config import dir_pbi_liang
import glob as glob

dir_path = os.path.dirname(os.path.realpath(__file__))
package_path = os.path.dirname(dir_path)
if package_path not in os.sys.path:
    os.sys.path.append(package_path)
from config.local_config import dir_pbi_liang

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# movie_serial = 'r64166_20201014_222407'

app.layout = html.Div([
    html.Div(dcc.Input(id='input-on-submit', type='text', size = 80,
                       placeholder='//pbi/collections/319/3190055/r64166_20201014_222407')),
    html.Button('Submit', id='submit-val', n_clicks=0),
    html.Div(id='container-button-summary', children='Enter a dir and press submit'),
    html.Div(id='container-plot', children='...'),
    html.Div(id='intermediate-value', style={'display': 'none'})
])


@app.callback(dash.dependencies.Output('intermediate-value', 'children'), [dash.dependencies.Input('input-on-submit', 'value')])
def clean_data(value):
    if value:
        try:
            value = value.replace('\\', '/')
            movie_serial = value.split('/')[6]
        except:
            return 'wrong directory'
    else:
        movie_serial = ''
    return movie_serial

@app.callback(
    dash.dependencies.Output('container-button-summary', 'children'),
    [dash.dependencies.Input('submit-val', 'n_clicks')],
    [dash.dependencies.State('input-on-submit', 'value')])
def display_status(n_clicks, value):
    if value:
        try:
            value = value.replace('\\', '/')
            files_h5 = sorted(glob.glob(os.path.join(value, '*loading_20*')))
            movie_serial = value.split('/')[6]
        except:
            return 'wrong directory'

        with open(os.path.join(dir_pbi_liang, 'EOLQC', 'Biotin_Count', 'biotin_files_dir.pkl'), "wb") as f:
            pickle.dump(value, f)

        file_des = os.path.join(dir_pbi_liang, 'EOLQC', 'Biotin_Count', 'biotin_' + movie_serial + '.html')
        if not os.path.exists(file_des):
            os.system(
                '/opt/rh/rh-python36/root/usr/bin/jupyter nbconvert --ExecutePreprocessor.timeout=600 --execute {:s} --to html --TemplateExporter.exclude_input=True --output {:s}'.format(
                    '/home/ltu/Python/python_proj/NB_biotin_count/Scripts/NewBtCount_101920.ipynb', file_des))
    else:
        movie_serial = ''

    return 'generating ' + movie_serial  # html.P(['Files found:', html.Br(), files_h5])


@app.callback(
    dash.dependencies.Output('container-plot', 'children'),
    [dash.dependencies.Input('intermediate-value', 'children')])
def display_status(movie_serial_new):
    file_des = r'\\pbi\dept\chip\ChipEng\Liang\EOLQC\Biotin_Count' + r'\biotin_' + movie_serial_new + '.html'
    # if not os.path.exists(file_des):
    #     os.system(
    #         '/opt/rh/rh-python36/root/usr/bin/jupyter nbconvert --ExecutePreprocessor.timeout=600 --execute {:s} --to html --TemplateExporter.exclude_input=True --output {:s}'.format(
    #             '/home/ltu/Python/python_proj/NB_biotin_count/Scripts/NewBtCount_101920.ipynb', os.path.join(dir_pbi_liang, 'EOLQC', 'Biotin_Count', 'biotin_'+movie_serial + '.html')))
    return html.A(html.Button('Wait & Click to Open', className='three columns'), href='file:///' + file_des)


if __name__ == "__main__":
    # app.run_server(debug=False, port=8080, use_reloader=False)
    app.run_server(debug=False, port=9003, use_reloader=False, host='0.0.0.0')
