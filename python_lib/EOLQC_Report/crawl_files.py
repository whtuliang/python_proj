# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 10:24:40 2020

@author: ltu
"""

import os
import time
import pickle 
from util_liang.Trie import TrieEOLQC 
#class TrieNode:
#    def __init__(self):
#        self.children = defaultdict(TrieNode)
#        self.val = []
#        
#class Trie:
#    def __init__(self):
#        self.root = TrieNode()
#        
#    def insert(self, chip, ts):
#        node = self.root
#        for c in chip:
#            node.val.append({"label":chip, "ts":ts})
#            node = node.children[c]
#        node.val.append({"label":chip, "ts":ts})
#            
#    def get(self, chip):
#        node = self.root
#        for c in chip:
#            if c in node.children:
#                node = node.children[c]
#            else:
#                return []
#        return node.val
#            
#    def sort(self):
#        def dfs(node):
#            if not node:
#                return
#            node.val.sort(key = lambda x: x["ts"], reverse = True)
#            for k,v in node.children.items():
#                dfs(v)
#        dfs(self.root)
                
t = TrieEOLQC()
#t.insert('abc',1)
#t.insert('abc',2)
#t.insert('abc',3)   
#t.insert('abc',0)  
#t.get('abc')    
#t.sort()


if os.name == 'nt':
  folder_dir = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
  save_dir = r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\EOLQC_Report'
else:
  folder_dir = '//pbi/dept/chip/ChipEng/EOLQC-Report'
  save_dir = '//pbi/dept/chip/ChipEng/Liang/Python-Lib/EOLQC_Report'
  
listOfFolders = [f for f in os.listdir(folder_dir) if not os.path.isfile(f)]
#options = []
for folder in listOfFolders:
    folder_path = os.path.join(folder_dir, folder)
    listOfFiles = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path,f))]
    for file in listOfFiles:
        file_path = os.path.join(folder_path, file)
        t.insert(file, time.localtime(os.path.getmtime(file_path)))
#        options.append({"label": file.split('.')[0], "value":file, "ts":os.path.getmtime(file_path)})
t.sort()
#options.sort(key = lambda x: x["ts"], reverse = True)
#options = [{"label": o["label"], "value":o["value"]} for o in options]



file_trie = open(os.path.join(save_dir, 'trie_EOLQC.obj'), 'wb') 
pickle.dump(t, file_trie)
file_trie.close()
#filehandler = open('trie_EOLQC.obj', 'rb') 
#object = pickle.load(filehandler)