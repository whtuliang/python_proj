import dash
import dash_table
import pandas as pd
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_core_components as dcc
import plotly.graph_objs as go
import plotly.express as px
# from flask import send_file

df = pd.read_csv(r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_SCC.csv'.replace('\\', '/'))
df = df[['Alias', 'Wafer', 'Chip', 'Substrate', 'pdProd%', 'PkMidC', 'ReadLengthMean', 'LP1', 'LP2',
         'CustomerLot', 'Owner', 'Runcode', 'Project', 'BCD (nm)', 'GradeChip', 'Sample_Prep_Lot_Num']]
# 'DefectCode', 'DefectComments',
pd.options.display.float_format = '${:,.3f}'.format
# df['pdProd%'] = df['pdProd%'].map('{:,.3f}'.format)
# df['PkMidC'] = df['PkMidC'].map('{:,.3f}'.format)

app = dash.Dash(__name__)


def discrete_background_color_bins(df, n_bins=5, columns='all'):
    import colorlover
    bounds = [i * (1.0 / n_bins) for i in range(n_bins + 1)]
    if columns == 'all':
        if 'id' in df:
            df_numeric_columns = df.select_dtypes('number').drop(['id'], axis=1)
        else:
            df_numeric_columns = df.select_dtypes('number')
    else:
        df_numeric_columns = df[columns]
    df_max = df_numeric_columns.max().max()
    df_min = df_numeric_columns.min().min()
    ranges = [
        ((df_max - df_min) * i) + df_min
        for i in bounds
    ]
    styles = []
    legend = []
    for i in range(1, len(bounds)):
        min_bound = ranges[i - 1]
        max_bound = ranges[i]
        backgroundColor = colorlover.scales[str(n_bins)]['seq']['Greens'][i - 1]
        color = 'white' if i > len(bounds) / 2. else 'inherit'

        for column in df_numeric_columns:
            styles.append({
                'if': {
                    'filter_query': (
                            '{{{column}}} >= {min_bound}' +
                            (' && {{{column}}} < {max_bound}' if (i < len(bounds) - 1) else '')
                    ).format(column=column, min_bound=min_bound, max_bound=max_bound),
                    'column_id': column
                },
                'backgroundColor': backgroundColor,
                'color': color
            })
        legend.append(
            html.Div(style={'display': 'inline-block', 'width': '60px'}, children=[
                html.Div(
                    style={
                        'backgroundColor': backgroundColor,
                        'borderLeft': '1px rgb(50, 50, 50) solid',
                        'height': '10px'
                    }
                ),
                html.Small(round(min_bound, 2), style={'paddingLeft': '2px'})
            ])
        )

    return styles, html.Div(legend, style={'padding': '5px 0 5px 0'})


(styles, legend) = discrete_background_color_bins(df, columns=['pdProd%'])

app.layout = html.Div([
    html.A(
        'Download EOLQC-Data',
        id='download-link1',
        download="EOLQC-Data.csv",
        href=r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_SCC.csv',
        target="_blank"
    ),
    html.Div(html.A(
        'Download EOLQC-RawData-2020',
        id='download-link2',
        download="EOLQC-Data-2020.csv",
        href=r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_2020.csv',
        target="_blank"
    )),
    html.Div(id='datatable-interactivity-container'),
    dash_table.DataTable(
        id='table',
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict('records'),
        style_cell={'textAlign': 'left', 'padding': '5px'},
        style_header={
            'backgroundColor': 'rgb(230, 230, 230)',
            'fontWeight': 'bold'
        },
        editable=True,
        filter_action="native",
        sort_action="native",
        sort_mode="multi",
        selected_rows=[],
        row_selectable="multi",
        page_size=20,
        page_action="native",

        style_cell_conditional=[
            {
                'if': {'column_id': c},
                'textAlign': 'left'
            } for c in ['Date', 'Region']
        ],
        style_data_conditional=[
                                   {
                                       'if': {'row_index': 'odd'},
                                       'backgroundColor': 'rgb(248, 248, 248)',
                                       'color': 'black'
                                   },
                                   {
                                       'if': {
                                           'filter_query': '{PkMidC} > 400',
                                           'column_id': 'PkMidC',
                                       },
                                       'backgroundColor': 'tomato',
                                       'color': 'white',
                                       'textDecoration': 'underline'
                                   }
                               ] + styles

    ),
    html.Div(dcc.Graph(
        id='scatter-chart',
        figure=px.scatter(df, x="Alias", y="pdProd%")
    # go.Figure(data=[go.Scatter(x=df.Alias, y=df['pdProd%'])])
    ))
])



# Graph: https://dash.plotly.com/datatable/interactivity
@app.callback(
    Output('datatable-interactivity', 'style_data_conditional'),
    [Input('datatable-interactivity', 'selected_rows')]
)
def update_styles(selected_columns):
    return [{
        'if': {'column_id': i},
        'background_color': '#D2F3FF'
    } for i in selected_columns]


@app.callback(
    Output('datatable-interactivity-container', "children"),
    [Input('datatable-interactivity', "derived_virtual_data"),
     Input('datatable-interactivity', "derived_virtual_selected_rows")])
def update_graphs(rows, derived_virtual_selected_rows):
    if derived_virtual_selected_rows is None:
        derived_virtual_selected_rows = []

    dff = df if rows is None else pd.DataFrame(rows)

    colors = ['#7FDBFF' if i in derived_virtual_selected_rows else '#0074D9'
              for i in range(len(dff))]

    return [
        dcc.Graph(
            id=column,
            figure={
                "data": [
                    {
                        "x": dff["Chip"],
                        "y": dff[column],
                        "type": "bar",
                        "marker": {"color": colors},
                    }
                ],
                "layout": {
                    "xaxis": {"automargin": True},
                    "yaxis": {
                        "automargin": True,
                        "title": {"text": column}
                    },
                    "height": 250,
                    "margin": {"t": 10, "l": 10, "r": 10},
                },
            },
        )
        for column in ["pdProd%", "PkMidC"] if column in dff
    ]


if __name__ == '__main__':
    app.run_server(debug=False)
