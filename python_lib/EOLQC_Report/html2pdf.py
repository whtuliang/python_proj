# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 14:49:50 2020

@author: ltu
"""

import pdfkit
config = pdfkit.configuration(wkhtmltopdf="C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe")

wafer = 'AX28101-04'
pdfkit.from_file(r'C:\Users\ltu\Documents\Python Scripts\jupyter_rconn_env\Scripts\test.html', 
                 r'C:\Users\ltu\Documents\Python Scripts\jupyter_rconn_env\Scripts\{0}.pdf'.format(wafer), configuration=config)
#pdfkit.from_file(r'\\pbi\dept\chip\ChipEng\EOLQC-Report\{0}\{1}.html'.format(wafer[:7], wafer), 
#                 r'C:\Users\ltu\Documents\Python Scripts\jupyter_rconn_env\Scripts\{0}.pdf'.format(wafer), configuration=config)