# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 14:25:34 2020

@author: ltu
"""

import os, sys
# import shutil
# import time
import numpy as np
import pandas as pd
from datetime import date, timedelta

dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if dir_path not in sys.path:
    sys.path.append(dir_path)


def gen_eolqc_data(argv):
    start_date = date.today() - timedelta(days=2)
    # today_str = '{0}-{1}-{2}'.format(today.year,today.month-1,today.day)
    date_str = start_date.strftime("%Y-%m-%d")
    from util_liang import sql_collections
    chip_list = sql_collections.get_EOLQC_chips(upload_date=date_str)
    df_eolqc = sql_collections.SCC_EOLQC(chip_list)

    # df_substrate = sql_collections.get_SCC_chip(df_eolqc['Chip'])
    # df_eolqc = df_eolqc.merge(df_substrate, left_on='Chip', right_on='Chip', how='left' )

    df_eolqc['pd_Productive'] = df_eolqc['pd_Productive'].astype(int)
    df_eolqc['pd_Empty'] = df_eolqc['pd_Empty'].astype(int)
    df_eolqc['pd_Other'] = df_eolqc['pd_Other'].astype(int)
    df_eolqc['pdProd%'] = df_eolqc['pd_Productive'] / (
                df_eolqc['pd_Productive'] + df_eolqc['pd_Empty'] + df_eolqc['pd_Other'])
    df_eolqc['pdOther%'] = df_eolqc['pd_Other'] / (
                df_eolqc['pd_Productive'] + df_eolqc['pd_Empty'] + df_eolqc['pd_Other'])
    df_eolqc['pdEmpty%'] = df_eolqc['pd_Empty'] / (
                df_eolqc['pd_Productive'] + df_eolqc['pd_Empty'] + df_eolqc['pd_Other'])

    from create_EOLQC_report import EOLQC_Report
    report = EOLQC_Report()
    df_chip_grade = report.get_chip_Grade(list(df_eolqc['Chip']))
    df_eolqc = df_eolqc.merge(df_chip_grade, left_on='Chip', right_on='Chip', how='left')

    df_chip_defect = report.get_chip_defect_owner_proj(list(df_eolqc['Chip']))
    df_eolqc = df_eolqc.merge(df_chip_defect, left_on='Chip', right_on='Chip', how='left')

    data_prep = report.get_Sample_Prep_Lot(list(df_eolqc['Chip']))
    df_eolqc = df_eolqc.merge(data_prep[['chip', 'Sample_Prep_Lot_Num']], left_on='Chip', right_on='chip', how='left')

    df_eolqc = df_eolqc.rename(
        columns={'wafer': 'Wafer', 'moviename': 'Movie', 'BCD_estimate': 'BCD (nm)', 'ALPStop_L1Power_mw': 'LP1',
                 'ALPStop_L2Power_mw': 'LP2', 'HQPkMidMedian_C': 'PkMidC'})  # 'Owner',  'Project'
    df_eolqc['pdProdMap'] = "http://usmp-vm-sequelwebapp/heatmaps/EOLQC/" + df_eolqc['Instrument'] + "/" + df_eolqc[
        'Movie'] + "." + df_eolqc['Substrate'] + ".P1.gif"
    df_eolqc['pdEmptyMap'] = "http://usmp-vm-sequelwebapp/heatmaps/EOLQC/" + df_eolqc['Instrument'] + "/" + df_eolqc[
        'Movie'] + "." + df_eolqc['Substrate'] + ".P0.gif"
    df_eolqc['pdOtherMap'] = "http://usmp-vm-sequelwebapp/heatmaps/EOLQC/" + df_eolqc['Instrument'] + "/" + df_eolqc[
        'Movie'] + "." + df_eolqc['Substrate'] + ".P2.gif"
    df_eolqc['Runcode'] = df_eolqc['sts_xml'].apply(lambda x: x.split('/')[4])
    df_eolqc['BCD (nm)'] = df_eolqc['BCD (nm)'].replace('', np.NaN)
    try:
        df_eolqc['LP1'] = df_eolqc['LP1'].astype(float)
        df_eolqc['LP2'] = df_eolqc['LP2'].astype(float)
        df_eolqc['PkMidC'] = df_eolqc['PkMidC'].astype(float)
        df_eolqc['BCD (nm)'] = df_eolqc['BCD (nm)'].astype(float)
        df_eolqc['Project'] = df_eolqc['Project'].astype(str)
    except:
        print('ValueError: could not convert data type')

    from util_liang.sql_collections import get_SA_Lot
    wafer_list = df_eolqc['Wafer'].unique().tolist()
    df_wafer_SA = get_SA_Lot(wafer_list)
    df_wafer_SA['SA_Lot'] = df_wafer_SA['SALot'].apply(lambda x: x[-4:])
    df_eolqc = df_eolqc.merge(df_wafer_SA, left_on='Wafer', right_on='ContainerName', how='left')

    if os.name == 'nt':
        path_EOLQC_csv = r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_SCC.csv'
        path_EOLQC_pkl = r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_SCC.pkl'
    else:
        path_EOLQC_csv = r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_SCC.csv'.replace('\\', '/')
        path_EOLQC_pkl = r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_SCC.pkl'.replace('\\', '/')

    df_eolqc_old = pd.read_pickle(path_EOLQC_pkl)
    # df_eolqc_old['SA_Lot'] = ''
    df_eolqc = df_eolqc_old.append(df_eolqc)
    df_eolqc = df_eolqc.drop_duplicates(subset=['Chip'], keep='last')

    df_eolqc.to_csv(path_EOLQC_csv)
    df_eolqc.to_pickle(path_EOLQC_pkl)
    return df_eolqc


if __name__ == '__main__':
    data = gen_eolqc_data(sys.argv)

#    run_notebook('EOLQC_Report1.ipynb')


# chip_list = pd.read_csv(r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\Sample Prep Lot Number_QueryHelp_10092020.csv')
# data_prep = report.get_Sample_Prep_Lot(chip_list['ContainerName'].to_list())
# data_prep.to_csv(r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\Sample Prep Lot Number_QueryHelp_10092020_Done.csv')
