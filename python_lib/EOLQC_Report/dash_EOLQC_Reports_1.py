# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 14:33:45 2020

@author: ltu
"""

import dash
from dash.exceptions import PreventUpdate
import dash_html_components as html
import dash_core_components as dcc
import os
import webbrowser
import pickle
from util_liang.Trie import TrieEOLQC 


external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
if os.name == 'nt':
    folder_dir = r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\EOLQC_Report'
    folder_output = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
else:
    folder_dir = r'//pbi/dept/chip/ChipEng/Liang/Python-Lib/EOLQC_Report'
    folder_output = r'//pbi/dept/chip/ChipEng/EOLQC-Report'
    
filehandler = open(os.path.join(folder_dir, 'trie_EOLQC.obj'), 'rb') 
trie_obj = pickle.load(filehandler)
filehandler.close()

#folder_dir = r'\\pbi\dept\chip\ChipEng\EOLQC-Report'
#listOfFolders = [f for f in os.listdir(folder_dir) if not os.path.isfile(f)]
#options = []
#for folder in listOfFolders:
#    folder_path = os.path.join(folder_dir, folder)
#    listOfFiles = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path,f))]
#    for file in listOfFiles:
#        file_path = os.path.join(folder_path, file)
#        options.append({"label": file.split('.')[0], "value":file, "ts":os.path.getmtime(file_path)})
#
#options.sort(key = lambda x: x["ts"], reverse = True)
options = [{"label": o["value"].split('.')[0], "value":o["value"]} for o in trie_obj.get('')]

#options = [
#    {"label": "New York City", "value": "NYC"},
#    {"label": "Montreal", "value": "MTL"},
#    {"label": "Minnesota", "value": "MN"},
#    {"label": "San Francisco", "value": "SF"},
#]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.layout = html.Div([
        html.Label(["Chip EOLQC Report", dcc.Dropdown(id="my-dynamic-dropdown", options = options, value = '', placeholder="Select a chip" )]),
        html.Button('Submit', id='button'),
#        dcc.Link(id='WaferLink', href='www.google.com'),
        html.Div(id='page-content'),
        html.Br(),html.Br(),
        html.Br(),html.Br(),
        html.Br(),html.Br(),
        html.A(html.Label('you must enable local file links in browser', className='three columns'), href ='https://chrome.google.com/webstore/detail/enable-local-file-links/nikfmfgobenbhmocjaaboihbeocackld')
#        html.Div(id='output-container-button',
#             children='Enter a value and press submit')
        ]
        
#        html.Label(["Multi dynamic Dropdown",dcc.Dropdown(id="my-multi-dynamic-dropdown", multi=True)] )
)
  
@app.callback( dash.dependencies.Output("my-dynamic-dropdown", "options"), [dash.dependencies.Input("my-dynamic-dropdown", "value")] )
def update_options(search_value):
    if not search_value:
        raise PreventUpdate
    if len(search_value)>=10:
        return options
    return trie_obj.get(search_value)
#    return [o for o in options if search_value in o["label"]]

@app.callback(dash.dependencies.Output('page-content', 'children'), [dash.dependencies.Input('button', 'n_clicks')], [dash.dependencies.State('my-dynamic-dropdown', 'value')])
def plot(n_clicks, value):
    if n_clicks:
        link = os.path.join(folder_output, value[:7], value)
        #link = (folder_dir + '\\' + value[:7] + '\\' +  value)
        #webbrowser.open_new_tab('file://' + r'\\pbi\dept\chip\ChipEng\EOLQC-Report\AX17033\AX17033-08.html')
        return html.A(html.Button('Click to Open Report', className='three columns'), href ='file:///'+link)  
    
#        webbrowser.open_new_tab('file://' + link)
#webbrowser.open_new_tab(r'//pbi/dept/chip/ChipEng/EOLQC-Report/AX17033/AX17033-08.html')


#@app.callback(
#    dash.dependencies.Output('output-container-button', 'children'),
#    [dash.dependencies.Input('button', 'n_clicks')],
#    [dash.dependencies.State('my-dynamic-dropdown', 'value')])
#def update_output(n_clicks, value):
#    return 'The input value was "{}" and the button has been clicked {} times'.format(
#        value,
#        n_clicks
#    )

if __name__ == "__main__":
    app.run_server(debug=False, port=8080, use_reloader=False)