# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 11:23:59 2020

@author: ltu
"""
import pandas as pd
import numpy as np
import pyodbc 
#import matplotlib
#import matplotlib.pyplot as plt
#import seaborn as sb
#from sklearn import preprocessing
#import matplotlib.patheffects as path_effects
import datetime
import os
import pickle


folders = [r'\\pbi\dept\systems\emcc\GIF\EOLQC\64010', r'\\pbi\dept\systems\emcc\GIF\EOLQC\64059', r'\\pbi\dept\systems\emcc\GIF\EOLQC\64092']

#file = r'\\pbi\dept\systems\emcc\GIF\EOLQC\64010\m64010_200128_031811.DA036890.P0.txt'
table_score = pd.DataFrame()

for folder in folders:
    for file in os.listdir(folder):
        if file.endswith(".txt"):
            with open(os.path.join(folder, file), 'r') as f:
                content = f.read()
                words = content.split()
                movie_info = words[1].split('.')
                table_score = table_score.append({'movie':movie_info[0],'substrate':movie_info[1],'param':movie_info[2], 'result':words[2], 'final_score':words[5],'color_score':words[8],'uniformity_score':words[11] }, ignore_index=True)
                
summary_file = r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\summary_sabrina.pkl'
file = open(summary_file, 'wb')
pickle.dump(table_score, file)

#with open(summary_file, 'rb') as f:
#    a = pickle.load(f)
                
          
data_2019 = pd.read_csv(r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_2019.csv')
data_2020 = pd.read_csv(r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_2020.csv')
data = data_2019.append(data_2020)
del data_2019, data_2020
data = data.merge(table_score, left_on='moviename', right_on='movie', how = 'left')
data.to_csv(r'\\pbi\dept\chip\ChipEng\Liang\Others\Ming-2019-09\Data\EOLQC_withScore.csv')
                