# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 10:42:55 2019

@author: ltu
"""
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
import zipfile
from matplotlib import pyplot as plt
import datetime
import pandas as pd
import os
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import urllib, json
from pandas.io.json import json_normalize
today = datetime.datetime.today()
#date_start = datetime.date(today.year,today.month-3,1)
date_start = today - datetime.timedelta(days=90)


def context_to_time(context):
    time = context.split('_')[1]
    year = int(time[0:2])+2000
    month = int(time[2:4])
    day = int(time[4:6])
    date = datetime.date(year, month, day)
    week = date.isocalendar()[1]
    return year, month, day, week, date

def p1_to_cat(line):
    p1= float(line.iloc[0]) 
    acquisition_status = line.iloc[1]
    if acquisition_status=='Hard_Elec':
        return 'Hard_Elec'
    elif pd.isna(p1):
        return 'Unknown'
    if p1<20:
        return "0 to 20%"
    elif p1<30:
        return "20% to 30%"
    elif p1<40:
        return "30% to 40%"
    elif p1<50:
        return "40% to 50%"
    else:
        return "50% to 100%"
    
def modify_acquisition_status(s, Minilid_mode,failure_mode_hard,failure_mode_soft):
    if s.iloc[1] in Minilid_mode:
        return 'Minilid'
    elif  s.iloc[1] in failure_mode_hard:
        return 'Hard_Elec'
    elif  s.iloc[1] in failure_mode_soft:
        return 'Soft_Elec'
    else:
        return s.iloc[0]
    
    
def plot_failure_month(data, region, acquisition_list,  save_folder, prod_type):
    if region=='World':
        data1 = data
    else:
        data1 = data[data['Account Region']==region]
    chip_count = data1.groupby(['Yr-Mth'])['acquisition_status'].count().reset_index()
    fig, ax = plt.subplots(figsize=(10, 5))
    ax.plot(chip_count['Yr-Mth'],chip_count['acquisition_status'], color='r', zorder=1, lw=1)
    ax.scatter(chip_count['Yr-Mth'],chip_count['acquisition_status'], marker='o', color='r', zorder=2)
    _ = plt.setp(ax.get_xticklabels(), rotation=90, ha="right",rotation_mode="anchor")
    ax.set_title('Chip Escape by Month in ' + region)
    ax.xaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
    ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
    ax.set_ylabel('Chip_Count', color='r')
    ax.spines['left'].set_color('red')
    ax.yaxis.label.set_color('red')
    ax.tick_params(axis='y', colors='red')
    #ax.legend(loc='upper left', bbox_to_anchor=(1, 0.9))
    #plt.tight_layout()
    ax2 = ax.twinx()
    chip_status_count = data1.groupby(['Yr-Mth','acquisition_status']).size().reset_index(name='counts')
    df = chip_status_count.pivot(index='Yr-Mth',columns='acquisition_status',values='counts').reset_index()
    for chip_status in acquisition_list:
        if chip_status not in df.columns:
            df[chip_status] = 0
#    if 'Minilid' not in df.columns:
#        df['Minilid'] = 0
#    if 'Hard_Elec' not in df.columns:
#        df['Hard_Elec'] = 0
#    if 'Soft_Elec' not in df.columns:
#        df['Soft_Elec'] = 0
    df = df.fillna(0)
    df = df.merge(chip_count, left_on=['Yr-Mth'], right_on=['Yr-Mth'])
    df['%Fails'] = (df['Failed']+df['Minilid']+df['Hard_Elec']+df['Soft_Elec'])/(df['acquisition_status'])*100
    ax2.scatter(df['Yr-Mth'],df['%Fails'], marker='d', color='g', facecolors='none', zorder=2)
    df['%Fails_Minilid'] = (df['Minilid'])/(df['acquisition_status'])*100
    ax2.scatter(df['Yr-Mth'],df['%Fails_Minilid'], marker='d', color='c', facecolors='none', zorder=2)
    plt.ylabel('%Fails', color='k')
    df['%Fails_Elec'] = (df['Hard_Elec']+df['Soft_Elec'])/(df['acquisition_status'])*100
    ax2.scatter(df['Yr-Mth'],df['%Fails_Elec'], marker='d', color='b', facecolors='none', zorder=2)
    df['%Fails_Elec_Hard'] = (df['Hard_Elec'])/(df['acquisition_status'])*100
    ax2.scatter(df['Yr-Mth'],df['%Fails_Elec_Hard'], marker='d', color='coral', facecolors='none', zorder=2)
    plt.ylabel('% Fails', color='k')
    if prod_type=='Sequel':
        ax2.legend(loc="upper right")  
    else:
        ax2.legend(loc="upper left")  
    ax2.plot(df['Yr-Mth'],df['%Fails'], color='g', zorder=1, lw=1)
    ax2.plot(df['Yr-Mth'],df['%Fails_Minilid'], color='c', zorder=1, lw=1)
    ax2.plot(df['Yr-Mth'],df['%Fails_Elec'], color='b', zorder=1, lw=1)
    ax2.plot(df['Yr-Mth'],df['%Fails_Elec_Hard'], color='coral', zorder=1, lw=1)
    fig.savefig(save_folder+'\\Plots\\' +prod_type+ ' '+region + ' Escape.png')
    df['%Escape'] = (df['Hard_Elec'] + df['Minilid'])/(df['acquisition_status'])*100
    return df


acquisition_list = ['Complete','Failed', 'Minilid', 'Hard_Elec','Soft_Elec']

def load_field_data(year=2018, month=1, date=1):
    path_folder = r'\\pbi\dept\chip\ChipEng\Liang\Field Data'.replace('\\','/')
    data_seq = pd.read_pickle(os.path.join(path_folder, 'Sequel.pkl') )
    data_spider = pd.read_pickle(os.path.join(path_folder, 'Spider.pkl') )
    data = data_seq.append(data_spider)
    data1 = data[ data['acquisition_status'].isin(acquisition_list) ]
    data1 = data1[data1['date']>=datetime.date(year, month, date)]
    return data1


def plotly_failure_month(data, region):
    prod_list = ['Sequel','Spider']
    prod_color = {'Sequel':'blue','Spider':'red'}
    if region=='World':
        data1 = data
    else:
        data1 = data[data['Account Region']==region]
    chip_count = data1.groupby(['Instr_Type','Yr-Mth'])['acquisition_status'].count().reset_index()
    
    chip_status_count = data1.groupby(['Instr_Type','Yr-Mth','acquisition_status']).size().reset_index(name='counts')
    df = pd.pivot_table(chip_status_count, index=['Instr_Type','Yr-Mth'],columns='acquisition_status',values='counts', aggfunc=np.sum).reset_index()
    for chip_status in acquisition_list:
        if chip_status not in df.columns:
            df[chip_status] = 0
    df = df.fillna(0)
    df = df.merge(chip_count, left_on=['Instr_Type','Yr-Mth'], right_on=['Instr_Type','Yr-Mth'])
    df['%Fails'] = (df['Failed']+df['Minilid']+df['Hard_Elec']+df['Soft_Elec'])/(df['acquisition_status'])*100
    df['%Fails_Minilid'] = (df['Minilid'])/(df['acquisition_status'])*100
    df['%Fails_Elec'] = (df['Hard_Elec']+df['Soft_Elec'])/(df['acquisition_status'])*100
    df['%Fails_Elec_Hard'] = (df['Hard_Elec'])/(df['acquisition_status'])*100
    df['%Escape'] = (df['Hard_Elec'] + df['Minilid'])/(df['acquisition_status'])*100
  
    def gen_failure_list(x, para):
        df_sub = data1[ (data1['Yr-Mth']==x['Yr-Mth'])  & (data1['Instr_Type']==x['Instr_Type'])  & (data1['acquisition_status']==para) ]
        return df_sub['Chip'].tolist()
    df['Minilid_List'] = df.apply(lambda x: gen_failure_list(x, para='Minilid'), axis=1)
    df['HardElec_List'] = df.apply(lambda x: gen_failure_list(x, para='Hard_Elec'), axis=1)

    def gen_href(x):
        if isinstance(x, list):
            output = 'Chip:'
            for item in x:
    #             output += "<a href= https://github.com>" + item + "</a>"
                output += "<br> "+ str(item)
            return output
        else:
            return x

    property_iter = [("acquisition_status", 'acquisition_status', 'Field Volume', 'Chip #'),
                     ('%Fails_Minilid','Minilid_List','Minilid_Failure_Rate','Failure %'),
                    ('%Fails_Elec_Hard','HardElec_List','HardElec_Failure_Rate','Failure %')]  #field, hovertext, title, y_axis
    
    for property in property_iter:
        fig = make_subplots(rows=1, cols=1)
        for prod in prod_list:
            df1 = df[df['Instr_Type']==prod]
            

            fig.add_trace(go.Scatter(x=df1["Yr-Mth"], y=df1[property[0]], mode='lines', line = dict(color=prod_color[prod]), 
                                       name=prod),   row=1, col=1)
            fig.add_trace(go.Scatter(x=df1["Yr-Mth"], y=df1[property[0]], mode="markers",  hovertext=list(map(lambda x, y:'{:.3f}'.format(x)+'%<br>'+ y if isinstance(y, str) else str(x), df1[property[0]].tolist(),df1[property[1]].apply(gen_href).tolist() )), hoverinfo="text",
                                      marker=dict(size=6,line=dict(width=2,color='DarkSlateGrey'), color=prod_color[prod]), 
                                     opacity=0.7, showlegend=False), row=1, col=1)  

    #     fig = px.line(chip_count, x="Yr-Mth", y="acquisition_status",   color='Instr_Type')



        fig.update_layout(
            title=go.layout.Title(
                text=property[2],  xref="paper", x=0
            ),
            xaxis=go.layout.XAxis(
                title=go.layout.xaxis.Title(
    #                 text="x Axis",
                    font=dict(family="Courier New, monospace", size=18,color="#7f7f7f")
                )
            ),
            yaxis=go.layout.YAxis(
                title=go.layout.yaxis.Title(
                    text=property[3],
                    font=dict(family="Courier New, monospace", size=18,color="#7f7f7f")
                )
            )
        )


        fig.show()
        
    data2 = data[ (data['date']>=date_start) & (data['acquisition_status'].isin(['Minilid', 'Hard_Elec']))]
    data2 = data2[['Instr_Type','date','acquisition_status','Chip','MovieContext','hard_failure_cause']]  #'week',,'Cell_ExpirationDate','full_name',

    headerColor = 'grey'
    rowEvenColor = 'lightgrey'
    rowOddColor = 'white'
    fig = go.Figure(data=[go.Table(
            columnwidth = [5,8,5,12,16,40],
      header=dict(
        values=list(data2.columns),
        line_color='darkslategray',
        fill_color=headerColor,
        align=['left','center'],
        font=dict(color='white', size=12)
      ),
      cells=dict(
        values=data2.values.transpose().tolist(),
        line_color='darkslategray',
        # 2-D list of colors for alternating rows
        fill_color = [[rowOddColor,rowEvenColor,rowOddColor, rowEvenColor,rowOddColor, rowEvenColor]*5],
        align = ['left', 'center'],
        font = dict(color = 'darkslategray', size = 11)
        ))
    ])
    
    fig.show()
    
    
def fetch_field_instr():
    instr_url_list = {'Sequel':"http://smrtfleet-api-staging:8080/v1/sq/instru", 'Spider':"http://smrtfleet-api-staging:8080/v1/sq2/instru"}
    data_instr = pd.DataFrame()
    for prod in ['Sequel', 'Spider']:
        instr_url = instr_url_list[prod]
        with urllib.request.urlopen(instr_url) as url:
            s = url.read().decode()
        ss = json.loads(s)['response']
        for key in ss.keys():
            sub_data = json_normalize(ss[key])
            sub_data['Instr_Type'] = prod
            data_instr = data_instr.append(sub_data)
    data_instr['x_Serial'] = data_instr['serial'].apply(lambda x: x[0:5] if((len(x)>5) and (x[5]=='R')) else x)
    data_instr['Account Region'] = data_instr['account_region'] 
    return data_instr
#    folder_path = r'C:\Users\ltu\Documents\R\Customer_Failure'
#    instr_sequel_name = 'instruments_overview-20191017223816-Sequel.csv'
#    instr_sequel = pd.read_csv( os.path.join(folder_path, instr_sequel_name) , dtype=str)
#    instr_sequel['Instr_Type'] = 'Sequel'
#    instr_spider_name = 'instruments_overview-20191017223913-Spider.csv'
#    instr_spider = pd.read_csv( os.path.join(folder_path, instr_spider_name) , dtype=str)
#    instr_spider['Instr_Type'] = 'Spider'
#    instr = instr_sequel.append(instr_spider)
#    instr['x_Serial'] = instr['Serial'].apply(lambda x: x[0:5] if((len(x)>5) and (x[5]=='R')) else x)
    
def fetch_field_data(prod_type = 'Sequel'):
    if prod_type == 'Sequel':
        internal = False
        if internal==True:
            activity_url = "http://vm-ts-instru_api-001:8080/v1/sequencing_performance/zip"
        else:
            activity_url = "http://smrtfleet-api.nanofluidics.com:8080/v1/sequencing_performance/zip"
        with urllib.request.urlopen(activity_url) as url:
            s = url.read().decode()
            download_link = s[15:-4]
        #    urllib.request.urlretrieve(download_link, '/Users/scott/Downloads/cat.jpg')  
        #temp_f = tempfile.TemporaryFile(mode='w+b')
        r = urllib.request.urlretrieve(download_link)
        with zipfile.ZipFile(r[0],"r") as zip_ref:
            folder_path = r'\\pbi\dept\chip\ChipEng\Liang\Python-Lib\data-temp'
            data_path = os.path.join(folder_path,  'sequencing_performance_data', zip_ref.namelist()[0])
            zip_ref.extractall(os.path.join(folder_path,  'sequencing_performance_data'))
        #data_name = 'sequencing_performance_details-20190626185214.csv'
        data = pd.read_csv( data_path, dtype=str)
    else:
        today = datetime.datetime.today()
        current_date = today.strftime("%Y%m%d")
        activity_url = "http://smrtfleet-api:8080/v1/sq2/seq_perf_details?start_date_utc=20170101&end_date_utc="+current_date
        with urllib.request.urlopen(activity_url) as url:
            s = url.read().decode()
        ss = json.loads(s)
        data = json_normalize(ss['response'])
    
    data['x_Serial'] = data['x_serial'].apply(lambda x:x[0:5] if(len(x)>5 and x[5]=='R') else x)
    return data